��    )      d              �  P   �     �            H     
   e     p  	   y  .   �     �     �     �     �  
   �     �     �     �  -   �     )     E     V  	   j     t     �     �     �     �     �     �  /   �     �     �     
                    0     6     B     F  =  R  �   �          2     C  �   X  %   �          "  E   :     �     �     �     �     �  	   �     �     �  c   �  -   c	      �	  +   �	     �	     �	  	   

     
     
  6   :
     q
     ~
  Z   �
     �
          ,     =     J  !   _     �     �     �     �   All categories have already been parsed! Clear cache or choose new categories... Apply Bar code Cache cleared! Cannot establish the connection. Check your credentials and try again... Categories Category Check all Choose shops, cities, categories that you need Cities Clear cache Continue General settings General... Host: Name Name: Parsing category and city lists from shops... Parsing has been stopped... Parsing is done! Parsing the data... Password: Path settings Port: Price Price on sale Products directory: Proxy Proxy settings Set the directory where to save parsed products Settings Shop parser Shops Start Stop Total categories: {} Type: Uncheck all Url Vendor code Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2021-10-16 18:44+0300
PO-Revision-Date: 2021-10-16 18:45+0300
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
 Все категории уже были разобраны! Очистите кеш или выберите новые категории... Применить Штрихкод Кеш очищен! Прокси не прошёл проверку. Проверьте корректность данных и повторите ещё раз... Категории продуктов Категория Выделить все Выберите магазины, города и категории Города Очистить кеш Продолжить Общие настройки Общие... Хост: Имя Имя: Извлечение списков категорий и городов из магазинов... Парсинг был остановлен... Парсинг завершён! Идёт процесс парсинга... Пароль: Настройки путей Порт: Цена Цена со скидкой Папка для сохранения товаров: Прокси Настройки прокси Выберите папку, куда сохранять полученные товары Настройки Парсер магазинов Магазины Начать Остановить Всего категорий: {} Тип: Снять выделение Ссылка Артикул 