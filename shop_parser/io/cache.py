from pathlib import Path
import shop_parser.core.parser


def write_cache(file_path: str, parser: shop_parser.core.parser.ShopParser):
    dir_path = Path(file_path).parent
    dir_path.mkdir(parents=True, exist_ok=True)

    with open(file_path, 'w', encoding='utf-8') as file:
        entries = parser.get_cache_entries()
        for entry in entries:
            file.write(entry)
            file.write('\n')


def read_cache(file_path: str, parser: shop_parser.core.parser.ShopParser):
    if not Path(file_path).is_file():
        return

    lines = []
    with open(file_path, encoding='utf-8') as file:
        for line in file:
            lines.append(line.rstrip())

    parser.set_cache_entries(lines)
