import csv
import json
import pandas
import logging
import os
from collections import OrderedDict
from datetime import datetime
from pathlib import Path

UNMERGED_SHOP_INFO_SHOPS_KEY = 'shops'
UNMERGED_SHOP_INFO_CITIES_KEY = 'cities'
UNMERGED_SHOP_INFO_SHOP_NAME_KEY = 'name'
UNMERGED_SHOP_INFO_CITY_NAME_KEY = UNMERGED_SHOP_INFO_SHOP_NAME_KEY


class ProductWriter:
    def __init__(self, root_dir: str, date_prefix: str = None, time_prefix: str = None):
        self.logger = logging.getLogger('product-writer')
        self.root_dir = root_dir + '/' + 'parsed-data'
        self.cities = {}

        self.date_prefix = date_prefix
        if not self.date_prefix:
            self.date_prefix = get_current_date_string()

        self.time_prefix = time_prefix
        if not self.time_prefix:
            self.time_prefix = get_current_time_string()

        self._read_unmerged_shops()
        self.merge()

    def write(self, shop_name: str, city_name: str, product: OrderedDict):
        file_path = self._find_products_file_path(shop_name, city_name)
        if not file_path:
            # print(f'[ProductWriter] Not found: "{shop_name}", "{city_name}"')
            header_labels = get_header_labels(product)
            file_path = self._generate_empty_products_file_by_shop(shop_name, city_name, header_labels)

        write_product(file_path, product)

    def get_date_prefix(self):
        return self.date_prefix

    def get_time_prefix(self):
        return self.time_prefix

    def merge(self):
        for shop_name in self.cities.keys():
            for city_name in self.cities[shop_name].keys():
                cache_file_path = self._get_products_cache_file_path(shop_name, city_name)
                file_path = self._get_products_file_path(shop_name, city_name)

                self._merge(cache_file_path, file_path)

        self.cities.clear()
        self._write_unmerged_shops()

    def _merge(self, cache_file_path, file_path):
        file_path = file_path.replace('csv', 'xlsx')
        try:
            cached_data = pandas.read_csv(cache_file_path, sep='\t')
        except pandas.errors.EmptyDataError:
            return  # cache is empty, nothing to do...

        cached_data = cached_data.drop_duplicates()

        old_data = pandas.read_excel(file_path) if os.path.exists(file_path) else None
        merged = pandas.concat([old_data, cached_data], sort=False)
        merged = merged.dropna(how='all', axis='columns')  # remove fully 'nan' columns

        write_excel(file_path, merged)

        clear_file(cache_file_path)

    def _get_products_directory(self, shop_name: str, city_name: str) -> str:
        return self._get_csv_shops_cache_directory() \
               + '/' + shop_name \
               + '/' + city_name

    def _get_csv_shops_cache_directory(self) -> str:
        return self.root_dir \
               + '/cache' \
               + '/csv' \
               + '/' + self.date_prefix \
               + '/' + self.time_prefix

    def _find_products_file_path(self, shop_name: str, city_name: str) -> str:
        out = ''
        if shop_name not in self.cities:
            return out

        file_paths = self.cities[shop_name]
        if city_name not in file_paths:
            return out

        out = file_paths[city_name]
        return out

    def _get_products_cache_file_path(self, shop_name: str, city_name: str) -> str:
        dir_path = self._get_products_directory(shop_name, city_name)
        return dir_path \
               + '/products.csv'

    def _generate_products_dir(self, file_path: str):
        dir_path = make_directory_by_file_path(file_path)
        self.logger.debug(f'Make directory: {dir_path}')

    def _generate_empty_products_file_by_shop(self, shop_name: str, city_name: str, header_labels: list) -> str:
        file_path = self._get_products_cache_file_path(shop_name, city_name)

        self._generate_products_dir(file_path)
        clear_file(file_path)
        write_csv_row(file_path, header_labels)

        self._insert_products_file_path(shop_name, city_name, file_path)
        self._write_unmerged_shops()

        return file_path

    def _insert_products_file_path(self, shop_name: str, city_name: str, file_path: str):
        if shop_name not in self.cities:
            self.cities[shop_name] = {}

        file_paths = self.cities[shop_name]
        if city_name not in file_paths:
            # print(f'[ProductWriter] Insert: added "{shop_name}", "{city_name}"')
            file_paths[city_name] = file_path
        else:
            self.logger.critical('Insert: should not happened!!!')
            raise

    def _get_products_file_path(self, shop_name: str, city_name: str) -> str:
        return self.root_dir \
               + '/' + self.date_prefix + '_' + self.time_prefix \
               + '/' + shop_name \
               + '/' + self._get_products_file_name(shop_name, city_name)

    def _get_products_file_name(self, shop_name: str, city_name: str) -> str:
        products_format = '{date} {shop_name} ({city_name}).csv'
        return products_format.format(
            date=self.date_prefix,
            shop_name=shop_name,
            city_name=city_name
        )

    def _get_unmerged_products_file_path(self) -> str:
        return self._get_csv_shops_cache_directory() \
               + '/unmerged-shops.json'

    def _write_unmerged_shops(self):
        file_path = self._get_unmerged_products_file_path()
        write_unmerged_shops(file_path, self.cities)

    def _read_unmerged_shops(self):
        file_path = self._get_unmerged_products_file_path()
        json_root = read_unmerged_shops(file_path)
        if not json_root:
            return

        shop_info_list = json_root[UNMERGED_SHOP_INFO_SHOPS_KEY]
        for shop_info in shop_info_list:
            shop_name = shop_info[UNMERGED_SHOP_INFO_SHOP_NAME_KEY]
            cities = shop_info[UNMERGED_SHOP_INFO_CITIES_KEY]
            for city in cities:
                city_name = city[UNMERGED_SHOP_INFO_CITY_NAME_KEY]
                file_path = self._get_products_cache_file_path(shop_name, city_name)
                self._insert_products_file_path(shop_name, city_name, file_path)


def get_header_labels(product: OrderedDict) -> list:
    out: list = []
    for key, value in product.items():
        out.append(key)

    return out


def write_excel(file_path: str, df: pandas.DataFrame):
    make_directory_by_file_path(file_path)
    writer = pandas.ExcelWriter(file_path, engine='xlsxwriter')

    df.to_excel(writer, sheet_name='Sheet1', index=False)  # send df to writer
    worksheet = writer.sheets['Sheet1']  # pull worksheet object
    for idx, col in enumerate(df, start=1):  # loop through all columns
        series = df[col]
        max_len = max((
            series.astype(str).map(len).max(),  # len of largest item
            len(str(series.name))  # len of column name/header
        )) + 1  # adding a little extra space
        # set column width
        worksheet.set_column(idx - 1, idx - 1, max_len + 1)  # '-1' because index=False
    writer.save()


def convert_to_string(value: float) -> str:
    value_format = '{:.2f}'
    out = value_format.format(value)
    out = out.replace('.', ',')

    return out


def convert_to_string_by_type(value) -> str:
    if isinstance(value, float):
        return convert_to_string(value)
    elif value is None:
        return ''

    return value


def get_current_date_string() -> str:
    format_string = '%Y-%m-%d'
    out = datetime.now().strftime(format_string)
    return out


def get_current_time_string() -> str:
    format_string = '%H-%M-%S'
    out = datetime.now().strftime(format_string)
    return out


def write_csv_row(file_path: str, row_strings: list[str]):
    with open(file_path, 'a', encoding='utf-8', newline='') as f:
        writer = csv.writer(f, delimiter='\t')
        writer.writerow(row_strings)


def write_product(file_path: str, product: OrderedDict):
    row = []
    for key, value in product.items():
        row.append(convert_to_string_by_type(value))

    write_csv_row(file_path, row)


def clear_file(file_path: str):
    with open(file_path, 'w') as file:
        pass


def make_directory_by_file_path(file_path: str) -> str:
    dir_path = Path(file_path).parent
    dir_path = str(dir_path)
    make_directory_by_dir_path(dir_path)

    return dir_path


def make_directory_by_dir_path(dir_path: str):
    path = Path(dir_path)
    path.mkdir(parents=True, exist_ok=True)


def write_unmerged_shops(file_path, shop_dict: dict):
    make_directory_by_file_path(file_path)

    shop_info_list = []
    for shop_name in shop_dict.keys():
        cities: list[dict] = []
        for city_name in shop_dict[shop_name].keys():
            city = {
                UNMERGED_SHOP_INFO_CITY_NAME_KEY: city_name
            }

            cities.append(city)

        shop_info = {
            UNMERGED_SHOP_INFO_SHOP_NAME_KEY: shop_name,
            UNMERGED_SHOP_INFO_CITIES_KEY: cities
        }

        shop_info_list.append(shop_info)

    json_root = {
        UNMERGED_SHOP_INFO_SHOPS_KEY: shop_info_list
    }

    with open(file_path, 'w', encoding='utf-8') as file:
        json.dump(json_root, file, ensure_ascii=False, indent=2)


def read_unmerged_shops(file_path) -> dict:
    if not Path(file_path).is_file():
        return {}

    with open(file_path, 'r', encoding='utf-8') as file:
        json_root = json.load(file)

    return json_root
