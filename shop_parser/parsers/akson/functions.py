import bs4

from shop_parser.network import Reply, headlessdriver
import json
import re

from bs4 import BeautifulSoup
from urllib.parse import urljoin
from collections import OrderedDict

import shop_parser.core.items as sp
import shop_parser.core.product_keys as product_keys

from selenium.webdriver.remote.webdriver import WebDriver, By
from selenium.common import exceptions
import time


# The site is built on js. Anyway there is an API, that can be used.
# The storeID can be obtained from either source code of the main page (it is somehwhere in "meta" tag
# down the page in JSON-form. Or (if not many) - hardcode, than you can inspect network requests in
# browser and look at cookies when city is changed.
#
# The link to API to request products is below:
# https://api1.akson.ru:8443/v8/catalog/section_products/fitingi_trubnye_rezbovye/33/0/?limit=100&skip=200
#                                                                ^                ^  ^      ^           ^
#                                                           category path    storeID | [0-100]pr2displ  number product
#                                                        (copy part of url)    [api version?]            to start from
def create_api_url(store_id: str, section_id: str, skip: int, limit: int = 100) -> str:
    return f'https://api1.akson.ru:8443/v8/catalog/section_products/{section_id}/{store_id}/0/' \
           f'?limit={limit}&skip={skip}'


def select_city(city_name: str, webdriver: WebDriver):
    city_container: webdriver = None
    try:
        city_container = webdriver.find_element(By.CLASS_NAME, 'city-choice')
    except exceptions.NoSuchElementException:
        button = webdriver.find_element(By.CLASS_NAME, 'top-bar__city-choice-item')
        button.click()
        time.sleep(0.3)
        headlessdriver.wait_until_page_ready(webdriver)
        city_container = webdriver.find_element(By.CLASS_NAME, 'city-choice')
    finally:
        element = city_container.find_element(By.XPATH, f'//p[text()="{city_name}"]')
        element.click()
        time.sleep(0.5)
        headlessdriver.wait_until_page_ready(webdriver)


def parse_category_item(element: str, url: str) -> sp.CategoryTreeItem:
    soup = BeautifulSoup(element, 'html.parser')
    name = soup.find('div', class_='categories__children-title').text.strip()
    tree_item = sp.CategoryTreeItem(name)

    level_2_nodes = soup.find_all('div', class_='categories__child-group')
    for level_2_node in level_2_nodes:
        try:
            name = level_2_node.find('a', class_='categories__child_title').text.strip()
            level_2_url = level_2_node.find('a').get('href')
            level_2_item = sp.CategoryTreeItem(name, urljoin(url, level_2_url), tree_item)
            level_3_nodes = level_2_node.find_all('a', class_='categories__child')[1:]
            for level_3_node in level_3_nodes:
                name = level_3_node.text.strip()
                level_3_url = level_3_node.get('href')
                level_3_item = sp.CategoryTreeItem(name, urljoin(url, level_3_url), level_2_item)
        except:
            pass
    return tree_item


def parse_catalog(webdriver: WebDriver) -> sp.CategoryTree:
    catalog_button = webdriver.find_element(By.CLASS_NAME, 'header__group-button')
    catalog_button.click()
    time.sleep(0.5)
    top_level_elements = webdriver.find_elements(By.CLASS_NAME, 'categories__root-item')
    top_level_elements = top_level_elements[2:]  # remove 'Сделай сам' and 'Готовые решения'
    tree = sp.CategoryTree()
    category_items = []
    for top_level_element in top_level_elements:
        top_level_element.click()
        headlessdriver.wait_until_page_ready(webdriver)
        categories_element = webdriver.find_element(By.CLASS_NAME, 'categories__children'). \
            get_attribute('innerHTML')
        tree_item = parse_category_item(categories_element, webdriver.current_url)
        category_items.append(tree_item)
    tree.root_item.children = category_items

    return tree


def parse_products_json(reply: Reply) -> list[OrderedDict]:
    json_doc = json.loads(reply.text)
    products = json_doc['data']['products']
    out = []
    for product in products:
        try:
            name = product['name'].replace('\n', ' ')
            url = f'https://akson.ru/p/{product["code"]}/'
            vendor_code = product['xmlId']  # TODO: ?
            price = product['price']
            # price_with_sale TODO: price with sale
            category = product['namePath'].replace('/', ' > ')
            out.append(product_keys.convert_to_product(vendor_code, category, name, price, url))
        except Exception:
            pass

    return out


def parse_product_count_json(reply: Reply) -> int:
    json_doc = json.loads(reply.text)
    products_number = int(json_doc['data']['cntAfterFilter'])
    return products_number


def parse_section_id(reply: Reply, **kwargs):
    # search for a any number of digits after "sectionId":
    match = re.search(r'(?<="sectionId":)\d+', reply.text)
    if match:
        id = match.group(0)
        return id


def parse_subcategories(reply: Reply, **kwargs) -> list[str]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    out = []
    navigation_node = soup.find('div', class_='catalog__menu')
    nodes = navigation_node.find_all('a', class_='child')
    for node in nodes:
        out.append(urljoin(reply.url, node.get('href')))
    return out


def parse_products_html(reply: Reply, **kwargs) -> list[OrderedDict]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    products = []
    product_nodes = soup.find_all('div', class_='product-matrix')
    if not product_nodes:
        raise RuntimeWarning("Page does not contain products. try to search for subcategories first")
    category = soup.find('div', class_='catalog__breadcrumbs')
    category = category.find_all('li', itemprop='itemListElement')[1:]
    category = [c.text for c in category][1:]
    category = ' > '.join(category)

    for product_node in product_nodes:
        price = product_node.find('div', class_='current-price').text  # Example: 2 323 руб
        price = ''.join(price.strip().split()[:-1])
        vendor_code = product_node.find('div', class_='product-info__code').text
        vendor_code = vendor_code.split()[1]

        info_node = product_node.find('div', class_='product-info__title')
        name = info_node.text.strip()
        url = info_node.find('a').get('href')
        products.append(product_keys.convert_to_product(vendor_code=vendor_code,
                                                        category=category,
                                                        name=name,
                                                        price=price,
                                                        url=urljoin(reply.url, url)))
    return products
