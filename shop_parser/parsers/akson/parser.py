from shop_parser.core.extractableparser import ExtractableShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.parsers import akson


class AksonParser(ExtractableShopParser):
    def __init__(self):
        super().__init__(
            name='Аксон',
            url='https://akson.ru',
            logger_name='AksonParser')

    def _create_shop_info_data_extractor(self) -> DataExtractor:
        return akson.AksonShopInfoDataExtractor()

    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        urls = [category_url]
        return akson.AksonCategoriesDataExtractor(urls, city_name)
