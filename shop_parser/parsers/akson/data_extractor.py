from shop_parser.core import DataExtractor
from shop_parser.network import SeleniumRequest, Request, Reply, headlessdriver

import shop_parser.core.items as spi
from shop_parser.core import data
from shop_parser.parsers.akson import functions
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode

from concurrent.futures import ThreadPoolExecutor

_name = 'Akson'
_url = 'https://akson.ru'

STORE_IDS = [
    spi.City('Череповец', '11'),
    spi.City('Дзержинск', '41'),
    spi.City('Иваново', '13'),
    spi.City('Калуга Болдина', '46'),
    spi.City('Калуга Московская', '44'),
    spi.City('Кинешма', '33'),
    spi.City('Кострома', '2'),
    spi.City('Москва', '50'),
    spi.City('Нижний Новгород', '43'),
    spi.City('Рыбинск', '29'),
    spi.City('Смоленск', '48'),
    spi.City('Тамбов', '45'),
    spi.City('Владимир', '36'),
    spi.City('Вологда', '16'),
    spi.City('Ярославль Фрунзе', '15'),
    spi.City('Ярославль Громова', '4')
]


class AksonShopInfoDataExtractor(DataExtractor):
    name = _name

    def start_requests(self):
        with ThreadPoolExecutor(max_workers=4) as executor:
            future = executor.map(self.__parse_category_tree, STORE_IDS)
            for city in future:
                yield city

    def __parse_category_tree(self, store: spi.City) -> data.City:
        driver = headlessdriver.create_webdriver(display_images=False, headless=True)
        driver.get(_url)
        headlessdriver.remove_all_elements_except('#app > section > header', driver)
        functions.select_city(store.name, driver)
        tree = functions.parse_catalog(driver)
        driver.quit()
        return data.City(store.name, store.domain_name, tree.root_item.children)


class AksonCategoriesDataExtractor(DataExtractor):
    name = _name

    def __init__(self, urls, city_name: str):
        super().__init__()
        self.start_urls = urls
        for city in STORE_IDS:
            if city.name == city_name:
                self.store_id = city.domain_name

    def start_requests(self):
        for url in self.start_urls:
            yield SeleniumRequest(url,
                                  callback=self.parse_products_html,
                                  cookies={'TP_CITY_STORE': self.store_id},
                                  postprocess=headlessdriver.scroll_page_down_till_end)

    def parse_products_html(self, reply: Reply, **kwargs):
        try:
            products = functions.parse_products_html(reply)
            for product in products:
                yield product
        except RuntimeWarning:  # page does not contain products
            subcategories = functions.parse_subcategories(reply)
            if len(subcategories) <= 1:
                raise RuntimeError(
                    f'Page with no products and subcategories: {reply.request.cookies}')
            for sub_url in subcategories:
                yield SeleniumRequest(sub_url,
                                      callback=self.parse_products_html,
                                      cookies={'TP_CITY_STORE': self.store_id},
                                      postprocess=headlessdriver.scroll_page_down_till_end)

