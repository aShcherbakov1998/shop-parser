from shop_parser.core import DataExtractor
from shop_parser.network import Request, Reply
import shop_parser.core.items as spi
from shop_parser.core import data
from shop_parser.parsers.vodoley import functions
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode

_name = 'Vodoley'
_url = 'https://водолей.рф/'

_cities = [spi.City('Красноярск', '0000896058')]


class VodoleyShopInfoDataExtractor(DataExtractor):
    name = _name

    def start_requests(self):
        for city in _cities:
            url = _url + f'?cityId={city.domain_name}'
            yield Request(url, callback=self.parse_category_tree, city=city)

    def parse_category_tree(self, reply: Reply, **kwargs):
        """keyword arguments:
            city: City """
        item_city = kwargs['city']

        category_tree = functions.parse_catalog_tree(reply)
        yield data.City(item_city.name, reply.request.url,
                        category_tree.root_item.children)


class VodoleyCategoriesDataExtractor(DataExtractor):
    name = _name

    def __init__(self, start_urls: list[str], city_name: str):
        super().__init__()
        self.start_urls = start_urls
        for city in _cities:
            if city.name == city_name:
                self.city_id = city.domain_name

    def start_requests(self):
        for url in self.start_urls:
            url += f'?cityId={self.city_id}&setPage=48'
            yield Request(url, callback=self.process_first_page)

    def process_first_page(self, reply: Reply, **kwargs):
        url_parsed = urlparse(reply.request.url)
        query_params = dict(parse_qsl(url_parsed.query))

        page_count = functions.parse_page_count(reply)

        for product in self.parse_products(reply):
            yield product

        for page_num in range(2, page_count + 1):
            query_params['PAGEN_1'] = page_num
            new_url = url_parsed._replace(query=urlencode(query_params))
            url = urlunparse(new_url)
            yield Request(url, callback=self.parse_products)

    def parse_products(self, reply: Reply, **kwargs):
        for product in functions.parse_products(reply):
            yield product
