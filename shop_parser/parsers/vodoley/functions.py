from bs4 import BeautifulSoup
from urllib.parse import urljoin
from collections import OrderedDict

from shop_parser.network import Reply
import shop_parser.core.items as sp
import shop_parser.core.product_keys as product_keys


# /?cityId={}
# /?setPage={}  - number of items visible (max = 48)
# /?PAGEN_1={}


def parse_cities(reply: Reply) -> list[sp.City]:
    pass


def parse_catalog_tree(reply: Reply) -> sp.CategoryTree:
    soup = BeautifulSoup(reply.text, 'html.parser')
    tree = sp.CategoryTree()
    categories = soup.find('ul', class_='menu-2__categories')
    level_1_nodes = categories.find_all('li', class_='menu-2__category')
    for level_1_node in level_1_nodes:
        name = level_1_node.find('a').text.strip()
        url = level_1_node.find('a').get('href')
        level_1_item = sp.CategoryTreeItem(name, urljoin(reply.url, url),
                                           tree.root_item)
        level_2_nodes = level_1_node.find_all('div', class_='menu-2__subcategory')
        for level_2_node in level_2_nodes:
            name = level_2_node.find('a').text.strip()
            url = level_2_node.find('a').get('href')
            level_2_item = sp.CategoryTreeItem(name, urljoin(reply.url, url),
                                               level_1_item)

            level_3_nodes = level_2_node.find_all('li', class_='menu-2__subcategory-item')

            for level_3_node in level_3_nodes:
                name = level_3_node.find('a').text.strip()
                url = level_3_node.find('a').get('href')
                level_3_item = sp.CategoryTreeItem(name, urljoin(reply.url, url),
                                                   level_2_item)
    return tree


def parse_page_count(reply: Reply) -> int:
    soup = BeautifulSoup(reply.text, 'html.parser')
    pagination = soup.find('div', class_='pagination')
    if not pagination:
        return 1

    page_nodes = pagination.find_all('a', class_='pagination__item')
    page_count = page_nodes[-2].text.strip()
    return int(page_count)


def parse_products(reply: Reply) -> list[OrderedDict]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    products = []
    category = soup.find('ul', class_='breadcrumbs__list')
    category = ' > '.join(item.text.strip() for item in category.find_all('li', class_='breadcrumbs__item')[2:])

    product_cards = soup.find_all('div', class_='catalog__product-item')

    for product in product_cards:
        vendor_code = product.find('div', class_='product-horizontal__article').text
        vendor_code = vendor_code.replace('Код:', '').strip()
        info_node = product.find('div', class_='product-horizontal__about')
        name = info_node.find('a').text.strip()
        url = info_node.find('a').get('href')
        price_node = product.find_all('div', class_='product-horizontal__price-wrap')
        price_node = list(filter(lambda node: '{{calculatePrice()}}' not in node.text, price_node))[0]

        current_price = price_node.find('div', class_='product-horizontal__price')
        current_price = current_price.text.replace('\xa0', '').replace('₽/шт', '').replace(' ', '').strip()
        old_price = price_node.find('div', class_='product-horizontal__price-old')
        if old_price:
            old_price = old_price.text.replace('\xa0', '') \
                .replace('₽/шт', '').replace(' ', '').strip()
        price_with_sale = float(current_price) if old_price else None
        price = float(old_price) if old_price else float(current_price)
        products.append(product_keys.convert_to_product(vendor_code, category, name, price,
                                                        urljoin(reply.url, url), price_with_sale))
    return products
