from shop_parser.core.extractableparser import ExtractableShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.parsers import vodoley


class VodoleyParser(ExtractableShopParser):
    def __init__(self):
        super().__init__(
            name='Водолей',
            url='https://водолей.рф/',
            logger_name='VodoleyParser')

    def _create_shop_info_data_extractor(self) -> DataExtractor:
        return vodoley.VodoleyShopInfoDataExtractor()

    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        urls = [category_url]
        return vodoley.VodoleyCategoriesDataExtractor(urls, city_name)
