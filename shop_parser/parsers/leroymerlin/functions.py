from bs4 import BeautifulSoup
from urllib.parse import urljoin
from collections import OrderedDict
import json

from shop_parser.network import Reply
import shop_parser.core.items as sp
import shop_parser.core.product_keys as product_keys


def parse_cities(reply: Reply) -> list[sp.City]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    city_nodes = soup.find_all('uc-regions-overlay-item')
    cities = []
    for city_node in city_nodes:
        info_node = city_node.find('a')
        id = info_node.get('href')
        name = city_node.text.strip()
        if name != 'Алматы':
            cities.append(sp.City(name, id))

    return cities


def parse_json_link(reply: Reply) -> str:
    soup = BeautifulSoup(reply.text, 'html.parser')
    link = soup.find('uc-catalog').get('url')
    return urljoin(reply.request.url, link)


def parse_catalog(reply: Reply) -> sp.CategoryTree:
    tree = sp.CategoryTree()
    json_doc = json.loads(reply.text)

    # TODO: place parse_item function definition outside in order
    #  not to create this function on each call
    def parse_item(tree_item_parent: sp.CategoryTreeItem, **kwargs):
        json_object = kwargs.get('json_object')
        name = json_object.get('name')
        if not name:
            return
        url = urljoin(reply.url, json_object['sitePath'])
        tree_item = sp.CategoryTreeItem(name, url, tree_item_parent)
        for child in json_object['childs']:
            parse_item(tree_item, json_object=child)

    for json_object in json_doc:
        parse_item(tree.root_item, json_object=json_object)
    return tree


# /?page={}
def parse_page_count(reply: Reply) -> int:
    soup = BeautifulSoup(reply.text, 'html.parser')
    page_num = 1
    pagination_node = soup.find('div', class_='s1pmiv2e_plp')
    if pagination_node:
        pages = pagination_node.find_all('span', class_='cef202m_plp')[:-1]  # last node is 'next'
        last_page = pages[-1]
        page_num = int(last_page.text)

    return page_num


def parse_products(reply: Reply) -> list[OrderedDict]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    item_cards = soup.find_all('div', class_='phytpj4_plp')
    category = soup.find_all('div', class_='i7podni_plp')
    category = ' > '.join([item.text.strip() for item in category][1:])
    products = []
    for item_card in item_cards:
        try:
            vendor_code = item_card.find('span', class_='t3y6ha_plp sn92g85_plp p16wqyak_plp').text
            vendor_code = vendor_code[4:].strip()  # remove 'Арт.'
            url = item_card.find('a').get('href')
            name = item_card.find('span', class_='t9jup0e_plp p1h8lbu4_plp').text.strip()
            price = item_card.find('p', class_='t3y6ha_plp xc1n09g_plp p1q9hgmc_plp').text.strip()
            price = float(price.replace('\xa0', '').replace(',', '.'))

            product = product_keys.convert_to_product(vendor_code, category, name, price, urljoin(reply.url, url))
            products.append(product)
        except:
            pass

    return products


def create_json_link_from_city_url(url: str) -> str:
    id = url.removeprefix('https://').split('.')[0]  # ['spb', 'barnaul', or 'leroymerlin' if moscow]
    template = f'/content/elbrus/{id if id != "leroymerlin" else "moscow"}/ru/displayedCatalogue.json'
    return urljoin(url, template)
