from shop_parser.core.extractableparser import ExtractableShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.parsers import leroymerlin


class LeroyMerlinParser(ExtractableShopParser):
    def __init__(self):
        super().__init__(
            name='Леруа Мерлен',
            url='https://leroymerlin.ru/',
            logger_name='LeroyMerlinParser')

    def _create_shop_info_data_extractor(self) -> DataExtractor:
        return leroymerlin.LeroyMerlinShopInfoDataExtractor()

    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        urls = [category_url]
        return leroymerlin.LeroyMerlinCategoriesDataExtractor(urls)
