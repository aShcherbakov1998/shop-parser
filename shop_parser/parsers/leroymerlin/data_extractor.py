from shop_parser.core import DataExtractor
from shop_parser.network import Request, Reply
import shop_parser.core.items as spi
from shop_parser.core import data
from shop_parser.parsers.leroymerlin import functions
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode

_name = 'LeroyMerlin'
_url = 'https://leroymerlin.ru/'
_city_mask = [
    'Москва, Московская область',
    'Барнаул',
    'Волгоград',
    'Волжский',
    'Воронеж',
    'Екатеринбург',
    'Иркутск',
    'Казань',
    'Нижний Новгород',
    'Новосибирск',
    'Омск',
    'Оренбург',
    'Пермь',
    'Ростов-на-Дону',
    'Санкт-Петербург',
    'Самара',
    'Саратов',
    'Тольятти',
    'Тюмень',
    'Уфа',
    'Хабаровск',
    'Челябинск',
]


class LeroyMerlinShopInfoDataExtractor(DataExtractor):
    name = _name

    def start_requests(self):
        yield Request(_url, callback=self.parse_cities)

    def parse_cities(self, reply: Reply, **kwargs):
        cities = functions.parse_cities(reply)
        cities = self.filter_cities(cities)

        for city in cities:
            url = functions.create_json_link_from_city_url(city.domain_name)
            yield Request(url, callback=self.parse_category_tree, city=city)

    def filter_cities(self, cities: list[spi.City]) -> list[spi.City]:
        out = []
        for city in cities:
            if city.name in _city_mask:
                out.append(city)
        return out

    def parse_category_tree(self, reply: Reply, **kwargs):
        """keyword arguments:
            city: City """
        item_city = kwargs['city']

        category_tree = functions.parse_catalog(reply)
        yield data.City(item_city.name, reply.request.url,
                        category_tree.root_item.children)


class LeroyMerlinCategoriesDataExtractor(DataExtractor):
    name = _name

    def __init__(self, start_urls: list[str]):
        super().__init__()
        self.start_urls = start_urls

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url, callback=self.process_first_page)

    def process_first_page(self, reply: Reply, **kwargs):
        url_parsed = urlparse(reply.request.url)
        page_count = functions.parse_page_count(reply)

        for product in self.parse_products(reply):
            yield product

        for page_num in range(2, page_count + 1):
            new_url = url_parsed._replace(query=urlencode({'page': page_num}))
            url = urlunparse(new_url)
            yield Request(url, callback=self.parse_products)

    def parse_products(self, reply: Reply, **kwargs):
        for product in functions.parse_products(reply):
            yield product
