import math

import bs4
from bs4 import BeautifulSoup
from urllib.parse import urljoin
from collections import OrderedDict

import shop_parser.core.items as sp
import shop_parser.core.product_keys as product_keys
from shop_parser.network import Reply


# 1. Parse Categories from "https://vodopad.ru/sitemap/"


def parse_catalog(reply: Reply) -> sp.CategoryTree:
    soup = BeautifulSoup(reply.text, 'html.parser')
    tree = sp.CategoryTree()
    root_node = soup.find('ul', class_='map-level-0')
    level_1_nodes = root_node.find_all('li', recursive=False)
    for level_1_node in level_1_nodes:
        name = level_1_node.find('a').text
        url = level_1_node.find('a').get('href')
        level_1_item = sp.CategoryTreeItem(name, urljoin(reply.url, url), tree.root_item)

        try:
            level_2_nodes = level_1_node.find('ul').find_all('li', recursive=False)
            for level_2_node in level_2_nodes:
                name = level_2_node.find('a').text
                url = level_2_node.find('a').get('href')
                level_2_item = sp.CategoryTreeItem(name, urljoin(reply.url, url), level_1_item)
                try:
                    level_3_nodes = level_2_node.find('ul').find_all('li', recursive=False)

                    for level_3_node in level_3_nodes:
                        name = level_3_node.find('a').text
                        url = level_3_node.find('a').get('href')
                        level_3_item = sp.CategoryTreeItem(name, urljoin(reply.url, url), level_2_item)
                except AttributeError:
                    pass
        except AttributeError:
            pass

    return tree


def parse_page_count(reply) -> int:
    soup = BeautifulSoup(reply.text, 'html.parser')
    try:
        pagination_node = soup.find('div', class_='pagination-row').find('div', class_='pn-pagers')
        page_buttons = pagination_node.find_all('a')
        return int(page_buttons[-1].text.strip())
    except AttributeError:
        return 1


def parse_products(reply) -> list[OrderedDict]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    products = []

    category = soup.find('ul', class_='block-breadcrumbs')
    category = ' > '.join([item.text for item in category.find_all('li')][1:])

    items = soup.find_all('div', class_='list-filter-result__item')
    for item in items:
        try:
            product = parse_product_item(item, category, reply)
            products.append(product)
        except AttributeError:
            pass
        except ValueError:
            pass
    table_items = soup.find_all('div', class_='list-filter-result-table__item')
    for item in table_items:
        try:
            products += parse_product_table(item, category, reply)
        except AttributeError:
            pass
        except ValueError:
            pass

    return products


def parse_product_item(item: bs4.Tag, category: str, reply) -> OrderedDict:
    name = item.find('div', class_='list-filter-result__name').text.strip()
    url = item.find('div', class_='list-filter-result__name').find('a').get('href')
    vendor_code = item.find('a', class_='prdct-artcl').text.replace('Код товара:', '').strip()

    price = item.find('span', class_='list-filter-result-price__valid').text
    price = beautify_price_string(price)
    price_old = item.find('span', class_='list-filter-result-price__old').text
    price_old = beautify_price_string(price_old)

    return convert_to_product(vendor_code,
                              name,
                              price,
                              urljoin(reply.url, url),
                              category,
                              price_old)


def parse_product_table(item: bs4.Tag, category: str, reply) -> list[OrderedDict]:
    base_name = item.find('div', class_='list-filter-result-table__name').text.strip()
    out = []
    table_body_node = item.find('tbody')
    table_rows = table_body_node.find_all('tr')

    for row in table_rows:
        vendor_code = row.find('a', class_='filter-table-charact__code-link').text.strip()
        url = row.find('a', class_='filter-table-charact__code-link').get('href')
        attribs = row.find('td', class_='list-filter-result-table__body-chars')
        if attribs:
            attribs = attribs.text.strip()
        price = row.find('span', class_='prc-charact').text
        price = beautify_price_string(price)
        price_old = item.find('span', class_='list-filter-result-price__old')
        if price_old:
            price_old = beautify_price_string(price_old.text)

        name = base_name + f' ({attribs})' if attribs else base_name
        product = convert_to_product(vendor_code,
                                     name,
                                     price,
                                     urljoin(reply.url, url),
                                     category,
                                     price_old)
        out.append(product)

    return out


def beautify_price_string(price: str) -> str:
    return price.replace('₽', '').replace(' ', '').strip()


def convert_to_product(vendor_code, name, price, url, category, price_old) -> OrderedDict:
    if price_old:
        return product_keys.convert_to_product(vendor_code,
                                               category,
                                               name,
                                               float(price_old),
                                               url,
                                               float(price))
    else:
        return product_keys.convert_to_product(vendor_code,
                                               category,
                                               name,
                                               float(price),
                                               url)
