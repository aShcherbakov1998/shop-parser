from shop_parser.core import DataExtractor
from shop_parser.network import Request, Reply
import shop_parser.core.items as spi
from shop_parser.core import data
from shop_parser.parsers.vodopad import functions
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode

_name = 'Vodopad'
_url = 'https://vodopad.ru'


class VodopadShopInfoDataExtractor(DataExtractor):
    name = _name

    def start_requests(self):
        yield Request('https://vodopad.ru/sitemap/', callback=self.parse_category_tree, city=spi.City("Все города"))

    def parse_category_tree(self, reply: Reply, **kwargs):
        """keyword arguments:
            city: City """
        item_city = kwargs['city']

        category_tree = functions.parse_catalog(reply)
        yield data.City(item_city.name, reply.request.url,
                        category_tree.root_item.children)


class VodopadCategoriesDataExtractor(DataExtractor):
    name = _name

    def __init__(self, start_urls: list[str]):
        super().__init__()
        self.start_urls = start_urls

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url+'?VIEW=LINE', callback=self.process_first_page)

    def process_first_page(self, reply: Reply, **kwargs):
        url_parsed = urlparse(reply.request.url)
        query_params = dict(parse_qsl(url_parsed.query))

        page_count = functions.parse_page_count(reply)
        for product in self.parse_products(reply):
            yield product

        for page_num in range(2, page_count+1):
            query_params.update({'PAGEN_1': page_num})
            new_url = url_parsed._replace(query=urlencode(query_params))
            url = urlunparse(new_url)
            yield Request(url, callback=self.parse_products)

    def parse_products(self, reply: Reply, **kwargs):
        for product in functions.parse_products(reply):
            yield product
