from shop_parser.core.extractableparser import ExtractableShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.parsers import vodopad


class VodopadParser(ExtractableShopParser):
    def __init__(self):
        super().__init__(
            name='Водопад',
            url='https://vodopad.ru',
            logger_name='VodopadParser')

    def _create_shop_info_data_extractor(self) -> DataExtractor:
        return vodopad.VodopadShopInfoDataExtractor()

    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        urls = [category_url]
        return vodopad.VodopadCategoriesDataExtractor(urls)
