from typing import Optional

from bs4 import BeautifulSoup
from collections import OrderedDict

from shop_parser.network import Reply
import shop_parser.core.items as sp
import shop_parser.core.product_keys as product_keys


def parse_cities(reply: Reply) -> list[sp.City]:
    out = []
    soup = BeautifulSoup(reply.text, 'html.parser')

    try:
        city_nodes = soup.find('div', class_='store-switcher')
        city_nodes = city_nodes.find('ul', class_='dropdown-select')
        city_nodes = city_nodes.find_all('li', class_='shop-switcher-item')

        for city_node_index in range(1, len(city_nodes)):
            try:
                city_node = city_nodes[city_node_index]
                city_id_node = city_node.find('span', class_='shop-switcher-item__name')

                city_id = city_id_node.get('data-id').strip()
                city_name = city_id_node.text.strip()

                out.append(sp.City(city_name, city_id))
            except AttributeError:
                continue
    except AttributeError:
        pass

    return out


def parse_catalog(reply: Reply) -> sp.CategoryTree:
    out = sp.CategoryTree()
    soup = BeautifulSoup(reply.text, 'html.parser')

    try:
        root_node = soup.find('nav', class_='top-menu')
        root_node = root_node.find('ol', class_='nav-primary')
        category_nodes = root_node.find_all('li', class_='menu-category', recursive=False)
        for category_node in category_nodes:
            try:
                category_url_node = category_node.find('a', class_='js-category-link')
                category_url = category_url_node.get('href')
                category_name = category_url_node.text.strip()
                category_tree_item = sp.CategoryTreeItem(category_name, category_url)

                category_node = category_node.find('div', class_='category-list-wrapper')
                category_node = category_node.find('ul', class_='js-category-list', recursive=False)
                subcategory_nodes = category_node.find_all('li', class_='menu-category')
                for subcategory_node in subcategory_nodes:
                    try:
                        subcategory_node = subcategory_node.find('a', class_='category-link')
                        subcategory_url = subcategory_node.get('href')
                        subcategory_name = subcategory_node.text.strip()
                        sp.CategoryTreeItem(subcategory_name, subcategory_url, category_tree_item)
                    except AttributeError:
                        continue

                if category_tree_item.children:
                    category_tree_item.parent = out.root_item
            except AttributeError:
                continue
    except AttributeError:
        pass

    return out


def parse_page_count(reply: Reply) -> int:
    out = 1
    soup = BeautifulSoup(reply.text, 'html.parser')

    try:
        pagination = soup.find('div', class_='pager')
        pagination = pagination.find('div', class_='pages')
        pagination = pagination.find('ol')
        pagination = pagination.find_all('li', recursive=False)
        if len(pagination) > 2:
            last_page_node = pagination[-2]
            out = int(last_page_node.text.strip())
    except AttributeError:
        pass

    return out


def parse_products(reply: Reply) -> list[OrderedDict]:
    out = []
    soup = BeautifulSoup(reply.text, 'html.parser')

    try:
        category_node = soup.find('div', class_='breadcrumbs')
        category_node = category_node.find('ul', attrs={'itemscope': ''})
        category_nodes = category_node.find_all('li', class_='breadcrumbs__item', recursive=False)
        category = __get_category_name_from_category_nodes(category_nodes)

        product_node_groups = soup.find_all('ul', class_='products-grid')
        for product_nodes in product_node_groups:
            product_nodes = product_nodes.find_all('li', class_='product-card', recursive=False)
            for product_node in product_nodes:
                try:
                    product_url_node = product_node.find('a', class_='ga-product-card-name')
                    url = product_url_node.get('href').strip()
                    name = product_url_node.text.strip()
                    price = __get_price_from_product_node(product_node)
                    vendor_code = __get_vendor_code_from_product_node(product_node)

                    product = product_keys.convert_to_product(
                        vendor_code,
                        category,
                        name,
                        price,
                        url
                    )

                    out.append(product)
                except AttributeError:
                    continue
    except AttributeError:
        pass

    return out


def __get_category_name_from_category_nodes(category_nodes: list) -> str:
    categories = []

    for i in range(1, len(category_nodes)):
        try:
            category_node = category_nodes[i]
            category_node = category_node.find('span', attrs={'itemprop': 'name'})
            categories.append(category_node.text)
        except AttributeError:
            continue

    if len(categories) > 1:
        if categories[-1] == categories[-2]:
            categories.pop()

    return ' > '.join(categories)


def __get_price_from_product_node(product_node) -> Optional[float]:
    out = None

    try:
        node = product_node.find('div', attrs={'data-equal-height': 'price'})
        node = node.find('div', class_='price-wrapper')
        node = node.find('div', class_='price-inner')
        node = node.find('div', class_='current-price')
        node = node.find('div', class_='price-box')
        node = node.find('span', class_='regular-price')
        node = node.find('span', class_='price')
        node = node.find_all('span')
        if len(node) > 1:
            price = node[1].text.strip()
            price = price.replace(' ', '')
            out = float(price)
    except AttributeError:
        pass

    return out


def __get_vendor_code_from_product_node(product_node) -> str:
    out = ''
    try:
        class_index = 3
        vendor_code = product_node.get('class')
        if len(vendor_code) > class_index:
            vendor_code = vendor_code[class_index]
            vendor_code = vendor_code.replace('sku-', '')
            out = vendor_code
    except AttributeError:
        pass

    return out
