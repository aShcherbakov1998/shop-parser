from shop_parser.core.extractableparser import ExtractableShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.parsers import castorama


class CastoramaParser(ExtractableShopParser):
    def __init__(self):
        super().__init__(
            name='Касторама',
            url='https://www.castorama.ru/',
            logger_name='CastoramaParser')

    def _create_shop_info_data_extractor(self) -> DataExtractor:
        return castorama.CastoramaShopInfoDataExtractor()

    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        urls = [category_url]
        return castorama.CastoramaCategoriesDataExtractor(urls, city_name)
