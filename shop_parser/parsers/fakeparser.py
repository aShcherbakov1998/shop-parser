import time
from collections import OrderedDict

import shop_parser.core.parser
import shop_parser.core.data
import shop_parser.core.items
import shop_parser.core.product_keys as product_keys


class FakeShopParser(shop_parser.core.parser.ShopParser):
    def __init__(self, shop_name: str = 'fake_shop', sleep_time: float = 0.1):
        super().__init__()
        self.shop_name = shop_name
        self.sleep_time = sleep_time

        self.product_index = 1
        self.product_price = 0.5
        self.product_price_step = 0.1
        self.product_url_format = 'https://z0r.de/products/{0}'
        self.product_name_format = 'Product {0}'
        self.product_vendor_code_format = '{0}'

    def parse_shop(self, url: str) -> shop_parser.core.data.Shop:
        out = get_fake_shop()
        out.name = self.shop_name

        sleep_time = self.sleep_time * 3
        time.sleep(sleep_time)

        return out

    def _parse_categories(self, shop: shop_parser.core.data.Shop):
        category_generator = self._make_category_generator_from_shop(shop)

        for category, city in category_generator:
            if self.interrupted:
                break

            if self._is_category_parsed(city, category):
                continue

            self._parse_category(city.name, category)
            self._mark_category_as_parsed(city, category)

    def _make_category_generator_from_shop(self, shop: shop_parser.core.data.Shop):
        for city in shop.cities:
            # print(f'  City name: {city.name}')
            generator = self._make_category_generator(city.categories)
            for category in generator:
                yield category, city

    def _make_category_generator(self, categories: list[shop_parser.core.data.Category]):
        for category in categories:
            if not category.children:
                yield category

            for child_category in self._make_category_generator(category.children):
                yield child_category

    def _parse_category(self, city_name: str, category: shop_parser.core.data.Category):
        name = self.product_name_format.format(self.product_index)
        vendor_code = self.product_vendor_code_format.format(self.product_index)
        url = self.product_url_format.format(self.product_index)
        price = self.product_price
        category = category.name
        price_on_sale = price * 0.90
        product = product_keys.convert_to_product(vendor_code, category, name, price, url, price_on_sale)

        self._mark_product_as_ready(city_name, product)
        self.product_index += 1
        self.product_price += self.product_price_step

        time.sleep(self.sleep_time)


def get_fake_shop() -> shop_parser.core.data.Shop:
    city1_category_names = ['category1', 'category2', 'category3']
    city1_subcategory_names = ['subcategory1_3', 'subcategory1_2', 'subcategory1_1']

    city2_category_names = ['category4', 'category5', 'category6']
    city2_subcategory_names = ['subcategory5_1', 'subcategory5_2', 'subcategory5_3']

    # City 1
    city1_categories = make_categories_from_strings(city1_category_names)
    city1_categories[0].children = make_categories_from_strings(city1_subcategory_names)
    city1_categories[1].children = make_categories_from_string('subcategory2_1')
    city1_categories[2].children = make_categories_from_string('subcategory3_1')

    city1 = shop_parser.core.data.City()
    city1.name = 'city1'
    city1.categories = city1_categories

    # City 2
    city2_categories = make_categories_from_strings(city2_category_names)
    city2_categories[0].children = make_categories_from_string('subcategory4_1')
    city2_categories[1].children = make_categories_from_strings(city2_subcategory_names)
    city2_categories[2].children = make_categories_from_string('subcategory6_1')

    city2 = shop_parser.core.data.City()
    city2.name = 'city2'
    city2.categories = city2_categories

    # City 3
    city3_categories = make_categories_from_strings(['category1', 'category5', 'category7'])
    city3_categories[0].children = make_categories_from_strings(city1_subcategory_names)
    city3_categories[1].children = make_categories_from_strings(city2_subcategory_names)
    city3_categories[1].children[0].children = make_categories_from_string('subcategory5_1_1')
    city3_categories[2].children = make_categories_from_string('subcategory7_1')

    city3 = shop_parser.core.data.City()
    city3.name = 'city3'
    city3.categories = city3_categories

    # Shop
    shop = shop_parser.core.data.Shop()
    shop.name = 'shop1'
    shop.cities = [city3, city2, city1]

    return shop


def make_category_from_string(s: str) -> shop_parser.core.data.Category:
    out = shop_parser.core.data.Category()
    out.name = s

    return out


def make_categories_from_string(s: str) -> list[shop_parser.core.data.Category]:
    return [make_category_from_string(s)]


def make_categories_from_strings(strings: list[str]) -> list[shop_parser.core.data.Category]:
    out = []
    for s in strings:
        out.append(make_category_from_string(s))

    return out
