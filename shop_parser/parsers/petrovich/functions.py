import math

from bs4 import BeautifulSoup
from urllib.parse import urljoin
from collections import OrderedDict

import shop_parser.core.items as sp
import shop_parser.core.product_keys as product_keys


# Petrovich is a huge shop based in a large number of regions throughout Russia.
# The most common regions can be obtained from the main page.
# IMPORTANT: the list of parsed cities would not include those, from which the request was sent.
#   That is why you need to get the last cityID from reply.url.

# 1. Request start page and parse cities (this time it is ready-to-go links, like https//moscow.petrovich.ru/)
# 2. For each city parse catalog from main page
# 3. For endpoint category calculate pagination
#       (total number of items can be parsed, as well as number of items on page
#       can be calculated. This time there is no 'last page')
# 4. For each page parse products info

# Vocabulary:
# ?p={num} - pagination

def parse_cities(response):
    soup = BeautifulSoup(response.text, 'html.parser')
    regions_container = soup.find('ul', class_='top_region_items')
    regions = []
    current_region_node = regions_container.find('span', class_='top_region_items_item_link_selected')
    current_name = current_region_node.text
    current_url = response.url
    regions.append(sp.City(current_name, current_url))

    for node in regions_container.find_all('li'):
        info = node.find('a')
        if info:
            name = info.text
            url = urljoin(response.url, info.get('href'))
            regions.append(sp.City(name, url))

    return regions


def parse_catalog(response):
    soup = BeautifulSoup(response.text, 'html.parser')
    catalog_node = soup.find('div', class_='sections-menu-content')
    tree = sp.CategoryTree()

    top_categories = catalog_node.find_all('a', recursive=False)
    top_categories_content = catalog_node.find_all('div', class_='masonry')
    for i in range(len(top_categories)):
        top = top_categories[i]
        content = top_categories_content[i]

        name = top.find('p').text
        url = urljoin(response.url, top.get('href'))
        top_item = sp.CategoryTreeItem(name, url, tree.root_item)

        subsections = content.find_all('div', class_='subsection')
        for sub in subsections:
            name = sub.find('a').text
            url = urljoin(response.url, sub.find('a').get('href'))
            sub_item = sp.CategoryTreeItem(name, url, top_item)

            sub_children = sub.find_all('li', class_='subsection-child')
            for child in sub_children:
                name = child.find('a').text
                url = urljoin(response.url, child.find('a').get('href'))
                sub_child_item = sp.CategoryTreeItem(name, url, sub_item)

    return tree


def parse_page_count(response) -> int:
    soup = BeautifulSoup(response.text, 'html.parser')
    products_total_node = soup.find('p', class_='md-down-hide')
    total_products_num = int(products_total_node.text.split()[-1])

    product_nodes = soup.find_all('div', class_='page-item-list')
    products_shown = len(product_nodes)
    return math.ceil(total_products_num / products_shown)


def parse_products(response) -> list[OrderedDict]:
    soup = BeautifulSoup(response.text, 'html.parser')
    product_nodes = soup.find_all('div', class_='page-item-list')

    category = ' > '.join([tag.text for tag in soup.find_all('li', property='itemListElement')][1:])
    products = []
    for node in product_nodes:
        try:
            name = node.find('a', class_='title').text
            price_with_card = node.find('p', class_='gold-price').text
            price_with_card = beautify_price_string(price_with_card)

            retail_price = node.find('p', class_='retail-price').text
            retail_price = beautify_price_string(retail_price)

            vendor_code = node.get('data-item-code')
            url = urljoin(response.url, node.find('a').get('href'))
            product = convert_to_product(vendor_code,
                                         category,
                                         name,
                                         retail_price,
                                         price_with_card,
                                         url)
            products.append(product)
        except AttributeError:
            pass

    return products


def beautify_price_string(price: str) -> float:
    # input: 5 390 Р
    price = price.strip()
    price = ''.join(price.split()[:-1])
    price = float(price.replace(',', '.'))
    return price


def convert_to_product(vendor_code: str,
                       category: str,
                       name: str,
                       retail_price: float,
                       price_with_card: float,
                       url: str) -> OrderedDict:
    out = OrderedDict()
    out[product_keys.VENDOR_CODE] = vendor_code
    out[product_keys.CATEGORY] = category
    out[product_keys.NAME] = name
    out[product_keys.PRICE] = retail_price
    out['Цена по карте'] = price_with_card
    out[product_keys.URL] = url
    return out
