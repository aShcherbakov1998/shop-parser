from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode

from shop_parser.core import DataExtractor
from shop_parser.core import data

from shop_parser.network import Request, Reply
from shop_parser.parsers.obi import functions
import shop_parser.core.items as spi

_name = 'OBI'
_url = 'https://www.obi.ru/'

city_IDs = [
    spi.City('Москва', '037'),
    spi.City('Санкт-Петербург', '005')
]


def _form_cookies(store_id: str) -> dict:
    return {'geomarket': store_id, 'obi_storeid': store_id, 'external_storeid': store_id}


class ObiShopInfoDataExtractor(DataExtractor):
    name = _name
    url = _url

    def start_requests(self):
        for city in city_IDs:
            yield Request('https://www.obi.ru/sitemap/', callback=self.parse_category_tree,
                          cookies=_form_cookies(city.domain_name), city=city)

    def parse_category_tree(self, reply: Reply, **kwargs):
        """keyword arguments:
            city: City """
        spi_city = kwargs['city']

        category_tree = functions.parse_catalog_tree(reply)
        yield data.City(spi_city.name, reply.request.url,
                        category_tree.root_item.children)


class ObiCategoriesDataExtractor(DataExtractor):
    name = _name

    def __init__(self, start_urls: list[str], city_name: str):
        super().__init__()
        self.start_urls = start_urls
        for city in city_IDs:
            if city.name == city_name:
                self.store_id = city.domain_name

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url, callback=self.parse_downlevel_categories,
                          cookies=_form_cookies(self.store_id))

    def parse_downlevel_categories(self, reply: Reply, **kwargs):
        categories_urls = functions.parse_downlevel_categories(reply)
        for url in categories_urls:
            yield Request(url, callback=self.process_first_page,
                          cookies=_form_cookies(self.store_id))

    def process_first_page(self, reply: Reply, **kwargs):
        url_parsed = urlparse(reply.request.url)
        query_params = dict(parse_qsl(url_parsed.query))

        page_count = functions.parse_page_count(reply)

        for product in self.parse_products(reply):
            yield product

        for page_num in range(2, page_count + 1):
            query_params.update({'page': page_num})
            new_url = url_parsed._replace(query=urlencode(query_params))
            url = urlunparse(new_url)
            yield Request(url, callback=self.parse_products,
                          cookies=_form_cookies(self.store_id))

    def parse_products(self, reply: Reply, **kwargs):
        for product in functions.parse_products(reply):
            yield product
