from shop_parser.core.extractableparser import ExtractableShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.parsers import obi


class ObiParser(ExtractableShopParser):
    def __init__(self):
        super().__init__(
            name='ОБИ',
            url='https://www.obi.ru/',
            logger_name='ObiParser')

    def _create_shop_info_data_extractor(self) -> DataExtractor:
        return obi.ObiShopInfoDataExtractor()

    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        urls = [category_url]
        return obi.ObiCategoriesDataExtractor(urls, city_name)
