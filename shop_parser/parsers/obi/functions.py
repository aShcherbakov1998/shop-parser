from urllib.parse import urljoin
from bs4 import BeautifulSoup
from collections import OrderedDict

from shop_parser.network import Reply
import shop_parser.core.items as sp
import shop_parser.core.product_keys as product_keys


# Unlike other shops, Obi does not have division by cities.
# The catalog you can see on it's pages presents all products available in Obi.
# The product can be highlighted gray if it is absent in current store, but it is
# always present in at least one other Obi store.
# Availability in stores can be obtained from target product page.

# As prices differ from city to city, wee need to open session to each city store
# and hold cookies.
# This is how store is changed:
# (https://www.obi.ru/store/change?storeID=014&redirectUrl=https%3A%2F%2Fwww.obi.ru%2Fmoyushaya-tekhnika%2Fpylesosy-i-aksessuary%2Fc%2F3104)

# List of stores can be obtained from https://www.obi.ru/gipermarket/
# Convenient-to-parse catalog: https://www.obi.ru/sitemap/

# Preparation:
# 1. List of stores need to be hardcoded (just one store from each city)
# 2. Parse catalog from: https://www.obi.ru/sitemap/

# Main parsing:
# 3. Parse down-level categories from obtained in [2], because sometimes there
#     are no products under [2] (Example: https://www.obi.ru/sad-i-dosug/sadovaya-tekhnika/c/862)
# 4. Parse pagination
# 5. Parse product info from the page with pagination.

# Vocabulary:
# page --> /?page={}
# storeID={}
# redirectUrl={}

def parse_catalog_tree(reply: Reply) -> sp.CategoryTree:
    soup = BeautifulSoup(reply.text, 'html.parser')
    tree = sp.CategoryTree()

    toplevel_category_nodes = soup.find_all('section', class_='cms-module cm-sitemap-list')

    for node in toplevel_category_nodes:
        name = node.find('a').text.strip()
        url = node.find('a').get('href')
        if url == 'https://www.obi.ru/sitemap/#':
            continue
        toplevel_item = sp.CategoryTreeItem(name, url, tree.root_item)

        downlevel_category_nodes = node.find_all('li')
        for node in downlevel_category_nodes:
            name = node.find('span', class_='link__txt')
            url = node.find('a').get('href')
            if name:
                name = name.text.strip()
                downlevel_item = sp.CategoryTreeItem(name, url, toplevel_item)

    return tree


# return list of urls
def parse_downlevel_categories(reply: Reply) -> list[str]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    urls = []
    node_container = soup.find('div', class_='box box-light article-navigation')
    nodes = node_container.find_all('li')

    for node in nodes:
        urls.append(node.find('a').get('href'))

    return urls


def parse_page_count(reply: Reply) -> int:
    soup = BeautifulSoup(reply.text, 'html.parser')
    pagination_bar = soup.find('a', class_='pagination-bar__link-refs js-pagination-link-refs')
    text = pagination_bar.text.strip()  # "Страница 1 из 20"
    num_pages = int(text.split()[-1])
    return num_pages


def parse_products(reply: Reply) -> list[OrderedDict]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    product_cards = soup.find_all('li', class_='product large')
    products = []

    category = soup.find('ul', itemscope='itemscope').find_all(lambda tag: tag.name == 'a' and tag.has_attr('wt_name'))
    category = list(OrderedDict.fromkeys([item.text for item in category]))
    category = ' > '.join(category[1:])  # remove 'Главная страница"
    for card in product_cards:
        info_container = card.find('span', class_='info-container')
        try:  # if there is an ad, it is stored in same way as product, but fields "name", "price" etc. are empty
            name = info_container.find('span', class_='description').text.strip()
            url = card.find('a').get('href')
            vendor_code = url.split('/')[-1]
            price_info = info_container.find('span', class_='price').text.strip()   # Example: '899,00 ₽ / M2459,00 ₽ / M2'
            price_info = price_info.replace('/ М2', '').replace(' ', '').replace(',', '.').replace('\xa0', '')  # Example: '899.00₽459.00₽'
            price_info = price_info.split('₽') # Example: ['899.00', '459.00']
            price = float(price_info[0])
            price_on_sale = float(price_info[1]) if len(price_info) > 1 and price_info[1] else None

            product = product_keys.convert_to_product(
                vendor_code, category, name, price, urljoin(reply.url, url), price_on_sale)
            products.append(product)
        except:
            pass

    return products
