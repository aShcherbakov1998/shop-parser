from bs4 import BeautifulSoup
from urllib.parse import urljoin
from selenium.webdriver.remote.webdriver import WebDriver, By
from collections import OrderedDict

from shop_parser.network import Reply
import shop_parser.core.items as sp
import shop_parser.core.product_keys as product_keys
from shop_parser.network import headlessdriver


def parse_cities(reply: Reply) -> list[sp.City]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    cities = soup.find('div', class_='af-location-cities').find_all('a')
    out = []
    for node in cities:
        name = node.text
        id = node.get('data-location')
        out.append(sp.City(name, id))
    return out


def parse_catalog(reply: Reply) -> sp.CategoryTree:
    tree = sp.CategoryTree()
    soup = BeautifulSoup(reply.text, 'html.parser')

    top_level_categories = soup.find_all('li', class_='af-menu-item__promo')
    for top_level_node in top_level_categories:
        name = top_level_node.find('a').text.replace('promo', '')
        url = top_level_node.find('a').get('href')

        parent_item = sp.CategoryTreeItem(name, urljoin(reply.url, url), tree.root_item)

        child_nodes = top_level_node.find_all('li', class_='af-menu-item')
        for child in child_nodes:
            name = child.text
            url = child.find('a').get('href')
            it = sp.CategoryTreeItem(name, urljoin(reply.url, url), parent_item)

    return tree


def parse_page_count(reply: Reply) -> int:
    soup = BeautifulSoup(reply.text, 'html.parser')
    paginator_node = soup.find('div', class_='paginator')
    if not paginator_node:
        return 1
    visible_pages = [node.text.strip() for node in paginator_node.find_all('a')]
    page_count = max(map(int, visible_pages))
    return page_count


def parse_products(reply: Reply) -> list[OrderedDict]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    product_cards = soup.find_all('div', class_='item item-wrapper')

    category = soup.find('ul', class_='breadcrumb-navigation')
    category = category.find_all('li', itemprop='itemListElement')
    category = ' > '.join([item.text for item in category][1:])

    products = []
    for product_card in product_cards:
        try:
            name = product_card.find('div', class_='name').text.strip()
            url = product_card.find('a').get('href')
            # have 'code chain' tag and then 'code'
            vendor_code = product_card.find_all('div', class_='code')[-1].text  # Код: 15945
            vendor_code = vendor_code.split()[-1].strip()
            try:  # if has price with sale
                price = product_card.find('span', class_='cur-price').text  # 17 236       руб.
                price = float(''.join(price.split()[:-1]).replace(',', '.'))  # '17236'
                price_on_sale = product_card.find('span', class_='base old').text
                price_on_sale = float(''.join(price_on_sale.split()[:-1]).replace(',', '.'))
                products.append(
                    product_keys.convert_to_product(vendor_code, category, name, price, urljoin(reply.url, url),
                                                    price_on_sale))
            except:
                price = product_card.find('div', class_='js-price-current').text  # Цена 18 500    руб.
                price = float(''.join(price.split()[1:-1]).replace(',', '.'))
                products.append(
                    product_keys.convert_to_product(vendor_code, category, name, price, urljoin(reply.url, url)))
        except:
            pass
    return products


def show_js_cities(webdriver: WebDriver):
    el = webdriver.find_element(By.CSS_SELECTOR, 'a[class="af-location-city JSLocationOpen"]')
    el.click()


def show_all_categories_js(webdriver: WebDriver):
    # if there is an add or other shit, that doens not  allow me to click on
    #  buttons, I just better remove all thresh from  the DOM-document
    headlessdriver.remove_all_elements_except('#left_menu', webdriver)

    while True:
        hidden_elements = webdriver.find_elements(by=By.CSS_SELECTOR,
                                                  value='li[class="af-menu-item af-menu-item__promo hide_child"]')
        if not hidden_elements:
            return
            # This strange thing is done because when js executes, the page DOM is modified, so hidden_elements can
            # be not valid anymore. The reversed is iterated, because items are extended to bottom, and this way the
            # page is more stable.
        for el in reversed(hidden_elements):
            webdriver.execute_script("arguments[0].scrollIntoView();", el)
            el.find_element(by=By.CLASS_NAME, value="af-menu-icon").click()


