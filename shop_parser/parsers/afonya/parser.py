from shop_parser.core.extractableparser import ExtractableShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.parsers import afonya


class AfonyaParser(ExtractableShopParser):
    def __init__(self):
        super().__init__(
            name='Афоня',
            url='https://www.afonya-spb.ru/',
            logger_name='AfonyaParser')

    def _create_shop_info_data_extractor(self) -> DataExtractor:
        return afonya.AfonyaShopInfoDataExtractor()

    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        urls = [category_url]
        return afonya.AfonyaCategoriesDataExtractor(urls)
