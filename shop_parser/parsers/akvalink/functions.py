from bs4 import BeautifulSoup
from urllib.parse import urljoin
from collections import OrderedDict

from shop_parser.network import Reply
import shop_parser.core.items as sp
import shop_parser.core.product_keys as product_keys


# /?cityId={}
# /?setPage={}  - number of items visible (max = 48)
# /?PAGEN_1={}
# !!Attention!! Not each user-agent is ok for this shop. Use fixed for example:
# user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.36 (KHTML, like Gecko)
# Chrome/54.0.2866.71 Safari/537.36'

def parse_cities(reply: Reply) -> list[sp.City]:
    pass


def parse_catalog_tree(reply: Reply) -> sp.CategoryTree:
    soup = BeautifulSoup(reply.text, 'html.parser')
    tree = sp.CategoryTree()
    catalog_node = soup.find('div', class_='catalog-section-list').find('ul')
    level_1_nodes = catalog_node.find_all('li', class_='selectedtopmenuli', recursive=False)
    for level_1_node in level_1_nodes:
        name = level_1_node.find('a').text.strip()
        url = level_1_node.find('a').get('href')
        level_1_item = sp.CategoryTreeItem(name, urljoin(reply.url, url), tree.root_item)

        level_2_wrapper_node = level_1_node.find('ul')
        level_2_nodes = level_2_wrapper_node.find_all('li', class_='selectedtopmenuli', recursive=False)

        for level_2_node in level_2_nodes:
            name = level_2_node.find('a').text.strip()
            url = level_2_node.find('a').get('href')
            level_2_item = sp.CategoryTreeItem(name, urljoin(reply.url, url), level_1_item)

            level_3_wrapper = level_2_node.find('ul')
            if not level_3_wrapper:
                continue
            level_3_nodes = level_3_wrapper.find_all('li', class_='selectedtopmenuli')

            for level_3_node in level_3_nodes:
                name = level_3_node.find('a').text.strip()
                url = level_3_node.find('a').get('href')
                if name == 'Смесители':
                    continue
                level_3_item = sp.CategoryTreeItem(name, urljoin(reply.url, url), level_2_item)
    return tree


def parse_page_count(reply: Reply) -> int:
    soup = BeautifulSoup(reply.text, 'html.parser')
    pagintaion_node = soup.find('div', class_='bx-pagination')
    if not pagintaion_node:
        return 1
    buttons = pagintaion_node.find_all('li')
    last_page = buttons[-2]
    return int(last_page.text.strip())


def parse_products(reply: Reply) -> tuple[list[OrderedDict], list[str]]:
    """Parses products and returns Ordered dict with products as a first parameter,
        and a list of urls to be parsed separately as a second parameter."""

    soup = BeautifulSoup(reply.text, 'html.parser')
    category = __parse_category(soup)

    products = []
    urls = []
    product_nodes = soup.find_all('div', class_='row_item')
    for node in product_nodes:
        try:
            url = node.find('a').get('href')
            if node.find('div', class_='select-offers'):
                urls.append(urljoin(reply.url, url))
                continue
            name = node.find('div', class_='product_name').text.strip()
            vendor_code = node.find('div', class_='bx_catalog_item_articul').find('p').text
            vendor_code = vendor_code.replace('Артикул', '').strip()
            price = node.find_all('span', class_='price')
            price_with_sale = None
            if len(price) == 1:
                price = float(price[0].text.replace(' ', '').strip())
            else:
                old = None
                new = None
                for p in price:
                    at_l = p.get_attribute_list('class')
                    if 'old' in at_l:
                        old = float(p.text.replace(' ', '').strip())
                    elif 'new' in at_l:
                        new = float(p.text.replace(' ', '').strip())
                price = old
                price_with_sale = new
            products.append(product_keys.convert_to_product(vendor_code,
                                                            category,
                                                            name,
                                                            price,
                                                            urljoin(reply.url, url),
                                                            price_with_sale))
        except AttributeError:
            pass
    return products, urls


def parse_product(reply: Reply) -> list[OrderedDict]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    products = []
    category = __parse_category(soup)
    name = soup.find('h1', class_='product-name-h1').text.strip()
    vendor_code = soup.find('div', class_='props active').find_all('tr')
    vendor_code = list(filter(lambda tag: 'Артикул' in tag.text, vendor_code))
    vendor_code = vendor_code[0].text.replace('Артикул', '').strip()
    offers_node = soup.find('div', itemprop='offers')
    characteristics = offers_node.find_all('div', class_='characteristic-item')
    for c in characteristics:
        item_type = c.find('a', class_='picularities').text.strip()
        url = c.find('a', class_='picularities').get('href')
        unique_code = url.split('/')[-2]
        price = c.find('meta', itemprop='price').get('content')
        products.append(product_keys.convert_to_product(vendor_code=f'{vendor_code}_{unique_code}',
                                                        category=category,
                                                        name=f'{name} {item_type}',
                                                        price=float(price),
                                                        url=urljoin(reply.url,url)))
    return products


def __parse_category(soup: BeautifulSoup) -> str:
    category = soup.find('div', class_='breadcrumbs').find_all('span')
    category = [item.text.strip() for item in category]
    return '>'.join(list(OrderedDict.fromkeys(category)))
