from shop_parser.core import DataExtractor
from shop_parser.network import Request, Reply
import shop_parser.core.items as spi
from shop_parser.core import data
from shop_parser.parsers.akvalink import functions
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode

_name = 'Akvalink'
_url = 'https://akvalink.ru/'
_user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.36 (KHTML, like Gecko) ' \
              'Chrome/54.0.2866.71 Safari/537.36 '


class AkvalinkShopInfoDataExtractor(DataExtractor):
    name = _name

    def start_requests(self):
        yield Request(_url, callback=self.parse_category_tree, user_agent=_user_agent,
                      city=spi.City('Все города'))

    def parse_category_tree(self, reply: Reply, **kwargs):
        """keyword arguments:
            city: City """
        item_city = kwargs['city']

        category_tree = functions.parse_catalog_tree(reply)
        yield data.City(item_city.name, reply.request.url,
                        category_tree.root_item.children)


class AkvalinkCategoriesDataExtractor(DataExtractor):
    name = _name

    def __init__(self, start_urls: list[str]):
        super().__init__()
        self.start_urls = start_urls

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url, callback=self.process_first_page, user_agent=_user_agent)

    def process_first_page(self, reply: Reply, **kwargs):
        url_parsed = urlparse(reply.request.url)
        query_params = dict(parse_qsl(url_parsed.query))

        page_count = functions.parse_page_count(reply)

        for request_or_product in self.parse_products(reply):
            yield request_or_product

        for page_num in range(2, page_count + 1):
            query_params['PAGEN_1'] = page_num
            new_url = url_parsed._replace(query=urlencode(query_params))
            url = urlunparse(new_url)
            yield Request(url, callback=self.parse_products, user_agent=_user_agent)

    def parse_products(self, reply: Reply, **kwargs):
        products, urls = functions.parse_products(reply)
        for p in products:
            yield p
        for url in urls:
            yield Request(url, callback=self.parse_product_from_product_page,
                          user_agent=_user_agent)

    def parse_product_from_product_page(self, reply: Reply, **kwargs):
        product_offers = functions.parse_product(reply)
        for p in product_offers:
            yield p
