from shop_parser.core.extractableparser import ExtractableShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.parsers import akvalink


class AkvalinkParser(ExtractableShopParser):
    def __init__(self):
        super().__init__(
            name='Аквалинк',
            url='https://akvalink.ru/',
            logger_name='AkvalinkParser')

    def _create_shop_info_data_extractor(self) -> DataExtractor:
        return akvalink.AkvalinkShopInfoDataExtractor()

    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        urls = [category_url]
        return akvalink.AkvalinkCategoriesDataExtractor(urls)
