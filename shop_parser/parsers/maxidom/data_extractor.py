from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode

from shop_parser.core import DataExtractor
from shop_parser.core import data

from shop_parser.network import Request, Reply
import shop_parser.network.url

from shop_parser.parsers.maxidom import functions


_name = 'Maxidom'
_url = 'https://www.maxidom.ru/'

# TODO: workaround?
_cities = []


class MaxidomShopInfoDataExtractor(DataExtractor):
    name = _name
    url = _url

    def start_requests(self):
        yield Request(_url, callback=self.parse_cities)

    def parse_cities(self, reply: Reply, **kwargs):
        # TODO: workaround?
        global _cities
        cities = functions.parse_cities(reply)
        _cities = cities

        if not cities:
            yield

        for city in cities:
            yield Request(
                _url,
                callback=self.parse_category_tree,
                cookies=_form_cookies(city.domain_name),
                city=city,
            )

    def parse_category_tree(self, reply: Reply, **kwargs):
        """keyword arguments:
            city: City """
        item_city = kwargs['city']

        category_tree = functions.parse_catalog(reply)
        yield data.City(
            item_city.name,
            reply.request.url,
            category_tree.root_item.children
        )


class MaxidomCategoriesDataExtractor(DataExtractor):
    name = _name

    def __init__(self, start_urls: list[str], city_name: str):
        super().__init__()
        self.start_urls = start_urls

        # TODO: workaround?
        for city in _cities:
            if city.name == city_name:
                self.city_id = city.domain_name

    def start_requests(self):
        for url in self.start_urls:
            yield Request(
                url,
                callback=self.process_first_page,
                cookies=_form_cookies(self.city_id),
            )

    def process_first_page(self, reply: Reply, **kwargs):
        urls = functions.parse_subcategory_urls(reply)
        if not urls:
            yield Request(
                _expand_url_product_limit(reply.request.url),
                callback=self.process_first_product_page,
                cookies=_form_cookies(self.city_id)
            )
        for url in urls:
            url = _expand_url_product_limit(url)
            yield Request(
                url,
                callback=self.process_first_product_page,
                cookies=_form_cookies(self.city_id)
            )

    def process_first_product_page(self, reply: Reply, **kwargs):
        page_count = functions.parse_page_count(reply)
        for product in self.parse_products(reply):
            yield product

        for page_index in range(2, page_count + 1):
            url = _set_url_product_page(reply.request.url, page_index)
            yield Request(
                url,
                callback=self.parse_products,
                cookies=_form_cookies(self.city_id),
            )

    def parse_products(self, reply: Reply, **kwargs):
        for product in functions.parse_products(reply):
            yield product


def _form_cookies(store_id: str) -> dict:
    return {
        'MAXI_LOC_ID': store_id
    }


def _expand_url_product_limit(url) -> str:
    params = {
        'amount': 100
    }

    return shop_parser.network.url.update_params(url, params)


def _set_url_product_page(url, page_index) -> str:
    params = {
        'PAGEN_3': page_index
    }

    return shop_parser.network.url.update_params(url, params)
