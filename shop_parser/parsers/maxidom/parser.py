from shop_parser.core.extractableparser import ExtractableShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.parsers import maxidom


class MaxidomParser(ExtractableShopParser):
    def __init__(self):
        super().__init__(
            name='Максидом',
            url='https://www.maxidom.ru/',
            logger_name='MaxidomParser')

    def _create_shop_info_data_extractor(self) -> DataExtractor:
        return maxidom.MaxidomShopInfoDataExtractor()

    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        urls = [category_url]
        return maxidom.MaxidomCategoriesDataExtractor(urls, city_name)
