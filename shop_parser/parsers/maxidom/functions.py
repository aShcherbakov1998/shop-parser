from bs4 import BeautifulSoup
from urllib.parse import urljoin, urlparse, urlencode, parse_qsl, urlunparse
from collections import OrderedDict

from shop_parser.core import items
from shop_parser.network import Reply
import shop_parser.core.product_keys as product_keys


# We use cookies now

# Lifehack how to use website without cookies. As the site does not store city identificator in it's url
# we need somehow to let it know what city we are interested in when making GET request.
# This is done as simply as adding "/?repIDchanged=3" to the url, where '3' is the id of the city.
# Example: https://www.maxidom.ru/catalog/drevesno-plitnye-materialy/?repIDchanged=3

# Another interesting thing is that when parsing items list from the page it is possible to set the number
# of items shown on the page. Without cookies this is done by adding /?amount=100 or whatever number you desire
# Example https://www.maxidom.ru/catalog/drevesno-plitnye-materialy/&amount=10

# Combination of parameters is passed via '&' so that finite url would look like below:
# Example: https://www.maxidom.ru/catalog/drevesno-plitnye-materialy/?repIDchanged=3&amount=10
# This can be easily done with scrapy.FormRequest(url=url, method='GET', formdata=params,...)
# where 'formdata' is the dict with desired params

# Parameters in use:
# repIDchanged={int} - city id
# amount={int} - number of products on the page
# PAGEN_3={int} - page number


def parse_cities(reply: Reply) -> list[items.City]:
    out = []

    soup = BeautifulSoup(reply.text, 'html.parser')
    try:
        cities_node = soup.find('ul', class_='cities-list')

        for node in cities_node.find_all('a', class_="cities-list__link"):
            try:
                name = node.text.strip()
                url = str(node.get('href'))  # example: https://www.maxidom.ru/?repIDchanged=2
                reprId = url.split('/')[-1]  # example: ?reprIDchanged=2
                id = reprId[-1]  # just value '2'
                city = items.City(name, id)
                out.append(city)
            except AttributeError:
                # print(f'[parse_cities, for] url: {reply.url}')
                continue
    except AttributeError:
        # print(f'[parse_cities] url: {reply.url}')
        pass

    return out


def parse_catalog(reply: Reply) -> items.CategoryTree:
    out = items.CategoryTree()
    soup = BeautifulSoup(reply.text, 'html.parser')

    try:
        root_node = soup.find('nav', class_='new-nav-search')
        category_nodes = root_node.find_all('li', class_='li-parent')
        for category_node in category_nodes:
            try:
                category_tree_item = __get_category_tree_item(reply.url, category_node)
                subcategory_nodes = __get_subcategory_nodes(category_node)
            except AttributeError:
                # print(f'[parse_catalog, item] url: {reply.url}')
                continue
            for subcategory_node in subcategory_nodes:
                try:
                    subcategory_item = __get_subcategory_tree_item(category_tree_item, reply.url, subcategory_node)
                except AttributeError:
                    # print(f'[parse_catalog, sub_item] url: {reply.url}')
                    continue

            if category_tree_item.children:
                category_tree_item.parent = out.root_item
    except AttributeError:
        # print(f'[parse_catalog] url: {reply.url}')
        pass

    return out


# parameter "PAGEN_3={int}"
def parse_page_count(reply: Reply) -> int:
    soup = BeautifulSoup(reply.text, 'html.parser')
    pagination_node = soup.find('ul', class_='ul-cat-pager')
    page_count = 1
    if pagination_node:
        last_page_node = pagination_node.find_all('li')[-1]
        last_page_url = last_page_node.find('a').get('href')
        page_count = int(last_page_url.split('=')[-1])
    return page_count


def parse_products(reply: Reply) -> list[OrderedDict]:
    out = []
    soup = BeautifulSoup(reply.text, 'html.parser')

    try:
        category_name = __get_category_name(soup)
        product_nodes = __get_product_nodes(soup)
        for product_node in product_nodes:
            try:
                product = __get_product(product_node, reply.url, category_name)
                if not product:
                    continue

                out.append(product)
            except AttributeError:
                # print(f'[parse_products, for] url: {reply.url}')
                continue
    except AttributeError:
        # print(f'[parse_products] url: {reply.url}')
        pass

    return out


def parse_subcategory_urls(reply: Reply) -> list[str]:
    out = []
    soup = BeautifulSoup(reply.text, 'html.parser')

    try:
        url_nodes = soup.find('nav', class_='nav-filter')
        url_nodes = url_nodes.find('ul')
        url_nodes = url_nodes.find_all('li')
        for url_node in url_nodes:
            try:
                url = url_node.find('a').get('href')
                if not url:
                    continue

                url = urljoin(reply.url, url)
                out.append(url)
            except AttributeError:
                # print(f'[parse_subcategory_urls, for] url: {reply.url}')
                continue
    except AttributeError:
        # print(f'[parse_subcategory_urls] url: {reply.url}')
        pass

    return out


def __get_category_tree_item(reply_url, category_node) -> items.CategoryTreeItem:
    url_node = category_node.find('div', class_='p_1')
    url_node = url_node.find('a', class_='parent_a')
    url = url_node.get('href')
    url = urljoin(reply_url, url)

    category_url_text_node = url_node.find('span')
    name = category_url_text_node.text.strip()

    return items.CategoryTreeItem(name, url)


def  __get_subcategory_nodes(category_node) -> list:
    subcategory_nodes = category_node.find('div', class_='wrap-submenu')
    if not subcategory_nodes:
        return []

    subcategory_nodes = subcategory_nodes.find('ul', class_='new-ul-child')
    return subcategory_nodes.find_all('li')


def __get_subcategory_tree_item(parent_item, reply_url, subcategory_node) -> items.CategoryTreeItem:
    subcategory_node = subcategory_node.find('a', class_='child_a')
    url = subcategory_node.get('href')
    url = urljoin(reply_url, url)
    name = subcategory_node.text.strip()

    return items.CategoryTreeItem(name, url, parent_item)


def __get_category_name(node) -> str:
    category_names = []
    node = node.find('div', class_='wrap-breadcrumbs')
    node = node.find('div', class_='maxi_container')
    node = node.find('ul', class_='breadcrumbs')
    category_nodes = node.find_all('li', itemprop='itemListElement')
    for category_node_index in range(2, len(category_nodes)):
        category_node = category_nodes[category_node_index]
        category_node = category_node.find('a', itemprop='item')
        category_node = category_node.find('span', itemprop='name')

        category_name = category_node.text.strip()
        category_names.append(category_name)

    return ' > '.join(category_names)


def __get_product_nodes(node) -> str:
    out = node.find('div', class_='item-list-inner')
    out = out.find_all('article', itemtype='http://schema.org/Product')

    return out


def __get_product(product_node, reply_url, category_name: str) -> OrderedDict:
    info_node = product_node.find('div', class_='caption-list')
    vendor_code = __get_product_vendor_code(info_node)

    title_node = info_node.find('a', class_='name-big')
    name = __get_product_name(title_node)
    url = __get_product_relative_url(title_node)
    url = urljoin(reply_url, url)

    price_node = product_node.find('div', class_='wrap-buy')
    price_node = price_node.find('div', class_='bl_prd_price')
    price_node = price_node.find('span', class_='price-list')
    price = __get_product_price(price_node)

    out = product_keys.convert_to_product(
        vendor_code,
        category_name,
        name,
        price,
        url,
    )

    return out


def __get_product_name(node) -> str:
    out = node.text.strip()
    if not out:
        return ''

    if out[-1] == '"':
        out = out[:len(out)]

    if out[0] == '"':
        out = out[1:]

    out = out.strip()
    out = ' '.join(out.split())     # Remove more than one space in the string
    return out


def __get_product_vendor_code(node) -> str:
    vendor_code_sku_index = 1
    vendor_code_token_index = 2
    out = ''

    node = node.find('div', class_='small-top')
    sku_nodes = node.find_all('small', class_='sku')
    if len(sku_nodes) <= vendor_code_sku_index:
        return out

    sku_node = sku_nodes[vendor_code_sku_index]
    tokens = sku_node.text.strip()
    tokens = tokens.split(' ')
    if len(tokens) <= vendor_code_token_index:
        return out

    out = tokens[vendor_code_token_index]
    return out


def __get_product_relative_url(node) -> str:
    out = node.get('href')
    return out


def __get_product_price(node) -> float:
    node = node.find('span', itemprop='price')
    out = node.text.strip()

    return float(out)
