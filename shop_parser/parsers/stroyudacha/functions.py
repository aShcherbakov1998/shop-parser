from typing import Optional, Tuple

from bs4 import BeautifulSoup
from collections import OrderedDict

from shop_parser.network import Reply
import shop_parser.core.items as sp
import shop_parser.core.product_keys as product_keys

from . import product_keys as stroyudacha_product_keys
from urllib.parse import urljoin


def parse_catalog(reply: Reply) -> sp.CategoryTree:
    soup = BeautifulSoup(reply.text, 'html.parser')
    tree = sp.CategoryTree()
    catalog_node = soup.find('ul', id='yw0')
    level_1_nodes = catalog_node.find_all('li', class_='level1')

    for level_1_node in level_1_nodes:
        name = level_1_node.find('a').text.strip()
        url = level_1_node.find('a').get('href')
        level_1_item = sp.CategoryTreeItem(name, urljoin(reply.url, url), tree.root_item)
        level_2_nodes = level_1_node.find_all('li', class_='level2')
        for level_2_node in level_2_nodes:
            name = level_2_node.find('a').text.strip()
            url = level_2_node.find('a').get('href')
            level_2_item = sp.CategoryTreeItem(name, urljoin(reply.url, url), level_1_item)
            level_3_nodes = level_2_node.find_all('li', class_='level3')
            for level_3_node in level_3_nodes:
                name = level_3_node.find('a').text.strip()
                url = level_3_node.find('a').get('href')
                level_3_item = sp.CategoryTreeItem(name, urljoin(reply.url, url), level_2_item)
    return tree


def parse_product_urls(reply: Reply) -> list[str]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    out = []
    product_container = soup.find('div', class_='data')
    product_nodes = product_container.find_all('div', class_='item')

    for product in product_nodes:
        url = product.find('a').get('href')
        out.append(urljoin(reply.url, url))

    return out


def parse_product(reply: Reply) -> Optional[OrderedDict]:
    out = None
    soup = BeautifulSoup(reply.text, 'html.parser')

    try:
        category_name = __get_category_name(soup)

        product_node = soup.find('div', attrs={'id': 'product_page'})
        vendor_code = __get_vendor_code(product_node)
        name = __get_name(product_node)
        url = reply.url
        price, price_on_sale = __get_price_and_price_on_sale(product_node)
        bar_code = __get_bar_code(product_node)
        out = product_keys.convert_to_product(
            vendor_code,
            category_name,
            name,
            price,
            url,
            price_on_sale
        )

        out[stroyudacha_product_keys.BAR_CODE] = bar_code
    except AttributeError:
        pass

    return out


def __get_vendor_code(product_node) -> str:
    node = product_node.find('div', attrs={'id': 'price_buy'}, recursive=True)
    node = node.find('div', class_='scode')
    text = node.text.strip()

    tokens = text.split(' ')
    if not tokens:
        return ''

    out = tokens[-1]
    return out


def __get_category_name(node) -> str:
    node = node.find('div', class_='full_width')
    node = node.find('div', class_='breadcrumbs')
    category_nodes = node.find_all('div', class_='breadcrumbs_link_div')

    category_names = []
    for i in range(1, len(category_nodes)):
        category_node = category_nodes[i]
        text = category_node.text.strip()
        if text:
            category_names.append(text)

    return ' > '.join(category_names)


def __get_name(product_node) -> str:
    node = product_node.find('h1', attrs={'id': 'product_header'})
    out = node.text.strip()

    return out


def __get_price_and_price_on_sale(product_node) -> Tuple[float, Optional[float]]:
    price_node = product_node.find('div', attrs={'id': 'price_buy'})
    old_price_node = price_node.find('div', class_='product_item_unit_old_price')
    current_price_node = price_node.find('div', attrs={'id': 'product_price'})

    old_price = __get_price(old_price_node)
    current_price = __get_price(current_price_node)

    price = None
    price_on_sale = None
    if old_price:
        price = old_price
        price_on_sale = current_price
    else:
        price = current_price

    return price, price_on_sale


def __get_price(price_node) -> Optional[float]:
    if not price_node:
        return None

    price_node = price_node.find('span', class_='price_number')
    out = price_node.get('content')
    if not out:
        return None

    out = out.strip()
    out = float(out)
    return out


def __get_bar_code(product_node) -> str:
    node = product_node.find('div', attrs={'id': 'product_description'})
    nodes = node.find_all('div', class_='product_properties_row')
    if not nodes:
        return ''

    bar_node = nodes[-1]
    bar_node = bar_node.find('div', class_='product_properties_value')
    return bar_node.text.strip()
