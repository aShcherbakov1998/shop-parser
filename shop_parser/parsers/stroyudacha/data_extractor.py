from shop_parser.core import DataExtractor
from shop_parser.core import data
import shop_parser.core.items as spi

from shop_parser.network import Request, Reply
import shop_parser.network.url
from shop_parser.parsers.stroyudacha import functions


_name = 'Stroyudacha'
_url = 'https://stroyudacha.ru/'


class StroyudachaShopInfoDataExtractor(DataExtractor):
    name = _name
    url = _url

    def start_requests(self):
        yield Request(
            _url,
            callback=self.parse_category_tree,
            city=spi.City('Все города'),
        )

    def parse_category_tree(self, reply: Reply, **kwargs):
        city = kwargs['city']

        category_tree = functions.parse_catalog(reply)
        yield data.City(
            city.name,
            reply.request.url,
            category_tree.root_item.children,
        )


class StroyudachaCategoriesDataExtractor(DataExtractor):
    name = _name

    def __init__(self, start_urls: list[str]):
        super().__init__()
        self.start_urls = start_urls

    def start_requests(self):
        for url in self.start_urls:
            url = _expand_product_count(url)
            yield Request(
                url,
                callback=self.process_page_with_products,
            )

    def process_page_with_products(self, reply: Reply, **kwargs):
        urls = functions.parse_product_urls(reply)
        for url in urls:
            yield Request(
                url,
                callback=self.parse_product,
            )

    def parse_product(self, reply: Reply, **kwargs):
        yield functions.parse_product(reply)


def _expand_product_count(url) -> str:
    request_page = 1000
    params = {
        'page': request_page
    }

    return shop_parser.network.url.update_params(url, params)
