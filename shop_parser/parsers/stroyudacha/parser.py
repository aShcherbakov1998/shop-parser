from shop_parser.core.extractableparser import ExtractableShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.parsers import stroyudacha


class StroyudachaParser(ExtractableShopParser):
    def __init__(self):
        super().__init__(
            name='Стройудача',
            url='https://stroyudacha.ru/',
            logger_name='StroyudachaParser')

    def _create_shop_info_data_extractor(self) -> DataExtractor:
        return stroyudacha.StroyudachaShopInfoDataExtractor()

    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        urls = [category_url]
        return stroyudacha.StroyudachaCategoriesDataExtractor(urls)
