from shop_parser.core import DataExtractor
from shop_parser.network import Request, Reply
import shop_parser.core.items as spi
from shop_parser.core import data
from shop_parser.parsers.baucenter import functions
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode

_name = 'Baucenter'
_url = 'https://baucenter.ru/'

_cities = []


class BaucenterShopInfoDataExtractor(DataExtractor):
    name = _name

    def start_requests(self):
        yield Request(_url, callback=self.parse_cities)

    def parse_cities(self, reply: Reply, **kwargs):
        global _cities
        cities = functions.parse_cities(reply)
        _cities = cities

        for city in cities:
            yield Request('https://baucenter.ru/search/map/',
                          cookies={'BITRIX_SM_CITY_ID': city.domain_name},
                          callback=self.parse_category_tree, city=city)

    def parse_category_tree(self, reply: Reply, **kwargs):
        """keyword arguments:
            city: City """
        item_city = kwargs['city']

        category_tree = functions.parse_catalog_tree(reply)
        yield data.City(item_city.name, reply.request.url,
                        category_tree.root_item.children)


class BaucenterCategoriesDataExtractor(DataExtractor):
    name = _name

    def __init__(self, start_urls: list[str], city_name: str):
        super().__init__()
        self.start_urls = start_urls
        for city in _cities:
            if city.name == city_name:
                self.city_id = city.domain_name

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url, cookies={'BITRIX_SM_CITY_ID': self.city_id},
                          callback=self.parse_subcategories)

    def parse_subcategories(self, reply:Reply, **kwargs):
        urls = functions.parse_downlevel_categories_urls(reply)
        for url in urls:
            url += '?count=36'
            yield Request(url, cookies={'BITRIX_SM_CITY_ID': self.city_id},
                          callback=self.process_first_page)

    def process_first_page(self, reply: Reply, **kwargs):
        url_parsed = urlparse(reply.request.url)
        query_params = dict(parse_qsl(url_parsed.query))

        page_count = functions.parse_page_count(reply)

        for product in self.parse_products(reply):
            yield product

        for page_num in range(2, page_count + 1):
            query_params['PAGEN_1'] = page_num
            new_url = url_parsed._replace(query=urlencode(query_params))
            url = urlunparse(new_url)
            yield Request(url, cookies={'BITRIX_SM_CITY_ID': self.city_id},
                          callback=self.parse_products)

    def parse_products(self, reply: Reply, **kwargs):
        for product in functions.parse_products(reply):
            yield product
