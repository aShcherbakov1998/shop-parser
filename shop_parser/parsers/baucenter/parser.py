from shop_parser.core.extractableparser import ExtractableShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.parsers import baucenter


class BaucenterParser(ExtractableShopParser):
    def __init__(self):
        super().__init__(
            name='Бауцентр',
            url='https://baucenter.ru/',
            logger_name='BaucenterParser')

    def _create_shop_info_data_extractor(self) -> DataExtractor:
        return baucenter.BaucenterShopInfoDataExtractor()

    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        urls = [category_url]
        return baucenter.BaucenterCategoriesDataExtractor(urls, city_name)
