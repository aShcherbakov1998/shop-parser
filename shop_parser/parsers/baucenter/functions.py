from bs4 import BeautifulSoup
from urllib.parse import urljoin
from collections import OrderedDict

from shop_parser.network import Reply
import shop_parser.core.items as sp
import shop_parser.core.product_keys as product_keys


# catalog can be obtained from 'https://baucenter.ru/search/map/'
# product count max 36 : /?count=36
# page: PAGEN_1={}
# city (cookie): BITRIX_SM_CITY_ID={}

def parse_cities(reply: Reply) -> list[sp.City]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    cities = []
    nodes = soup.find_all('li', class_='city-dropdown_item')

    for node in nodes:
        name = node.text.strip()
        city_id = node.find('a').get('data-id').strip()
        cities.append(sp.City(name, city_id))
    return cities


def parse_catalog_tree(reply: Reply) -> sp.CategoryTree:
    soup = BeautifulSoup(reply.text, 'html.parser')
    tree = sp.CategoryTree()
    catalog_wrapper = soup.find_all('div', class_='sitemapWrapper js-bottom-nav_wrapper')[1]
    categories_groups = catalog_wrapper.find_all('nav', class_='bottom-nav')
    for group in categories_groups:
        name = group.find('h4').text.strip()
        url = group.find('a').get('href')
        toplevel_item = sp.CategoryTreeItem(name, urljoin(reply.url, url), tree.root_item)
        downlevel_nodes = group.find_all('a', class_='bottom-nav_item')
        for node in downlevel_nodes:
            name = node.text.strip()
            url = node.get('href')
            downlevel_item = sp.CategoryTreeItem(name, urljoin(reply.url, url), toplevel_item)
    return tree


def parse_downlevel_categories_urls(reply: Reply) -> list[str]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    out = []
    sidebar = soup.find('div', class_='catalog-sidebar_item_body')
    category_nodes = sidebar.find_all('li')
    for category in category_nodes:
        url = category.find('a').get('href')
        out.append(urljoin(reply.url, url))
    return out


def parse_page_count(reply: Reply) -> int:
    soup = BeautifulSoup(reply.text, 'html.parser')
    pagination = soup.find('nav', class_='pagination')
    if not pagination:
        return 1

    buttons = pagination.find_all('a', class_='pagination_button')
    buttons = list(filter(lambda b: b.text.strip(), buttons))  # remove empty lines
    return int(buttons[-1].text.strip())


def parse_products(reply: Reply) -> list[OrderedDict]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    products = []
    category = soup.find('ol', itemtype='http://schema.org/BreadcrumbList')
    category = category.find_all('li', itemprop='itemListElement')[1:]
    category = ' > '.join(item.text.strip() for item in category)
    items = soup.find_all('div', class_='catalog_item')
    for item in items:
        try:
            name = item.find('div', class_='catalog_item_heading h4').text.strip()
            url = item.find('a').get('href')
            vendor_code = item.find('div', class_='catalog_item_article').text.strip()  # ex: Артикул: 403002190
            vendor_code = vendor_code.split()[-1].strip()
            price = item.find('span', class_='price-block_price').text.strip()  # ex: 21 000.–           за шт
            price = ''.join(price.split()).replace('.–', ' ').split()[0]
            products.append(product_keys.convert_to_product(vendor_code, category, name, price, urljoin(reply.url, url)))
        except:
            pass

    return products
