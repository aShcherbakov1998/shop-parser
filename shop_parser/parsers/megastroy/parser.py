from shop_parser.core.extractableparser import ExtractableShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.parsers import megastroy


class MegastroyParser(ExtractableShopParser):
    def __init__(self):
        super().__init__(
            name='Мегастрой',
            url='https://megastroy.com/',
            logger_name='MegastroyParser')

    def _create_shop_info_data_extractor(self) -> DataExtractor:
        return megastroy.MegastroyShopInfoDataExtractor()

    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        urls = [category_url]
        return megastroy.MegastroyCategoriesDataExtractor(urls)
