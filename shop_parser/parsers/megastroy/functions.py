from bs4 import BeautifulSoup
from urllib.parse import urljoin
from collections import OrderedDict

from shop_parser.network import Reply
from shop_parser.core import items
import shop_parser.core.product_keys as product_keys


def parse_category_tree(reply: Reply) -> items.CategoryTree:
    def parse_item(tree_item_parent: items.CategoryTreeItem, **kwargs):
        soup_node = kwargs.get('soup_node')
        soup_items = soup_node.find_all('li', recursive=False)
        for soup_item in soup_items:
            item_info_node = soup_item.find('a')
            name = item_info_node.get('title')
            url = item_info_node.get('href')
            tree_item = items.CategoryTreeItem(name, urljoin(reply.url, url), tree_item_parent)
            next_level_node = soup_item.find('ul')
            if next_level_node:
                parse_item(tree_item, soup_node=next_level_node)

    soup = BeautifulSoup(reply.text, 'html.parser')
    root_catalog_node = soup.find('ul')  # don't know why soup.find('ul',class_='lev-1') does not work

    return items.CategoryTree.growUp(parse_item, soup_node=root_catalog_node)


def parse_page_count(reply: Reply) -> int:
    soup = BeautifulSoup(reply.text, 'html.parser')
    pagination_node = soup.find('ul', class_='pagination')

    out = 1
    if pagination_node is not None:
        page_nodes = pagination_node.find_all('li')
        if len(page_nodes) > 0:
            out = int(page_nodes[-1].text)

    return out


def parse_products_from_catalog(reply: Reply) -> list[OrderedDict]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    cards = soup.find_all('div', class_='product-prev')
    category_node = soup.find('ul', class_='crumbs clearfix')
    category_node = category_node.find_all('span', property='name')
    category = ' > '.join([node.text.strip() for node in category_node][1:])

    products = []
    for card in cards:
        try:
            image_node = card.find('div', class_='img')
            vendor_code = card.find('div', class_='code').find('span').text
            url = image_node.find('a').get('href')  # Example: '/products/238051'
            url = urljoin(reply.url, url)

            name = image_node.find('a').get('title')
            price = float(card.find('div', class_='price').find('b').text.replace(' ', ''))
            product = product_keys.convert_to_product(vendor_code, category, name, price, url)
            products.append(product)
        except:
            pass

    return products


def parse_categories_from_category_page(reply: Reply) -> list[str]:
    soup = BeautifulSoup(reply.text, 'html.parser')
    nodes = soup.find_all('div', class_='product-prev')

    urls = []
    for node in nodes:
        url = node.find('a').get('href')
        urls.append(urljoin(reply.url, url))
    return urls
