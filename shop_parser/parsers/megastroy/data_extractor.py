from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode

from shop_parser.core import DataExtractor
from shop_parser.core import data
import shop_parser.core.items as spi

from shop_parser.network import Request, Reply
from shop_parser.parsers.megastroy import functions

_name = "Megastroy"
_start_url_template = 'https://{}.megastroy.com'


# TODO: https://kazan.megastroy.com/catalog/plitka-dlya-vannoy?products=
#  1.   parse if products= is on the page.
#  2.   cookies show all products.


class MegastroyShopInfoDataExtractor(DataExtractor):
    name = _name
    start_url_template = _start_url_template
    city_IDs = [
        spi.City('Казань', 'kazan'),
        spi.City('Наб.Челны', 'chelny', ),
        spi.City("Ульяновск", 'ulyanovsk'),
        spi.City('Чебоксары', 'cheboksary'),
        spi.City('Саранск', 'saransk'),
        spi.City('Стерлитамак', 'sterlitamak'),
        spi.City('Йошкар-Ола', 'yoshkarola'),
    ]

    def start_requests(self):
        for city in self.city_IDs:
            url = self.start_url_template.format(city.domain_name)
            yield Request(url, callback=self.parse_category_tree, city=city)

    def parse_category_tree(self, reply: Reply, **kwargs):
        category_tree = functions.parse_category_tree(reply)
        spi_city = kwargs['city']
        yield data.City(
            name=spi_city.name,
            url=self.start_url_template.format(spi_city.domain_name),
            categories=category_tree.root_item.children
        )


class MegastroyCategoriesDataExtractor(DataExtractor):
    name = _name
    start_url_template = _start_url_template
    url_filters = ['/products', '/collection/']

    def __init__(self, start_urls: list[str] = []):
        super().__init__()
        self.start_urls = start_urls

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url, callback=self.process_first_page)

    def process_first_page(self, reply: Reply, **kwargs):
        url_parsed = urlparse(reply.url)
        query_params = dict(parse_qsl(url_parsed.query))

        requests = []
        try:
            for product in self.parse_products(reply, **kwargs):
                yield product
        except Exception:
            try:
                urls = functions.parse_categories_from_category_page(reply)
                for url in urls:
                    requests.append(Request(url, self.process_first_page))
            except Exception:
                self._logger.exception("message")
                self._logger.warning(f'Unable to parse {reply.url}')

        page_count = functions.parse_page_count(reply)
        for page_num in range(2, page_count):
            query_params.update({'page': page_num})
            new_url = url_parsed._replace(query=urlencode(query_params))
            url = urlunparse(new_url)
            requests.append(Request(url, callback=self.parse_products))
        for request in requests:
            if not any(url_filter in request.url for url_filter in self.url_filters):
                yield request

    def parse_products(self, reply: Reply, **kwargs):
        for product in functions.parse_products_from_catalog(reply):
            yield product
