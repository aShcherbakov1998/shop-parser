from bs4 import BeautifulSoup
from urllib.parse import urljoin
from collections import OrderedDict

from shop_parser.network import Reply
import shop_parser.core.items as sp
import shop_parser.core.product_keys as product_keys


def parse_cities(reply: Reply) -> list[sp.City]:
    out = []
    soup = BeautifulSoup(reply.text, 'html.parser')

    try:
        city_nodes = soup.find('div', class_='city-dropdown__group')
        city_nodes = city_nodes.find_all('a', class_='city-dropdown__item')
        for city_node in city_nodes:
            try:
                out.append(__get_city(city_node))
            except AttributeError:
                continue
    except AttributeError:
        pass

    return out


def parse_catalog(reply: Reply) -> sp.CategoryTree:
    out = sp.CategoryTree()
    soup = BeautifulSoup(reply.text, 'html.parser')

    try:
        root_node = soup.find('div', class_='catalog_section_list')
        category_nodes = root_node.find_all('div', class_='item_block', recursive=False)
        for category_node in category_nodes:
            try:
                main_category_node = category_node.find('li', class_='name')
                main_category_item = __create_main_category_item(reply.url, main_category_node, out.root_item)

                subcategory_nodes = category_node.find_all('li', class_='sect')
                for subcategory_node in subcategory_nodes:
                    __create_subcategory_item(reply.url, subcategory_node, main_category_item)
            except AttributeError:
                continue
    except AttributeError:
        pass

    return out


def parse_page_count(reply: Reply) -> int:
    out = 1
    soup = BeautifulSoup(reply.text, 'html.parser')

    try:
        pagination = soup.find('div', class_='pagination-block')
        page_nodes = pagination.find_all('a', class_='pagination-block__item')
        if len(page_nodes) > 1:
            page_node = page_nodes[-2]
            page_node = page_node.find('span')
            out = int(page_node.text.strip())

    except AttributeError:
        pass

    return out


def parse_products(reply: Reply) -> list[OrderedDict]:
    out = []
    soup = BeautifulSoup(reply.text, 'html.parser')

    try:
        category_name = __get_category_name(soup)

        product_group_node = soup.find('div', class_='product-list')
        product_nodes = product_group_node.find_all('div', 'product-list__card', recursive=False)
        for product_node in product_nodes:
            try:
                product_node = product_node.find('div', class_='product-card__container')
                vendor_code = __get_vendor_code_from_product_node(product_node)
                price = __get_price_from_product_node(product_node)

                url_node = product_node.find('a', class_='product-card__name')
                name = __get_name_from_url_node(url_node)
                url = __get_url_from_url_node(url_node)
                url = urljoin(reply.url, url)

                product = product_keys.convert_to_product(
                    vendor_code,
                    category_name,
                    name,
                    price,
                    url
                )

                out.append(product)
            except AttributeError:
                continue
    except AttributeError:
        pass

    return out


def __create_main_category_item(reply_url, main_category_node, parent_item):
    node = __get_category_node_with_link(main_category_node)
    name = node.find('span').text.strip()
    url = node.get('href')
    url = urljoin(reply_url, url)
    return sp.CategoryTreeItem(name, url, parent_item)


def __create_subcategory_item(reply_url, subcategory_node, parent_item):
    node = __get_category_node_with_link(subcategory_node)
    name = node.text.strip()
    url = node.get('href')
    url = urljoin(reply_url, url)
    return sp.CategoryTreeItem(name, url, parent_item)


def __get_category_node_with_link(node):
    return node.find('a', class_='dark_link')


def __get_vendor_code_from_product_node(product_node) -> str:
    node = product_node.find('div', class_='product-card__code')
    text = node.text.strip()
    values = text.split(' ')
    if len(values) < 2:
        return ''

    return values[1]


def __get_price_from_product_node(product_node) -> float:
    node = product_node.find('div', class_='product-card__price')
    text = node.get('data-price')

    return float(text)


def __get_name_from_url_node(url_node) -> str:
    text = url_node.text.strip()
    text_tokens = text.split(' ')
    if not text_tokens:
        return text

    # There is sometimes a vendor code at the beginning of the string.
    # We remove all the digits at the beginning
    digits_token = text_tokens[0]
    if not __is_int(digits_token):
        return text

    mid_pos = len(digits_token) + 1
    return text[mid_pos:]


def __get_url_from_url_node(url_node) -> str:
    return url_node.get('href')


def __get_category_name(node):
    category_class_node_id = 'breadcrumbs-modern__link'

    categories_node = node.find('div', class_='breadcrumbs-modern')
    category_nodes = categories_node.find_all('a', class_=category_class_node_id, recursive=True)
    current_category_node = categories_node.find('span', class_=category_class_node_id)
    if current_category_node:
        category_nodes.append(current_category_node)

    category_text_list = []
    for i in range(2, len(category_nodes)):
        category_node = category_nodes[i]
        category_node = category_node.find('span', attrs={'itemprop': 'name'})
        text = category_node.text.strip()
        category_text_list.append(text)

    return ' > '.join(category_text_list)


def __is_int(s: str) -> bool:
    if not s:
        return False

    for symbol in s:
        if not symbol.isdigit():
            return False

    return True


def __get_city(node) -> sp.City:
    city_name = node.text.strip()
    city_id = node.get('data-id')

    return sp.City(city_name, city_id)
