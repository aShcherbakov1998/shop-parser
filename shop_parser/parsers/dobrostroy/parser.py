from shop_parser.core.extractableparser import ExtractableShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.parsers import dobrostroy


class DobrostroyParser(ExtractableShopParser):
    def __init__(self):
        super().__init__(
            name='Добрострой',
            url='https://добрострой.рф/',
            logger_name='DobrostroyParser')

    def _create_shop_info_data_extractor(self) -> DataExtractor:
        return dobrostroy.DobrostroyShopInfoDataExtractor()

    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        urls = [category_url]
        return dobrostroy.DobrostroyCategoriesDataExtractor(urls, city_name)
