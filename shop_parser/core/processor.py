import logging
import traceback
import re
from pathlib import Path

from PySide6.QtCore import QObject, Signal

from shop_parser.network import NetworkManager, Request, Reply
from shop_parser.core import DataExtractor, DataManipulator, CACHE_DIRECTORY


class Processor(QObject):
    """This class manages all the process of web-scrapping.

    The Processor's responsibilities are:
       - Deliver requests-replies from *DataExtractor* to *NetworkManager* and vice-versa.
       - Control extracted data processing (save or pass somewhere forward).
       - Track the scrapping progress.

    Attributes:
    ----------
    *_network : NetworkManager*
        provides network connection.
    *_extractors : list[DataExtractor]*
        DataExtractor subclasses to scrape data from the web-page.
    *_kwargs_storage : list[dict]*
        a dictionary of user's kwargs that should be transferred to reply's callback.
    *_logger : Logger*
        a Logger instance to log the working process.

    Methods:
    --------
    *add_data_extractor(data_extractor: DataExtractor)*:
        add a DataExtractor's subclass to the process.
    *start()*:
        start the process of web-scrapping.
    *abort()*:
        abort the process of web-scrapping.

    **Private methods**:

    *_start_iteration(generator, **kwargs)*
        start iterating over DataExtractor's generator-method
    *_process_request_or_data(self, request_or_data, **kwargs)*
        process a yield value of the DataExtractor (is either Request or ____)   #TODO: or?
    *_process_request(self, request: Request, **kwargs)*
        deliver request to NetworkManager.
    *_process_reply(self, reply: Reply, **kwargs)*
        deliver reply to DataExtractor.
    """
    finished = Signal()

    def __init__(self):
        super().__init__()
        self._network = NetworkManager()
        self._extractors = []
        self._datamanipulator = DataManipulator()
        self._logger = logging.getLogger('Processor')

    def add_data_extractor(self, data_extractor: DataExtractor):
        """Adds a data_extractor to it's process.
        :param data_extractor: a subclass of DataExtactor.
        """
        if issubclass(type(data_extractor), DataExtractor):
            if not data_extractor.name:
                raise ValueError(f'No name specified in {data_extractor.__name__}.')
            self._extractors.append(data_extractor)
            self._logger.debug(f'Add data extractor "{data_extractor.name}".')
        else:
            raise ValueError("data_extractor should be a subclass of DataExtractor.")

    def set_data_manipulator(self, data_manipulator: DataManipulator):
        self._datamanipulator = data_manipulator

    def start(self):
        """Start the process of web-scrapping"""
        self._logger.debug('Start processing...')
        for data_extractor in self._extractors:
            self.__start_iteration(data_extractor.start_requests,
                                   extractor_name=data_extractor.name)

    def abort(self):
        """Abort the process of web-scrapping"""
        self._network.stop()
        self._logger.info('Abort has been called, stop processing...')
        self.finished.emit()

    def __start_iteration(self, generator, **kwargs):
        """Starts iteration for given generator object.
        :param generator: a generator object (DataExtractor method).

        *Keyword Arguments:*
           *reply: Reply* - if presents, then is transferred as
            a param to generator(). If not, than it is the start_requests()
        """
        reply = None
        try:
            if 'reply' in kwargs:
                reply = kwargs.pop('reply')
                for request_or_data in generator(reply, **reply.request.kwargs):
                    self.__process_request_or_data(request_or_data, **kwargs)
            else:  # probably start_requests
                for request_or_data in generator():
                    self.__process_request_or_data(request_or_data, **kwargs)
        except Exception as ex:
            self._logger.warning(f'{kwargs["extractor_name"]} throws exception "{ex}" '
                                 f'at {reply.url if reply else "start_requests"}')
            self.__save_reply(reply, extractor_name=kwargs['extractor_name'], exception=ex)

        if self.check_finifshed():
            self.finished.emit()

    def __process_request_or_data(self, request_or_data, **kwargs):
        if issubclass(type(request_or_data), Request):
            self.__process_request(request_or_data, **kwargs)
        # elif issubclass(request_or_data, DATA_BASE):
        # pass
        else:
            self.__process_data(request_or_data, **kwargs)

    def __process_request(self, request: Request, **kwargs):
        """Saves kwargs and sends a request with _process_reply callback"""
        extractor_kwargs = request.kwargs
        request.kwargs['__callback__'] = request.callback
        request.kwargs['extractor_name'] = kwargs['extractor_name']

        # TODO: currently only 'GET' is supported.
        request.callback = self.__process_reply
        self._network.get(request)

    def __process_reply(self, reply: Reply, **kwargs):
        """Finds extractors kwargs and starts_iteration in extractor's callback"""
        request = reply.request
        extractor_name = request.kwargs['extractor_name']
        reply.request.callback = request.kwargs['__callback__']

        if reply.successful():
            self._logger.debug(f'start iteration{extractor_name}')
            self.__start_iteration(request.callback, reply=reply, extractor_name=extractor_name)
            self._logger.debug(f'stop iteration{extractor_name}')
        else:
            if self.check_finifshed():
                self.finished.emit()

    def __process_data(self, data, **kwargs):
        self._datamanipulator.add(data, kwargs['extractor_name'])

    def check_finifshed(self) -> bool:
        # check if the whole process is finished
        finished = self._network.requests_in_process() == 0 and \
                   self._network.requests_in_queue() == 0
        
        return finished

    def __save_reply(self, reply: Reply, **kwargs):
        file_name = re.sub(r"[/\:*?«<>|.]", '', reply.url) + '.txt'
        file_dir = f'{CACHE_DIRECTORY}/error_pages/{kwargs["extractor_name"]}'
        Path(file_dir).mkdir(parents=True, exist_ok=True)
        self._logger.debug('start save file')
        with open(f'{file_dir}/{file_name}', 'w', encoding='utf-8') as file:
            file.write(f'address: {reply.url}\n\n')
            traceback.print_exc(file=file)
            file.write(reply.text)
        self._logger.debug('stop save file')
