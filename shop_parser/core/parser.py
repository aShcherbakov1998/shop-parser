import logging
from abc import abstractmethod
import threading
from collections import OrderedDict

from PySide6 import QtCore

import shop_parser.core.data
import shop_parser.core.items


# Base class for all shop parsers
class ShopParser(QtCore.QObject):
    progress_changed = QtCore.Signal(int)
    started = QtCore.Signal()
    finished = QtCore.Signal()

    def __init__(self):
        super().__init__()
        self.progress = 0
        self.interrupted = False
        self.on_product_ready_callback = None
        self.category_cache: set[str] = set()
        self.category_cache_lock = threading.Lock()
        self._logger = logging.getLogger("ShopParser")

    @abstractmethod
    def parse_shop(self, url: str) -> shop_parser.core.data.Shop:
        pass

    def parse_categories(self, shop: shop_parser.core.data.Shop, on_product_ready_callback: callable = None):
        self.interrupted = False
        self.progress = 0
        self.on_product_ready_callback = on_product_ready_callback

        self.started.emit()
        self._parse_categories(shop)
        self.finished.emit()

    def clear_category_cache(self):
        with self.category_cache_lock:
            self.category_cache.clear()

    def get_cache_entry_count(self):
        with self.category_cache_lock:
            return len(self.category_cache)

    def get_cache_entries(self):
        with self.category_cache_lock:
            return self.category_cache.copy()   # Copy to avoid data race

    def set_cache_entries(self, entries):
        with self.category_cache_lock:
            self.category_cache.clear()
            for entry in entries:
                self.category_cache.add(entry)

    def add_cache_entry(self, entry: str):
        with self.category_cache_lock:
            self.category_cache.add(entry)

    @abstractmethod
    def _parse_categories(self, shop: shop_parser.core.data.Shop):
        pass

    def get_iteration_count(self, shop: shop_parser.core.data.Shop) -> int:
        out = 0
        for city in shop.cities:
            out += self._get_category_count(city, city.categories)

        return out

    def _get_category_count(
            self,
            city: shop_parser.core.data.City,
            categories: list[shop_parser.core.data.Category]) -> int:
        out = 0
        for category in categories:
            if category.children:
                out += self._get_category_count(city, category.children)
            elif not self._is_category_parsed(city, category):
                out += 1

        return out

    def _mark_category_as_parsed(self, city: shop_parser.core.data.City, category: shop_parser.core.data.Category):
        if self.interrupted:
            self._logger.debug(f'mark category: process is interrupted')
            return

        self._logger.debug(f'mark category start {city.name, category.name}')
        category_cache_entry = self._get_category_cache_entry(city, category)
        if not self.__is_category_cache_entry_parsed(category_cache_entry):
            self.progress += 1
            self.progress_changed.emit(self.progress)

        self.add_cache_entry(category_cache_entry)
        # print(f'[Cache] Category added: "{category_cache_entry}" (size: {self.get_cache_entry_count()})')
        self._logger.debug(f'category cache add{city.name, category.name}')
        self.category_cache.add(category_cache_entry)
        self._logger.debug(f'mark category stop {city.name, category.name}')

    def _get_category_cache_entry(self, city: shop_parser.core.data.City, category: shop_parser.core.data.Category) -> str:
        out = ' '
        category_parent = category
        while category_parent is not None:
            name = category_parent.name
            category_parent = category_parent.parent

            if name == 'root':  # Root element has 'root' as its name. We don't need that in the cache
                continue

            out = ' ' + name + out

        out = city.name + out
        return out.rstrip()

    def _is_category_parsed(self, city: shop_parser.core.data.City, category: shop_parser.core.data.Category) -> bool:
        category_cache_entry = self._get_category_cache_entry(city, category)
        return self.__is_category_cache_entry_parsed(category_cache_entry)

    def __is_category_cache_entry_parsed(self, category_cache_entry: str) -> bool:
        with self.category_cache_lock:
            return category_cache_entry in self.category_cache

    def _mark_product_as_ready(self, city_name: str, product: OrderedDict):
        if self.on_product_ready_callback:
            self.on_product_ready_callback(city_name, product)
