import logging


class DataManipulator:
    def add(self, data, data_extractor_name: str, **kwargs):
        print(f'Got some data from {data_extractor_name} of type {type(data)}')


class DataHolder(DataManipulator):
    def __init__(self):
        super().__init__()
        self.data = {}

    def add(self, data, data_extractor_name: str, **kwargs):
        if data_extractor_name not in self.data:
            self.data[data_extractor_name] = []
        self.data[data_extractor_name].append((data, kwargs))


class CallbackDataManipulator(DataManipulator):
    def __init__(self, callback: callable):
        super().__init__()
        self.callback = callback

    def add(self, data, data_extractor_name: str, **kwargs):
        self.callback(data, data_extractor_name, **kwargs)
