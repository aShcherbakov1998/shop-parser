import logging
from abc import abstractmethod

from PySide6.QtCore import QEventLoop

from shop_parser.core.parser import ShopParser
from shop_parser.core.data_extractor import DataExtractor
from shop_parser.core import data, items, Processor, DataHolder, CallbackDataManipulator


class ExtractableShopParser(ShopParser):
    def __init__(self, name, url, logger_name='ExtractableShopParser'):
        super().__init__()
        self.name = name
        self.url = url
        self._logger = logging.getLogger(logger_name)

    def parse_shop(self, url: str) -> data.Shop:
        processor = Processor()
        data_extractor = self._create_shop_info_data_extractor()
        processor.add_data_extractor(data_extractor)

        data_holder = DataHolder()
        processor.set_data_manipulator(data_holder)

        event_loop = QEventLoop()
        processor.finished.connect(event_loop.quit)

        processor.start()
        if not processor.check_finifshed():
            event_loop.exec()
        cities_with_kwargs = data_holder.data.get(data_extractor.name)

        cities = [pair[0] for pair in cities_with_kwargs]
        shop_info = data.Shop(self.name, self.url, cities)

        return shop_info

    @abstractmethod
    def _create_shop_info_data_extractor(self) -> DataExtractor:
        pass

    @abstractmethod
    def _create_categories_data_extractor(self, category_url: str, city_name: str) -> DataExtractor:
        pass

    def _parse_categories(self, shop: data.Shop):
        for city, category in get_category_generator(shop):
            if self.interrupted:
                break
            elif self._is_category_parsed(city, category):
                continue

            self._logger.debug(f'parse category start {city.name, category.name}')
            self._parse_category(city.name, category)

            self._logger.debug(f'mark category start {city.name, category.name}')
            self._mark_category_as_parsed(city, category)

            self._logger.debug(f'parse category stop {city.name, category.name}')

    def _parse_category(self, city_name: str, category):
        data_extractor = self._create_categories_data_extractor(category.url, city_name)
        processor = Processor()
        processor.add_data_extractor(data_extractor)
        processor.set_data_manipulator(
            CallbackDataManipulator(
                lambda product, parser_name, **kwargs:
                self._mark_product_as_ready(city_name, product)))

        event_loop = QEventLoop()
        processor.finished.connect(event_loop.quit)
        processor.start()
        self._logger.debug(f'loop start')

        # TODO: see network-timer branch !!! 
        event_loop.exec()
        self._logger.debug(f'loop end')


def get_category_generator(shop: data.Shop):
    for city in shop.cities:
        category_tree = items.CategoryTree()
        category_tree.root_item.children = city.categories
        for category in category_tree.get_downlevel_items():
            yield city, category
