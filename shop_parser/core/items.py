from anytree import NodeMixin, RenderTree


class CategoryTreeItem(NodeMixin):
    def __init__(self, name: str = None, url: str = None, parent=None):
        super().__init__()
        self.name = name
        self.url = url
        self.parent = parent


class City:
    def __init__(self, name: str = None, domain_name: str = None):
        self.name = name
        # {domain_name}.site-name.com ---> this is not actually so easy..
        # sometimes (like leroy-merlin) city 'id' is present twice: '{id}.leroymerlin.com/{id}/product<...>'
        # furthermore this can be simple number, stored in cookies of the website (see megastroy.ru)
        # so let's rename it back into 'id' ?
        # > We'll look into it later...
        self.domain_name = domain_name  # id

    def __eq__(self, other):
        if isinstance(other, City):
            return self.name == other.name \
                   and self.domain_name == other.domain_name

        return NotImplemented

    def __repr__(self):
        return f'City(' \
               f'name: "{self.name}", ' \
               f'domain_name: {self.domain_name})'


"""Down are help structures."""


class CategoryTree:
    @staticmethod
    def growUp(item_generator, **kwargs):
        tree = CategoryTree()
        item_generator(tree.root_item, **kwargs)
        return tree

    def __init__(self):
        self.root_item = CategoryTreeItem('root')  # should never be modified

    def get_downlevel_items(self) -> list[CategoryTreeItem]:
        return self.get_filtered_items(lambda item: not item.children)

    def get_filtered_items(self, predicate):
        return self.__filter_items_recurse__(self.root_item, predicate)

    def __filter_items_recurse__(self, item: CategoryTreeItem, predicate) -> list[CategoryTreeItem]:
        filtered = [item] if predicate(item) and item is not self.root_item else []

        for item in item.children:
            filtered += self.__filter_items_recurse__(item, predicate)
        return filtered

    def print(self):
        for pre, fill, node in RenderTree(self.root_item):
            print(f'{pre} {node.name} {node.url}')
