import logging

from shop_parser.network import Request, Reply


class DataExtractor:
    """This class is to be used as a Base class for all your web data extraction needs.

    The process of web-scraping is similar to well-known python Scrapy package.
    DataExtractor is a generator-like class that yields Requests or Data that are
    caught by the Processor class. The callback, yielded in Request is called when
    Reply comes.

    Attributes:
    -----------
    *name : str*
        a name to be used in logging.
    *start_urls: list[str]*
        entry points to start shop web-scrapping.

    Methods:
    ---------
    *start_requests():*
        start yielding requests from start_urls.
    *process_reply(reply: Reply):*
        ............

    Subclassing:
    ~~~~~~~~~~~
    In subclasses implement *process_reply()* method, that is called as a default callback
    for all start_urls. Another option is to rewrite *start_requests()* method for more
    informative code.
    """

    name = ''
    start_urls = []

    def __init__(self):
        self._logger = logging.getLogger(self.name)

    def start_requests(self):
        """Starts iteration over start_urls"""
        self._logger.debug('start requests')
        for url in self.start_urls:
            yield Request(url, self.process_reply)

    def process_reply(self, reply: Reply, **kwargs):
        raise NotImplementedError("Has to be implemented in subclasses")
