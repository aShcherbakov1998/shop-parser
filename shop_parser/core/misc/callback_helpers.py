from collections import OrderedDict


class OnProductReadyCallbackHelper:
    def __init__(self, shop_name: str, on_product_ready_callback):
        self.shop_name = shop_name
        self.on_product_ready_callback = on_product_ready_callback

    def on_product_ready(self, city_name: str, product: OrderedDict):
        self.on_product_ready_callback(self.shop_name, city_name, product)


class OnParserProgressChangedCallbackHelper:
    def __init__(self, shop_name: str, on_progress_changed_callback):
        super().__init__()
        self.shop_name = shop_name
        self.on_progress_changed_callback = on_progress_changed_callback

    def on_progress_changed(self, progress: int):
        self.on_progress_changed_callback(self.shop_name, progress)
