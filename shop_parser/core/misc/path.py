import os
from time import time


ONE_WEEK_IN_SECONDS = 60 * 60 * 24 * 7 * 1
TWO_WEEKS_IN_SECONDS = ONE_WEEK_IN_SECONDS * 2


def get_all_files_in_directory(dir_path):
    out = []
    for root, dirs, files in os.walk(dir_path):
        for file in files:
            path = os.path.join(root, file)
            out.append(path)

    return out


def remove_old_files_in_dir(dir_path, time_diff_in_secs=TWO_WEEKS_IN_SECONDS):
    """Removes files that are older
    (the difference between the current time and the time of the creation)
    than "time_diff_in_secs" parameter.

    It only removes files, not directories.
    """

    file_paths = get_all_files_in_directory(dir_path)
    current_time = time()
    for file_path in file_paths:
        last_modified_time = os.path.getmtime(file_path)
        time_diff = current_time - last_modified_time

        if time_diff > time_diff_in_secs:
            os.remove(file_path)
