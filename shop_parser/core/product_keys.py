from collections import OrderedDict
from shop_parser.translations import _

VENDOR_CODE = _('Vendor code')
CATEGORY = _('Category')
NAME = _('Name')
PRICE = _('Price')
URL = _('Url')
PRICE_ON_SALE = _('Price on sale')


def convert_to_product(
        vendor_code,
        category,
        name,
        price,
        url,
        price_on_sale=None
) -> OrderedDict:
    out = OrderedDict()

    if price_on_sale is None:
        price_on_sale = ''

    out[VENDOR_CODE] = vendor_code
    out[CATEGORY] = category
    out[NAME] = name
    out[PRICE] = price
    out[PRICE_ON_SALE] = price_on_sale
    out[URL] = url

    return out
