from anytree import NodeMixin


class Category(NodeMixin):
    def __init__(self, name: str = None, url: str = None, parent=None):
        super().__init__()
        self.name = name
        self.url = url
        self.parent = parent


class City:
    def __init__(self, name: str = '', url: str = '', categories: list[Category] = []):
        self.name = name
        self.url = url
        self.categories = categories


class Shop:
    def __init__(self, name: str = '', url: str = '', cities: list[City] = []):
        self.name = name
        self.url = url
        self.cities = cities
