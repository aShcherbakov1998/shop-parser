"""
shop_parser.core
~~~~~~~~~~~~~~~~

This module contains base classes for organizing your shop-parsing processes.
The DataExtractor class is to be used as a base class for implementing the web data extraction.
The Processor class starts scrapping processes for given data-extractors, manages
delivering messages between NetworkManager and DataExtractor subclasses, [stores progress?]
etc.

If the scraping process require using multiple data-extractors synchronously, yoy can ..
TODO: write what you can^ (No I don't really know what I can)
"""

from PySide6.QtCore import QStandardPaths, QDir

CACHE_DIRECTORY = QDir(QStandardPaths.writableLocation(QStandardPaths.AppDataLocation) + '/ShopParser/')
CACHE_DIRECTORY.mkpath(CACHE_DIRECTORY.path())
CACHE_DIRECTORY = CACHE_DIRECTORY.path()

LOGS_DIRECTORY = QDir(CACHE_DIRECTORY + '/logs')
LOGS_DIRECTORY.mkpath(LOGS_DIRECTORY.path())
LOGS_DIRECTORY = LOGS_DIRECTORY.path()

from .data_extractor import DataExtractor
from .data_manipulator import *
from .processor import Processor
