import shop_parser.io.products


class ProductPipeline(object):
    def process_item(self, item, spider):
        file_path = 'products.csv'  # TODO: better name
        product = item.convert_to_object()
        shop_parser.io.products.write_csv(file_path, product)
        return item
