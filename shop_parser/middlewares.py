from fake_useragent import UserAgent

# User agent generator
USER_AGENT = UserAgent()


class CoreMiddleware(object):
    def process_request(self, request, spider):
        # TODO: Proxy stuff
        # request.meta['proxy'] = 'user:password@host:port'

        request.meta['User-Agent'] = USER_AGENT.random
