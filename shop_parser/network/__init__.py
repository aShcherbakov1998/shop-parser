"""
shop_parser.network
~~~~~~~~~~~~~~~~~~~


This module contains classes to manage internet connection.
"""


class Request:
    """Base class for network request."""

    def __init__(self, url: str, callback: callable, cookies: dict = None,
                 keep_session: bool = False, user_agent: str = None, **kwargs):
        """
        Creates request with given parameters.
        :param url: target url.
        :param callback:  a callable object to be called on reply:
                          Required signature: foo(reply: Reply, **kwargs)
        :param cookies: a dict of key-value cookie pairs to be sent with request.
        :param keep_session: if 'True' loads cookies from current session automatically,
                             if 'False" - ignores session cookies. If arg 'cookies' is not None,
                             session cookies are updated with arg 'cookies'
        :param user_agent: user_agent header to be used with request.
        :param kwargs: kwargs to be passed to callback whe request cimes.
                       This is made for user usage only.
        """
        self.url: str = url
        self.callback: callable = callback
        self.cookies: dict = cookies
        self.kwargs: dict = kwargs
        self.keep_session = keep_session
        self.user_agent = user_agent

    def __eq__(self, other):
        if isinstance(other, Request):
            return self.url == other.url and \
                   self.cookies == other.cookies

    def __hash__(self):
        cookies_str = ''.join(self.cookies.values()) if self.cookies else ''
        hash_str = self.url + cookies_str
        return hash_str.__hash__()

    def __repr__(self):
        return f'Request object (url: {self.url}, ' \
               f'callback: {self.callback}, ' \
               f'cookies: {self.cookies})'


class SeleniumRequest(Request):
    def __init__(self, url: str, callback: callable, cookies: dict = None, postprocess: callable = None, **kwargs):
        super().__init__(url, callback, cookies, **kwargs)
        self.postprocess = postprocess

    def __eq__(self, other):
        if isinstance(other, SeleniumRequest):
            return self.url == other.url and \
                   self.cookies == other.cookies


class Reply:
    """Stores all data needed for crawling in python-convenient form."""

    def __init__(self, status_code: int, url: str, text: str, request: Request):
        self.status_code = status_code
        self.url = url
        self.text = text
        self.request = request

    def successful(self):
        return self.status_code and 200 <= self.status_code < 300

    def status_string(self):
        return HTTPStatus(self.status_code).phrase


from . import proxy
from .manager import NetworkManager
from http import HTTPStatus
