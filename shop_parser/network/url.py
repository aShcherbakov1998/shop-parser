from urllib.parse import urlparse, urlencode, parse_qsl


def update_params(url: str, params: dict, clear_old_params: bool = False) -> str:
    parsed = urlparse(url)
    old_params = dict() if clear_old_params else dict(parse_qsl(parsed.query))
    merged_params = urlencode({**old_params, **params})
    parsed = parsed._replace(query=merged_params)

    return parsed.geturl()
