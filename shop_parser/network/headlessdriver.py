from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from webdriver_manager import chrome, opera, firefox, microsoft
from enum import Enum
import time


# The order is so that drivers with 'headless mode' go first
class DriverType(Enum):
    Chrome = 0
    Edge = 1
    Firefox = 2
    Opera = 3
    Explorer = 4


AVAILABLE_DRIVER = None


def create_webdriver(headless: bool = False, display_images: bool = True) -> WebDriver:
    global AVAILABLE_DRIVER

    if AVAILABLE_DRIVER is None:
        for driver_type in DriverType:
            try:
                driver = create_specific_webdriver(driver_type, headless, display_images)
                AVAILABLE_DRIVER = driver_type
                return driver
            except:
                pass

    if AVAILABLE_DRIVER is None:  # there is no available drivers
        raise RuntimeError("No available webdriver.")

    return create_specific_webdriver(AVAILABLE_DRIVER, headless, display_images)


def create_specific_webdriver(driver_type: DriverType, headless: bool = False,
                              display_images: bool = True) -> WebDriver:
    if driver_type == DriverType.Edge:
        options = webdriver.EdgeOptions()
        options.add_argument('--headless') if headless else None

        return webdriver.Edge(microsoft.EdgeChromiumDriverManager().install(),
                              options=options)
    if driver_type == DriverType.Chrome:
        options = webdriver.ChromeOptions()

        options.add_argument('--headless') if headless else None
        if not display_images:
            chrome_prefs = {"profile.default_content_settings": {"images": 2},
                            "profile.managed_default_content_settings": {"images": 2}}
            options.experimental_options["prefs"] = chrome_prefs

        return webdriver.Chrome(chrome.ChromeDriverManager().install(),
                                options=options)
    if driver_type == DriverType.Opera:
        # No headless mode, sorry
        return webdriver.Opera(opera.OperaDriverManager().install())
    if driver_type == DriverType.Firefox:
        options = webdriver.FirefoxOptions()
        options.add_argument('-headless') if headless else None
        profile = webdriver.FirefoxProfile()
        if not display_images:
            profile.set_preference("permissions.default.image", 2)

        return webdriver.Firefox(executable_path=firefox.GeckoDriverManager().install(),
                                 options=options, firefox_profile=profile)
    if driver_type == DriverType.Explorer:
        return webdriver.Ie(microsoft.IEDriverManager().install())


def remove_all_elements_except(element_query_selector: str, webdriver: WebDriver):
    """Removes all elements except those, that is found with 'element_query_selector'.
       This is especially useful in order to remove all modal add banners intercepting
       user input.
    :param element_query_selector: a query selector to the target element
    :param webdriver: Selenium Webdriver object
    """
    webdriver.execute_script(
        f'var current = document.querySelector("{element_query_selector}");'
        ' while(current.parentNode){'
        '   var parent = current.parentNode;'
        '   var children = parent.children;'
        '   for(var i = children.length-1; i >=0; i--){'
        '     if(!children[i].isSameNode(current) && '
        '        !children[i].isSameNode(document.head)){ '  # tag <head> should never be deleted 
        '           parent.removeChild(children[i]);'
        '     }'
        '  }'
        '   current = parent;'
        '}'
    )


def wait_until_page_ready(webdriver: WebDriver):
    """If JavaScript is currently running, waits until it finishes.

    :param webdriver: Selenium Webdriver object
    """
    WebDriverWait(webdriver, 10).until(lambda driver:
                                       driver.execute_script('return document.readyState') == 'complete')


def scroll_page_down_till_end(webdriver: WebDriver):
    """Scrolls page with dynamic loading to the end while it is possible.

    :param webdriver: Selenium Webdriver object
    """
    last_height = webdriver.execute_script("return document.body.scrollHeight")

    while True:
        webdriver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(0.5)
        wait_until_page_ready(webdriver)

        new_height = webdriver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            # try again to be sure it's done
            time.sleep(3.0)
            new_height = webdriver.execute_script("return document.body.scrollHeight")
            if new_height == last_height:
                break
        last_height = new_height
