from enum import Enum


class ProxyType(Enum):
    NoProxy = 0
    Socks5Proxy = 1
    HttpProxy = 2


proxy_type = ProxyType.NoProxy
host = ''
port = 0
username = ''
password = ''
