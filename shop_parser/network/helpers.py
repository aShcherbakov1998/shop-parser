from random import uniform
from PySide6.QtCore import QUrl
from shop_parser.network import Request
from PySide6.QtNetwork import QNetworkRequest
from PySide6.QtCore import QByteArray
from collections import deque


class DelayRandomizer:
    """
    Generates delay within [ref_value - deviation : ref_value + deviation] time in seconds.

    Attributes:
    ----------
    mid : float
        the reference delay value.
    deviation : float
        the maximum deviation from the reference value in seconds

    Methods:
    ----------
    get_millis():
        returns random delay in milliseconds
    get_seconds():
        returns random delay in seconds
    """

    def __init__(self, mid: float = 3.0, deviation: float = 1.0):
        """
        :param mid: the mid value in seconds
        :param deviation: the deviation in seconds
        """
        self.ref_value = mid
        self.deviation = deviation

    def get_secs(self, mid: float = None, deviation: float = None) -> float:
        """Returns the random value within [ref_value - deviation : ref_value + deviation]
        in seconds"""
        if mid is None:
            mid = self.ref_value
        if deviation is None:
            deviation = self.deviation

        return uniform(mid - deviation, mid + deviation)

    def get_millis(self, mid: float = None, deviation: float = None) -> int:
        """Returns the random value within [mid - deviation : mid + deviation]
        in milliseconds."""
        return int(self.get_secs(mid, deviation) * 1000)


class RequestsStorage:
    def __init__(self):
        self._dict: dict[str, list[Request]] = {}

    def add(self, request: Request):
        if request.url in self._dict:
            self._dict[request.url.lower()].append(request)
        else:
            self._dict[request.url.lower()] = [request]

    def get(self, q_request: QNetworkRequest) -> Request:
        requests_for_url = self._dict[q_request.url().url().lower()]
        q_cookies = bytes(q_request.rawHeader(QByteArray(b'Cookie'))).decode('utf-8')
        for i in range(len(requests_for_url)):
            request = requests_for_url[i]
            if request.cookies:
                cookies = '; '.join([f'{key}={value}' for key, value in request.cookies.items()])
                if q_cookies == cookies:
                    return requests_for_url.pop(i)
        # if cookies in q_request, but no cookies in requests
        return requests_for_url.pop(0)

    def remove(self, request: Request):
        requests_for_url = self._dict[request.url.lower()]
        for i in range(len(requests_for_url)):
            if requests_for_url[i] == request:
                requests_for_url.pop(i)
                return

    def clear(self):
        self._dict.clear()


class ErrorCounter:
    def __init__(self):
        self.requests: dict[Request, int] = dict()

    def increase(self, request: Request):
        if request in self.requests:
            self.requests[request] += 1
        else:
            self.requests[request] = 1

    def count(self, request: Request) -> int:
        return self.requests[request] if request in self.requests else 0
