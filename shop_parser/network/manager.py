import logging
import re
import time

from shop_parser.network import Request, SeleniumRequest, Reply as SimpleReply
from shop_parser.network.helpers import *
from shop_parser.network import proxy
from shop_parser.network import headlessdriver

from PySide6.QtCore import QObject, QTimer, QByteArray, QEventLoop
from PySide6.QtNetwork import (QNetworkAccessManager,
                               QNetworkRequest,
                               QNetworkReply,
                               QNetworkProxy,
                               QNetworkCookie)

from fake_useragent import UserAgent

ua_gen = UserAgent()


class NetworkManager(QObject):
    """
    A class to manage the internet connection.

    NetworkManager gives a high-level API to send 'GET' and 'POST'
    requests with no need to worry about headers, delays, proxy, etc...


    Methods:
    ----------
    abort():
        stops all network communication and clears all cached data.
    get(url: str, callback):
        requests the page from the given url. When response comes, the
        provided callback is called.
    post(url: str, data, callback):
        posts data to the given url. When response comes, the
        provided callback is called.
    start():
        starts internet communication if it was stopped by user before.
        Connection can also be resumed by using *get()* and *post()*.
    stop():
        stops (pauses) all internet communication.
    """

    def __init__(self, name: str = 'NetworkManager', parent: QObject = None):
        """

        :param name: the name used in log messages.
        :param parent: a standard QObject parent used in memory-management by Qt library.
        """
        super().__init__(parent)

        self._manager = QNetworkAccessManager(self)
        self._delay_randomizer = DelayRandomizer()
        self._requests_storage = RequestsStorage()
        self._queued_requests = deque()
        self._error_counter = ErrorCounter()
        self._timer = QTimer(self)
        self._keep_session = False
        self._user_agent = 'random'
        self._requests_in_process = 0
        self._logger = logging.getLogger(name)

        self._manager.finished.connect(self.__process_reply)
        self._timer.timeout.connect(self.__send_enqueued_request)

        self.update_proxy()
        # TODO:  don't forget about proxy authentication required signal

    def __del__(self):
        self._timer.timeout.disconnect()
        self._manager.finished.disconnect()

        del self._manager
        del self._timer

    def get(self, request: Request):
        """Enqueues the request. Saves the callback.
        The request will be sent after all requests enqueued before."""
        self._queued_requests.append(request)
        self._requests_storage.add(request)
        # start timer if not active
        self.start()

    def requests_in_queue(self) -> int:
        return len(self._queued_requests)

    def requests_in_process(self) -> int:
        return self._requests_in_process

    def set_keep_session(self, state: bool):
        """If 'True', a fixed user-agent header is used.
        Otherwise user-agent is randomly generated on each request."""
        self._keep_session = state

        # take one user-agent header that will be used in future requests
        # if we store cookies (i.e. session_id etc.)
        self._user_agent = ua_gen.random if state else 'random'
        self._logger.info(f'keep session set to {state}.')

    def is_active(self):
        """Returns false if stopped by user calling stop() or there
        is no queued_requests to work with. Else returns true"""
        return self._timer.isActive()

    def start(self):
        """Starts sending queued requests if stopped by user.
        Else does nothing."""
        if not self._timer.isActive():
            self._timer.start(1)
            self._logger.debug('Start working...')

    def stop(self):
        """Stops sending queued requests."""
        if self.is_active():
            self._timer.stop()
            self._logger.debug(f'Stop working with {self.requests_in_queue()} '
                               f'requests waiting to be sent.')

    def abort(self):
        """Stops sending requests. Clears all cached data."""
        self.stop()
        self._queued_requests.clear()
        self._requests_storage.clear()
        self._logger.info(f'Aborting with {self.requests_in_queue()} '
                          f'requests waiting to be sent.')

    def __prepare_qtrequest(self, request: Request) -> QNetworkRequest:
        """Prepares the QNetworkRequest from the given url.
        Sets request's raw-headers and cookies (if specified)."""

        self.__manage_cookies(request)
        # TODO
        user_agent_header = ua_gen.random
        if self._keep_session:
            user_agent_header = self._user_agent
        if request.user_agent:
            user_agent_header = request.user_agent

        request = QNetworkRequest(QUrl(request.url))
        request.setRawHeader(QByteArray(b"user-agent"), QByteArray(user_agent_header))
        request.setRawHeader(QByteArray(b"Content-Type"), QByteArray(b'text/html; charset=UTF-8'))

        # TODO: set other raw headers.

        return request

    def __send_request(self, request: QNetworkRequest, data=None):
        self._logger.debug(f'send {request.url()}')
        self._manager.get(request)
        self._requests_in_process += 1
        # restart the timer with new delay interval
        self.__restart_timer()

    def __send_enqueued_request(self):
        try:
            request = self._queued_requests.popleft()
        except IndexError:
            self.stop()
            return

        if isinstance(request, SeleniumRequest):
            self.__process_selenium_request(request)
        else:
            q_request = self.__prepare_qtrequest(request)
            self.__send_request(q_request)

    def __process_selenium_request(self, request: SeleniumRequest):
        driver = headlessdriver.create_webdriver(headless=True, display_images=False)
        driver.get(request.url)
        if request.cookies:
            driver.delete_all_cookies()
            for name, value in request.cookies.items():
                driver.add_cookie({'name': name, 'value': value})
            driver.get(request.url)
        self._logger.info(f'SeleniumRequest: {request.url} {request.cookies if request.cookies else "" }')
        if request.postprocess:
            request.postprocess(driver)
        self._requests_storage.remove(request)
        request.callback(SimpleReply(status_code=200, url=driver.current_url, text=driver.page_source, request=request))
        driver.quit()

    def __restart_timer(self):
        self._timer.start(self._delay_randomizer.get_millis())

    def __process_reply(self, reply: QNetworkReply):
        """Processes the reply. If reply is successful, calls the callback.
         If forbidden, then sleeps and resends. Else NotImplemented."""

        initial_request = self._requests_storage.get(reply.request())
        self._requests_in_process -= 1
        status_code = reply.attribute(QNetworkRequest.Attribute.HttpStatusCodeAttribute)
        operation = request_operation(reply)

        # delete the reply from memory
        reply.deleteLater()
        success = status_code and 200 <= status_code < 300

        cookies = initial_request.cookies
        if success:
            self._logger.info(f'{status_code} {operation} {reply.request().url().url()} {cookies if cookies else ""}')
        elif not status_code:
            self._logger.warning(f'ErrorString: "{reply.errorString()}" at {reply.url().url()} {cookies if cookies else ""}. '
                                 f'Repeats done {self._error_counter.count(initial_request)}')
        else:
            self._logger.warning(f'{status_code} {operation} {reply.url().url()} {cookies if cookies else ""}. '
                                 f'Repeats done {self._error_counter.count(initial_request)}')

        if not success:
            if self._error_counter.count(initial_request) < 3:  # TODO: parameter
                self._error_counter.increase(initial_request)
                self.__sleep(self._delay_randomizer.get_secs(5.0, 1.0))
                q_request = self.__prepare_qtrequest(initial_request)
                self._requests_storage.add(initial_request)
                self.__send_request(q_request)
            else:
                out_reply = form_http_reply(reply, initial_request)
                initial_request.callback(out_reply)
            # that won't work with POST, because i don't store data anymore.
            return

        out_reply = form_http_reply(reply, initial_request)
        initial_request.callback(out_reply)

    def __sleep(self, secs: float):
        self.stop()
        event_loop = QEventLoop()
        # stop local event_loop on timeout
        QTimer.singleShot(int(secs * 1000), event_loop.quit)
        event_loop.exec()
        self.start()

    def __manage_cookies(self, request: Request):
        """QNetworkAccessManager stores cookies by default. cookies are set in
        QNetworkAccessManager.prepareRequest(). It is better to rewrite those method,
        but the simpler solution is to just remove cookies if they exist,
        so that there would be nothing to set."""

        cookie_jar = self._manager.cookieJar()
        if not request.keep_session:
            cookies_for_url = cookie_jar.cookiesForUrl(request.url)
            if cookies_for_url:
                for cookie in cookies_for_url:
                    cookie_jar.deleteCookie(cookie)

        if request.cookies:
            cookies = [QNetworkCookie(QByteArray(key), QByteArray(value)) for
                       key, value in request.cookies.items()]
            cookie_jar.setCookiesFromUrl(cookies, QUrl(request.url))

    def update_proxy(self):
        if proxy.proxy_type == proxy.ProxyType.NoProxy:
            proxy_type = QNetworkProxy.NoProxy
        elif proxy.proxy_type == proxy.ProxyType.Socks5Proxy:
            proxy_type = QNetworkProxy.Socks5Proxy
        elif proxy.proxy_type == proxy.ProxyType.HttpProxy:
            proxy_type = QNetworkProxy.HttpProxy
        _proxy = QNetworkProxy(proxy_type, proxy.host,
                               int(proxy.port),
                               proxy.username,
                               str(proxy.password))
        self._manager.setProxy(_proxy)


def form_http_reply(reply: QNetworkReply, request: Request):
    # TODO: use regexp
    encoding = bytes(reply.rawHeader(QByteArray(b'Content-Type'))).decode()
    encoding = encoding.split(';')
    for value in encoding:
        if 'charset' in value:
            encoding = value.split('=')[1].strip()
            break
    if not isinstance(encoding, str):
        encoding = None
    return SimpleReply(status_code=reply.attribute(QNetworkRequest.Attribute.HttpStatusCodeAttribute),
                       url=reply.url().url(),
                       text=str(reply.readAll(), encoding if encoding else 'utf-8'),
                       request=request)


def request_operation(reply: QNetworkReply) -> str:
    operation = reply.operation()
    if operation == QNetworkAccessManager.GetOperation:
        return 'GET'
    elif operation == QNetworkAccessManager.PostOperation:
        return 'POST'
    elif operation == QNetworkAccessManager.HeadOperation:
        return 'HEAD'
    elif operation == QNetworkAccessManager.PutOperation:
        return 'PUT'
    elif operation == QNetworkAccessManager.DeleteOperation:
        return 'DELETE'
    elif operation == QNetworkAccessManager.CustomOperation:
        return 'CUSTOM'
    else:
        return 'UNKNOWN'
