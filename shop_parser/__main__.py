import sys, os.path, datetime
import logging.config

from PySide6.QtWidgets import QApplication
from PySide6.QtGui import QIcon

from shop_parser import rc_res_rc
from shop_parser.ui.mainwindow import MainWindow
import shop_parser.core
from shop_parser import telegram_bot

SESSION_LOG_FILEPATH: str = ''


def main():
    init_logging()

    app = QApplication(sys.argv)
    app.setApplicationName('ShopParser')
    app.setOrganizationName('MasterProf')
    window = MainWindow()
    app.setWindowIcon(QIcon(R':/icons/res/icons8_spider-man_new.ico'))

    window.show()
    app.exec()

    send_logs()
    sys.exit()


def send_logs():
    try:
        telegram_bot.send_file(SESSION_LOG_FILEPATH)
    except Exception as e:
        logging.warning(f'Log file not sent. Reason: {e}')


def init_logging():
    global SESSION_LOG_FILEPATH
    current_module_dir = os.path.dirname(os.path.abspath(__file__))
    logging_config_file_path = os.path.join(current_module_dir, 'logging.conf')
    logging.config.fileConfig(logging_config_file_path)

    # init FileHandler
    logs_dir = shop_parser.core.LOGS_DIRECTORY
    file_name = '{:%Y-%m-%d}.log'.format(datetime.datetime.now())
    SESSION_LOG_FILEPATH = logs_dir + '/' + file_name
    fh = logging.FileHandler(SESSION_LOG_FILEPATH, mode='a', encoding='utf-8')
    fh.setLevel('DEBUG')
    formatter = logging.Formatter('[%(asctime)s] %(name)s %(levelname)s %(message)s', datefmt='%d-%m-%Y %H:%M:%S')
    fh.setFormatter(formatter)
    logging.getLogger().addHandler(fh)

    print(f'Log file path: {SESSION_LOG_FILEPATH}')


if __name__ == '__main__':
    main()
