import gettext
import os.path


def get_locales_dir():
    path = os.getcwd().split('\\')
    path = path[:path.index('shop-parser')]
    return '\\'.join(path) + '\\shop-parser\\locales'


print('Locales directory:', get_locales_dir())
ru = gettext.translation('base', localedir=get_locales_dir(), languages=['ru'])
ru.install()
_ = ru.gettext
