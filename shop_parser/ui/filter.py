from typing import Optional

from PySide6.QtGui import QStandardItemModel, QStandardItem
from PySide6.QtCore import Qt

from shop_parser.core.data import Shop, Category, City


def filter_shop(
        shop: Shop,
        cities_model: QStandardItemModel,
        categories_model: QStandardItemModel
) -> Shop:
    out = Shop()
    out.name = shop.name
    out.url = shop.url
    out.cities = []

    items = get_top_level_items(categories_model)
    for city in shop.cities:
        if not is_item_selected(cities_model, city.name):
            continue

        categories = filter_categories(city.categories, items)
        if categories:
            filtered_city = City()
            filtered_city.name = city.name
            filtered_city.url = city.url
            filtered_city.categories = categories
            out.cities.append(filtered_city)

    return out


def filter_categories(
        categories: list[Category],
        items: list[QStandardItem]
) -> list[Category]:
    out = []
    for category in categories:
        item = find_item_by_name(items, category.name)
        if item is None:
            continue

        if not is_item_enabled(item):
            continue

        filtered_category = Category()
        filtered_category.name = category.name
        filtered_category.url = category.url

        should_add_category = True
        if category.children:
            item_children = get_children(item)
            filtered_category.children = filter_categories(category.children, item_children)
            if not filtered_category.children:
                should_add_category = False

        if not should_add_category:
            continue

        out.append(filtered_category)

    return out


def find_item_by_name(items: list[QStandardItem], name: str) -> Optional[QStandardItem]:
    for item in items:
        if item.text() == name:
            return item

    return None


def find_item_in_model_by_name(model: QStandardItemModel, item_name: str) -> Optional[QStandardItem]:
    items = get_top_level_items(model)
    return find_item_by_name(items, item_name)


def get_children(item: QStandardItem) -> list[QStandardItem]:
    out = []
    for i in range(item.rowCount()):
        out.append(item.child(i))

    return out


def get_top_level_items(model: QStandardItemModel) -> list[QStandardItem]:
    out = []
    for i in range(model.rowCount()):
        item = model.item(i, 0)
        out.append(item)

    return out


def set_item_enabled(item: QStandardItem, enable: bool = True):
    state = Qt.Checked
    if not enable:
        state = Qt.Unchecked

    item.setCheckState(state)


def is_item_enabled(item: QStandardItem) -> bool:
    return item.checkState() == Qt.Checked \
           or item.checkState() == Qt.PartiallyChecked


def is_item_selected(model: QStandardItemModel, item_name: str) -> bool:
    out = False
    for i in range(model.rowCount()):
        item = model.item(i, 0)
        if item.text() == item_name:
            if item.checkState() == Qt.Checked:
                out = True
                break

    return out


def set_item_selected(model: QStandardItemModel, item_name: str, selected: bool = True):
    for i in range(model.rowCount()):
        item = model.item(i, 0)
        if item.text() == item_name:
            set_item_enabled(item, selected)
            break
