from PySide6.QtCore import Qt
from PySide6.QtWidgets import QWidget


def show_widget_on_top(widget: QWidget):
    widget.show()
    widget.setWindowState(
        widget.windowState()
        & ~Qt.WindowMinimized
        | Qt.WindowActive)

    # widget.raise_()
    widget.activateWindow()
