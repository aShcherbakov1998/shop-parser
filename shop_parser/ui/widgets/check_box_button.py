from PySide6.QtGui import QPaintEvent
from PySide6.QtWidgets import (QPushButton, QWidget, QStylePainter, QStyle, QStyleOptionButton)

from shop_parser.translations import _


class ViewSelectionButton(QPushButton):
    def __init__(self, checked: bool, parent: QWidget = None):
        super().__init__(parent)
        self.state = checked
        self.setToolTip(_('Check all') if checked else _('Uncheck all'))

    def paintEvent(self, event: QPaintEvent):
        super().paintEvent(event)
        option = QStyleOptionButton()
        self.initStyleOption(option)
        option.state |= QStyle.State_Enabled | QStyle.State_On if self.state else QStyle.State_Off

        move_left_offset = 11
        option.rect = self.rect()
        option.rect.moveLeft(option.rect.left() + move_left_offset)
        painter = QStylePainter(self)
        painter.drawControl(QStyle.CE_CheckBox, option)
