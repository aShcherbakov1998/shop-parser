import os

from PySide6.QtCore import QByteArray, QDataStream, QIODevice
from PySide6.QtWidgets import (
    QWidget,
    QGridLayout,
    QVBoxLayout,
    QGroupBox,
    QLabel,
    QLineEdit,
    QPushButton,
    QFileDialog,
    QStyle
)

from shop_parser.translations import _
from shop_parser.network import proxy
from shop_parser.ui.widgets.proxy_widget import ProxyWidget


class GeneralSettingsWidget(QWidget):
    def __init__(self, products_root_dir: str = ''):
        super(GeneralSettingsWidget, self).__init__()

        self.setWindowTitle(_('General settings'))
        self.__create_widgets()
        self.__connect_proxy_signals()

        if not products_root_dir:
            products_root_dir = self.__get_default_products_root_dir()

        self.set_products_root_dir(products_root_dir)

    def products_root_dir(self) -> str:
        return self.products_root_dir_line_edit.text()

    def set_products_root_dir(self, root_dir: str):
        self.products_root_dir_line_edit.setText(root_dir)

    def save_state(self):
        byte_array = QByteArray()
        stream = QDataStream(byte_array, QIODevice.WriteOnly)
        stream << self.proxy_widget.save_state()
        return byte_array

    def restore_state(self, byte_array: QByteArray) -> bool:
        stream = QDataStream(byte_array)
        proxy_state = QByteArray()
        stream >> proxy_state
        return self.proxy_widget.restore_state(proxy_state)

    def __create_widgets(self):
        self.main_layout = QVBoxLayout()
        self.__create_proxy_group_box()
        self.__create_path_group_box()

        self.main_layout.addWidget(self.path_group_box)
        self.main_layout.addWidget(self.proxy_widget)
        self.main_layout.addStretch()
        self.setLayout(self.main_layout)

    def __create_path_group_box(self):
        self.path_group_box = QGroupBox(_('Path settings'))
        self.path_group_box_layout = QGridLayout()

        self.products_root_dir_label = QLabel(_('Products directory:'))
        self.products_root_dir_line_edit = QLineEdit()
        self.products_root_dir_line_edit.setReadOnly(True)
        self.products_root_dir_button = QPushButton()
        icon = self.style().standardIcon(getattr(QStyle, 'SP_DirIcon'))
        self.products_root_dir_button.setIcon(icon)
        self.products_root_dir_button.setFixedWidth(50)

        self.products_root_dir_button.clicked.connect(self.__on_products_root_dir_button_clicked)
        self.path_group_box_layout.addWidget(self.products_root_dir_label, 0, 0)
        self.path_group_box_layout.addWidget(self.products_root_dir_line_edit, 0, 1)
        self.path_group_box_layout.addWidget(self.products_root_dir_button, 0, 2)

        # self.path_group_box_layout.setColumnStretch(1, 1)
        self.path_group_box.setLayout(self.path_group_box_layout)

    def __create_proxy_group_box(self):
        self.proxy_widget = ProxyWidget(self)

    def __on_products_root_dir_button_clicked(self):
        dir_path = QFileDialog.getExistingDirectory(
            caption=_('Set the directory where to save parsed products'),
            dir=self.products_root_dir()
        )

        if not dir_path:
            return

        self.set_products_root_dir(dir_path)

    def __get_default_products_root_dir(self) -> str:
        return os.getcwd()

    def __proxy_changed(self):
        proxy.proxy_type = proxy.ProxyType(self.proxy_type_combobox.currentIndex())
        proxy.host = self.proxy_host_line_edit.text()
        proxy.port = self.proxy_port_spin_box.value()
        proxy.username = self.proxy_name_line_edit.text()
        proxy.password = self.proxy_password_line_edit.text()

    def __connect_proxy_signals(self):
        # self.proxy_type_combobox.currentIndexChanged.connect(self.__proxy_changed)
        # self.proxy_host_line_edit.editingFinished.connect(self.__proxy_changed)
        # self.proxy_port_spin_box.valueChanged.connect(self.__proxy_changed)
        # self.proxy_name_line_edit.editingFinished.connect(self.__proxy_changed)
        # self.proxy_password_line_edit.editingFinished.connect(self.__proxy_changed)
        pass
