from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
    QGroupBox,
    QAbstractItemView,
    QWidget,
    QPushButton,
    QBoxLayout,
    QSpacerItem,
    QSizePolicy
)

from shop_parser.ui.widgets.check_box_button import ViewSelectionButton


class ViewContainer(QGroupBox):
    def __init__(self, view: QAbstractItemView, title: str = None, parent: QWidget = None):
        super().__init__(parent)
        self.setTitle(title)
        self.view = view
        self.select_all_button: QPushButton = None
        self.clear_selection_button: QPushButton = None
        self.__create_widgets()
        self.__connect_signals()

    def select_all(self):
        model = self.view.model()
        if model:
            for i in range(model.rowCount()):
                model.setData(model.index(i, 0), Qt.Checked, Qt.CheckStateRole)

    def clear_selection(self):
        model = self.view.model()
        if model:
            for i in range(model.rowCount()):
                model.setData(model.index(i, 0), Qt.Unchecked, Qt.CheckStateRole)

    def __create_widgets(self):
        self.select_all_button = ViewSelectionButton(checked=True, parent=self)
        self.clear_selection_button = ViewSelectionButton(checked=False, parent=self)
        button_layout = QBoxLayout(QBoxLayout.LeftToRight)
        button_layout.addSpacerItem(QSpacerItem(2, 0, QSizePolicy.Expanding, QSizePolicy.Minimum))
        button_layout.addWidget(self.select_all_button)
        button_layout.addWidget(self.clear_selection_button)
        button_layout.setContentsMargins(0, 0, 0, 0)
        button_layout.setSpacing(0)

        global_layout = QBoxLayout(QBoxLayout.TopToBottom)
        global_layout.addLayout(button_layout)
        global_layout.addWidget(self.view)
        global_layout.setSpacing(3)
        self.setLayout(global_layout)
        self.setContentsMargins(0, 0, 0, 0)

    def __connect_signals(self):
        self.select_all_button.clicked.connect(self.select_all)
        self.clear_selection_button.clicked.connect(self.clear_selection)
