from PySide6.QtGui import QMovie
from PySide6.QtCore import Signal, QEventLoop, QTimer, QByteArray, QDataStream, QIODevice
from PySide6.QtNetwork import QNetworkAccessManager, QNetworkProxy, QNetworkRequest, QNetworkReply
from PySide6.QtWidgets import (
    QGroupBox,
    QLabel,
    QComboBox,
    QLineEdit,
    QSpinBox,
    QGridLayout,
    QBoxLayout,
    QWidget,
    QPushButton,
    QSpacerItem,
    QSizePolicy,
    QMessageBox
)

from shop_parser.translations import _
from shop_parser import rc_res_rc
from shop_parser.network import proxy


class ProxyWidget(QWidget):
    changed = Signal()

    def __init__(self, parent: QWidget = None):
        super().__init__(parent)
        self.__create_widgets()
        self.apply_button.clicked.connect(self.validate)
        self.proxy_type_combobox.currentIndexChanged.connect(self.__enable_widgets)
        self.__enable_widgets()
        self._movie = QMovie(R':/icons/res/loading.gif')

    def __create_widgets(self):
        self.proxy_type_combobox = QComboBox()
        self.proxy_host_line_edit = QLineEdit()
        self.proxy_port_spin_box = QSpinBox()
        self.proxy_name_line_edit = QLineEdit()
        self.proxy_password_line_edit = QLineEdit()
        self.apply_button = QPushButton(_('Apply'))
        self.process_indicator = QLabel()

        group_box = QGroupBox(_('Proxy settings'), self)
        group_box_layout = QGridLayout()
        group_box.setLayout(group_box_layout)
        group_box_layout.setColumnStretch(1, 1)
        self.setContentsMargins(0, 0, 0, 0)

        self.proxy_type_combobox.addItems([type.name for type in proxy.ProxyType])
        group_box_layout.addWidget(QLabel(_('Type:')), 0, 0)
        group_box_layout.addWidget(self.proxy_type_combobox, 0, 2)

        group_box_layout.addWidget(QLabel(_('Host:')), 1, 0)
        group_box_layout.addWidget(self.proxy_host_line_edit, 1, 2)

        self.proxy_port_spin_box.setRange(0, 65535)
        group_box_layout.addWidget(QLabel(_('Port:')), 2, 0)
        group_box_layout.addWidget(self.proxy_port_spin_box, 2, 2)

        group_box_layout.addWidget(QLabel(_('Name:')), 3, 0)
        group_box_layout.addWidget(self.proxy_name_line_edit, 3, 2)

        group_box_layout.addWidget(QLabel(_('Password:')), 4, 0)
        group_box_layout.addWidget(self.proxy_password_line_edit, 4, 2)

        main_layout = QBoxLayout(QBoxLayout.TopToBottom)
        main_layout.setContentsMargins(0, 0, 0, 0)
        main_layout.addWidget(group_box)
        button_layout = QBoxLayout(QBoxLayout.LeftToRight)
        button_layout.addWidget(self.process_indicator)
        button_layout.addSpacerItem(QSpacerItem(3, 1, QSizePolicy.Expanding, QSizePolicy.Expanding))
        button_layout.addWidget(self.apply_button)
        main_layout.addLayout(button_layout)
        self.setLayout(main_layout)
        # self.process_indicator.hide()

    def validate(self):
        self.show_process_indicator()

        network = QNetworkAccessManager()
        proxy = _get_network_proxy(
            self.proxy_type_combobox.currentIndex(),
            self.proxy_host_line_edit.text(),
            self.proxy_port_spin_box.value(),
            self.proxy_name_line_edit.text(),
            self.proxy_password_line_edit.text())
        network.setProxy(proxy)

        reply = network.get(QNetworkRequest('https://myip.ru/'))
        loop = QEventLoop()
        reply.finished.connect(loop.quit)
        QTimer.singleShot(5000, loop.quit)
        loop.exec()
        self.hide_process_indicator()
        if reply.isFinished():
            if reply.error() == QNetworkReply.NoError:
                self.apply()
                return

        QMessageBox.warning(
            self,
            _('Proxy'),
            _('Cannot establish the connection. '
              'Check your credentials and try again...'))

    def apply(self):
        proxy.proxy_type = proxy.ProxyType(self.proxy_type_combobox.currentIndex())
        proxy.host = self.proxy_host_line_edit.text()
        proxy.port = self.proxy_port_spin_box.value()
        proxy.username = self.proxy_name_line_edit.text()
        proxy.password = self.proxy_password_line_edit.text()

    def discard(self):
        self.proxy_type_combobox.setCurrentIndex(proxy.proxy_type.value)
        self.proxy_host_line_edit.setText(proxy.host)
        self.proxy_port_spin_box.setValue(proxy.port)
        self.proxy_name_line_edit.setText(proxy.username)
        self.proxy_password_line_edit.setText(proxy.password)

    def __enable_widgets(self):
        enabled = self.proxy_type_combobox.currentIndex() != 0
        self.proxy_host_line_edit.setEnabled(enabled)
        self.proxy_port_spin_box.setEnabled(enabled)
        self.proxy_name_line_edit.setEnabled(enabled)
        self.proxy_password_line_edit.setEnabled(enabled)
        self.proxy_port_spin_box.setEnabled(enabled)

    def save_state(self) -> QByteArray:
        byte_array = QByteArray()
        stream = QDataStream(byte_array, QIODevice.WriteOnly)
        stream.writeInt32(self.proxy_type_combobox.currentIndex())
        stream.writeString(self.proxy_host_line_edit.text())
        stream.writeInt32(self.proxy_port_spin_box.value())
        stream.writeString(self.proxy_name_line_edit.text())
        stream.writeString(self.proxy_password_line_edit.text())
        return byte_array

    def restore_state(self, state: QByteArray) -> bool:
        stream = QDataStream(state)
        type = stream.readInt32()
        host = stream.readString()
        port = stream.readInt32()
        name = stream.readString()
        password = stream.readString()

        self.proxy_type_combobox.setCurrentIndex(type)
        self.proxy_host_line_edit.setText(host)
        self.proxy_port_spin_box.setValue(port)
        self.proxy_name_line_edit.setText(name)
        self.proxy_password_line_edit.setText(password)

        return type and host and port and name and password

    def show_process_indicator(self):
        self.process_indicator.setMovie(self._movie)
        self.process_indicator.show()
        self._movie.start()

    def hide_process_indicator(self):
        self._movie.stop()
        self.process_indicator.hide()


def _get_network_proxy(type: int, host: str, port: int, user: str, password: str) -> QNetworkProxy:
    if type == proxy.ProxyType.NoProxy.value:
        proxy_type = QNetworkProxy.NoProxy
    elif type == proxy.ProxyType.Socks5Proxy.value:
        proxy_type = QNetworkProxy.Socks5Proxy
    elif type == proxy.ProxyType.HttpProxy.value:
        proxy_type = QNetworkProxy.HttpProxy

    return QNetworkProxy(
        proxy_type,
        host,
        int(port),
        user,
        password)
