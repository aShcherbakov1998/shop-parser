import enum

from PySide6.QtWidgets import QPushButton, QStyle, QWidget, QBoxLayout
from PySide6.QtCore import Signal

from shop_parser.translations import _


class ActionPanel(QWidget):
    class Action(enum.Enum):
        START = 0
        STOP = 1
        RESUME = 2

    start_requested = Signal()
    stop_requested = Signal()
    clear_requested = Signal()

    def __init__(self, parent: QWidget = None):
        super().__init__(parent)
        icon = self.style().standardIcon(getattr(QStyle, 'SP_DialogResetButton'))
        self._clear_cache_button = QPushButton(icon, _('Clear cache'), self)
        self._start_button = QPushButton(self)
        self.set_action(self.Action.START)
        self._action = ActionPanel.Action.START
        self._start_button.clicked.connect(self.__start_button_clicked)
        self._clear_cache_button.clicked.connect(lambda: self.clear_requested.emit())
        self.__init_layout()

    def set_action(self, action: Action):
        if action == ActionPanel.Action.START:
            icon_style = 'SP_MediaPlay'
            self._start_button.setText(_('Start'))
        if action == ActionPanel.Action.RESUME:
            icon_style = 'SP_MediaSeekForward'
            self._start_button.setText(_('Continue'))
        elif action == ActionPanel.Action.STOP:
            icon_style = 'SP_MediaPause'
            self._start_button.setText(_('Stop'))
        icon = self.style().standardIcon(getattr(QStyle, icon_style))
        self._start_button.setIcon(icon)
        self._action = action

    def set_clear_cache_enabled(self, enabled: bool):
        self._clear_cache_button.setEnabled(enabled)

    def __init_layout(self):
        layout = QBoxLayout(QBoxLayout.LeftToRight)
        layout.addWidget(self._clear_cache_button)
        layout.addWidget(self._start_button)
        self.setLayout(layout)
        self.setContentsMargins(0, 0, 0, 0)

    def __start_button_clicked(self):
        if self._action == self.Action.START or \
                self._action == self.Action.RESUME:
            self.start_requested.emit()
        elif self._action == self.Action.STOP:
            self.stop_requested.emit()
