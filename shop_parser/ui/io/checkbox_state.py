from pathlib import Path
import json

from PySide6.QtGui import QStandardItemModel, QStandardItem

import shop_parser.ui.filter


NAME_JSON_KEY = 'name'
IS_ENABLED_JSON_KEY = 'is_enabled'
CITIES_JSON_KEY = 'cities'
CATEGORIES_JSON_KEY = 'categories'
CHILDREN_JSON_KEY = 'children'


def write(
        file_path: str,
        shop_item: QStandardItem,
        cities_model: QStandardItemModel,
        categories_model: QStandardItemModel):
    Path(file_path).parent.mkdir(parents=True, exist_ok=True)

    shop_state = get_shop_state(shop_item, cities_model, categories_model)
    with open(file_path, 'w', encoding='utf-8') as file:
        json.dump(shop_state, file, ensure_ascii=False, indent=2)


def read(
        file_path: str,
        shop_item: QStandardItem,
        cities_model: QStandardItemModel,
        categories_model: QStandardItemModel):
    if not Path(file_path).is_file():
        return

    with open(file_path, 'r', encoding='utf-8') as file:
        state = json.load(file)

    shop_parser.ui.filter.set_item_enabled(shop_item, state[IS_ENABLED_JSON_KEY])
    select_cities_by_state(cities_model, state[CITIES_JSON_KEY])
    select_categories_by_state(categories_model, state[CATEGORIES_JSON_KEY])


def get_shop_state(
        shop_item: QStandardItem,
        cities_model: QStandardItemModel,
        categories_model: QStandardItemModel
) -> dict:
    shop_name = shop_item.text()
    out = {
        NAME_JSON_KEY: shop_name,
        IS_ENABLED_JSON_KEY: shop_parser.ui.filter.is_item_enabled(shop_item),
        CITIES_JSON_KEY: get_cities_state(cities_model),
        CATEGORIES_JSON_KEY: get_categories_state(categories_model)
    }

    return out


def get_cities_state(model: QStandardItemModel) -> list:
    out = []
    item_count = model.rowCount()
    for row in range(item_count):
        item = model.item(row)
        state = get_standard_item_state(item)

        out.append(state)

    return out


def get_categories_state(model: QStandardItemModel) -> list:
    out = []
    item_count = model.rowCount()
    for row in range(item_count):
        item = model.item(row)
        state = get_standard_item_state(item)
        state[CHILDREN_JSON_KEY] = get_standard_item_children_state(item)

        out.append(state)

    return out


def get_standard_item_state(item: QStandardItem) -> dict:
    return {
        NAME_JSON_KEY: item.text(),
        IS_ENABLED_JSON_KEY: shop_parser.ui.filter.is_item_enabled(item)
    }


def get_standard_item_children_state(item: QStandardItem) -> list:
    out = []

    child_count = item.rowCount()
    if child_count <= 0:
        return out

    for row in range(child_count):
        child_item = item.child(row)
        state = get_standard_item_state(child_item)
        state[CHILDREN_JSON_KEY] = get_standard_item_children_state(child_item)
        out.append(state)

    return out


def select_cities_by_state(model: QStandardItemModel, states: list[dict]):
    items = shop_parser.ui.filter.get_top_level_items(model)
    for state in states:
        item = shop_parser.ui.filter.find_item_by_name(items, state[NAME_JSON_KEY])
        if not item:
            continue

        shop_parser.ui.filter.set_item_enabled(item, state[IS_ENABLED_JSON_KEY])


def select_categories_by_state(model: QStandardItemModel, states: list[dict]):
    items = shop_parser.ui.filter.get_top_level_items(model)
    select_standard_items_by_state(items, states)


def select_standard_items_by_state(items: list[QStandardItem], states: list[dict]):
    for state in states:
        item = shop_parser.ui.filter.find_item_by_name(items, state[NAME_JSON_KEY])
        if not item:
            continue

        is_enabled = state[IS_ENABLED_JSON_KEY]
        shop_parser.ui.filter.set_item_enabled(item, is_enabled)
        if not is_enabled:
            continue

        state_children = state[CHILDREN_JSON_KEY]
        child_items = shop_parser.ui.filter.get_children(item)
        select_standard_items_by_state(child_items, state_children)
