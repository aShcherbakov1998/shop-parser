from anytree import Node
import shop_parser.core.data


def get_unique_strings(strings: list[str]):
    return list(dict.fromkeys(strings))


def get_unique_city_names(cities: list[shop_parser.core.data.City]):
    city_names = []
    for city in cities:
        city_names.append(city.name)

    return get_unique_strings(city_names)


# Returns 2 lists:
# 1: city list
# 2: category node list
def merge_into_unique_lists(shop: shop_parser.core.data.Shop):
    out_city_list = get_unique_city_names(shop.cities)
    out_category_node_list: list[Node] = []

    is_new_node = True

    for city in shop.cities:
        for category in city.categories:
            existing_node = None
            is_new_node = True
            for node in out_category_node_list:
                if category.name == node.name:
                    existing_node = node
                    is_new_node = False
                    break

            if existing_node is None:
                existing_node = Node(category.name)

            if len(category.children) > 0:
                add_nodes(existing_node, category.children)

            if is_new_node:
                out_category_node_list.append(existing_node)

    return out_city_list, out_category_node_list


def add_nodes(parent_node: Node, categories: list[shop_parser.core.data.Category]):
    for category in categories:
        existing_node = None
        for node in parent_node.children:
            if category.name == node.name:
                existing_node = node
                break

        if existing_node is None:
            existing_node = Node(category.name, parent=parent_node)

        if len(category.children) > 0:
            add_nodes(existing_node, category.children)
