import logging
from anytree import Node

import threading
from threading import Thread
from queue import Queue
from collections import OrderedDict
from configparser import ConfigParser
import subprocess, os

from PySide6 import QtCore
from PySide6.QtCore import Qt, QSettings, QCoreApplication, QTimer
from PySide6.QtGui import QStandardItemModel, QStandardItem
from PySide6.QtWidgets import (
    QMainWindow,
    QWidget,
    QProgressBar,
    QLabel,
    QHBoxLayout,
    QVBoxLayout,
    QListView,
    QTreeView
)

from shop_parser.translations import _

import shop_parser.ui.core
from shop_parser.ui.tristatestandarditem import TristateStandardItem
from shop_parser.ui.widgets import GeneralSettingsWidget, ViewContainer, ActionPanel
import shop_parser.ui.utils
import shop_parser.ui.filter
import shop_parser.ui.io.checkbox_state

import shop_parser.core.data
import shop_parser.core.parser
import shop_parser.core.items
import shop_parser.core.product_keys
import shop_parser.core

from shop_parser.io.products import ProductWriter
import shop_parser.io.cache

import shop_parser.core.misc.path
from shop_parser.core.misc.callback_helpers import OnParserProgressChangedCallbackHelper
from shop_parser.core.misc.callback_helpers import OnProductReadyCallbackHelper

from shop_parser.parsers.fakeparser import FakeShopParser  # TODO: remove?
from shop_parser.parsers import (
    MaxidomParser,
    MegastroyParser,
    PetrovichParser,
    ObiParser,
    LeroyMerlinParser,
    AfonyaParser,
    VodopadParser,
    AksonParser,
    BaucenterParser,
    VodoleyParser,
    AkvalinkParser,
    CastoramaParser,
    DobrostroyParser,
    StroyudachaParser,
)

PARSER_CLASSES = [
    MegastroyParser,
    MaxidomParser,
    PetrovichParser,
    ObiParser,
    LeroyMerlinParser,
    AfonyaParser,
    VodopadParser,
    AksonParser,
    BaucenterParser,
    VodoleyParser,
    AkvalinkParser,
    CastoramaParser,
    DobrostroyParser,
    StroyudachaParser,
]


class MainWindow(QMainWindow):
    parse_shops_finished = QtCore.Signal()
    parse_shop_finished = QtCore.Signal()

    def __init__(self):
        super(MainWindow, self).__init__()

        self.logger = logging.getLogger('ui')
        self._settings = QSettings(
            QCoreApplication.organizationName(),
            QCoreApplication.applicationName())

        self.shops: list[shop_parser.core.data.Shop] = []
        self.cities_map: dict[str, QStandardItemModel] = {}
        self.categories_map: dict[str, QStandardItemModel] = {}

        self.total_category_count = 0
        self.parsers: dict[str, shop_parser.core.parser.ShopParser] = {}
        self.parser_progress_callback_helpers: list[OnParserProgressChangedCallbackHelper] = []
        self.parser_threads: list[Thread] = []
        self.parser_callback_helpers: list[OnProductReadyCallbackHelper] = []
        self.parser_thread_count: int = 0
        self.parser_finished_count: int = 0
        self.is_abort_clicked = False
        self.product_queue = Queue()
        self.product_queue_sentinel = object()
        self.product_consumer_thread = None
        self.product_writer = None

        self.write_cache_timer = QTimer(self)
        self.write_cache_timer.timeout.connect(self.on_write_cache_timeout)
        self.write_cache_timer_delay = 60000  # in milliseconds
        self.write_cache_sentinel = object()

        self.shop_parser_master_thread = None
        self.shop_parser_master_lock = threading.Lock()  # Used to add a parsed shop

        self.setWindowTitle(_('Shop parser'))
        self.create_widgets()
        self.create_shop_parsers()
        self.restore_settings()

        self._remove_old_files()

    def restore_settings(self):
        self.general_settings_widget.restore_state(self._settings.value('settings'))
        self.restoreState(self._settings.value('state'))
        self.restoreGeometry(self._settings.value('geometry'))

    def create_widgets(self):
        self.general_settings_widget = GeneralSettingsWidget()

        self.settings_menu = self.menuBar().addMenu(_('Settings'))
        self.general_settings_action = self.settings_menu.addAction(_('General...'))
        self.general_settings_action.triggered.connect(self.on_general_settings_action_clicked)

        self.tools_menu = self.menuBar().addMenu('Инструменты')  # TODO: Дааань =)
        self.open_parsed_data_folder_action = self.tools_menu.addAction('Открыть каталог данных...')
        self.open_parsed_data_folder_action.triggered.connect(self.open_parsed_data_folder)

        self.shop_list_widget = self.create_shop_list_widget()
        self.city_list_widget = self.create_city_list_widget()
        self.category_tree_widget = self.create_category_tree_widget()

        shop_layout = QHBoxLayout()
        shop_layout.addWidget(ViewContainer(self.shop_list_widget, _('Shops')))
        shop_layout.addWidget(ViewContainer(self.city_list_widget, _('Cities')))
        shop_layout.addWidget(ViewContainer(self.category_tree_widget, _('Categories')))

        button_layout = QHBoxLayout()
        self.progress_bar = QProgressBar()
        self.progress_bar.setValue(0)
        self.progress_bar.setFormat('%v / %m')
        button_layout.addWidget(self.progress_bar)
        self.progress_bar.hide()

        self.total_category_count_label = QLabel()
        self.statusBar().addPermanentWidget(self.total_category_count_label)
        self.total_category_count_label.hide()

        self.action_panel = ActionPanel(self)
        self.action_panel.start_requested.connect(self.start_parse)
        self.action_panel.stop_requested.connect(self.stop_parse)
        self.action_panel.clear_requested.connect(self.on_clear_cache_button_clicked)

        button_layout.addStretch()
        button_layout.addWidget(self.action_panel)

        main_layout = QVBoxLayout()
        main_layout.addLayout(shop_layout)
        main_layout.addLayout(button_layout)

        central_widget = QWidget()
        central_widget.setLayout(main_layout)

        self.setCentralWidget(central_widget)

        self.status_label = QLabel('')
        self.statusBar().addWidget(self.status_label)

    def create_shop_list_widget(self):
        self.shop_model = QStandardItemModel()
        widget = QListView()
        widget.setModel(self.shop_model)

        widget.selectionModel().selectionChanged.connect(self.on_row_selected)

        return widget

    def closeEvent(self, event):
        self._join_parser_threads()
        self._join_product_consumer_thread()

        self._settings.setValue('settings', self.general_settings_widget.save_state())
        self._settings.setValue('state', self.saveState())
        self._settings.setValue('geometry', self.saveGeometry())

        self.write_checkbox_state()

        event.accept()

    def on_general_settings_action_clicked(self):
        shop_parser.ui.utils.show_widget_on_top(self.general_settings_widget)

    def open_parsed_data_folder(self):
        dir = rf'{self.general_settings_widget.products_root_dir()}\parsed-data'
        dir = dir if os.path.exists(dir) else os.getcwd()
        subprocess.Popen(f'explorer "{dir}"')

    def on_row_selected(self, current, previous):
        selected_index = current.indexes()[0]
        selected_item = self.shop_model.item(selected_index.row(), selected_index.column())
        shop_name = selected_item.text()

        self.city_list_widget.setModel(self.cities_map[shop_name])
        self.category_tree_widget.setModel(self.categories_map[shop_name])

    def on_write_cache_timeout(self):
        self.product_queue.put(self.write_cache_sentinel)

    def start_parse(self):
        # Start
        self.is_abort_clicked = False
        self.logger.info('Starting parsing process...')
        self.status_label.setText(_('Parsing the data...'))

        total_iteration_count = 0
        done_iteration_count = 0
        for shop in self.shops:
            parser = self.parsers[shop.name]
            done_iteration_count += parser.get_cache_entry_count()

            if not self.is_shop_selected(shop.name):
                continue

            shop = shop_parser.ui.filter.filter_shop(shop, self.cities_map[shop.name], self.categories_map[shop.name])
            if not shop.cities:
                continue

            parser_iteration_count = parser.get_iteration_count(shop)
            if parser_iteration_count <= 0:
                continue

            total_iteration_count += parser_iteration_count
            callback_helper = OnProductReadyCallbackHelper(shop.name, self.on_product_ready)
            thread = Thread(target=parser.parse_categories, args=(shop, callback_helper.on_product_ready,))
            # thread = Thread(target=parser.parse_categories, args=(shop, self.on_product_ready, ))
            self.parser_threads.append(thread)
            self.parser_callback_helpers.append(callback_helper)
            self.logger.info(f'Shop "{shop.name}". Category count: {parser_iteration_count}')

        if total_iteration_count <= 0:
            self.logger.info(f'All categories have already been parsed!')
            self.status_label.setText(_('All categories have already been parsed! '
                                        'Clear cache or choose new categories...'))
            self.progress_bar.hide()
            return

        # Reset the state
        self.parser_thread_count = len(self.parser_threads)
        self.parser_finished_count = 0
        self.progress_bar.setMaximum(total_iteration_count + done_iteration_count)
        self.progress_bar.setValue(done_iteration_count)
        self.progress_bar.show()

        self.action_panel.set_action(ActionPanel.Action.STOP)
        self.action_panel.set_clear_cache_enabled(False)

        if self.product_writer is None:
            self.product_writer = ProductWriter(self.general_settings_widget.products_root_dir())
            self.write_session_config()

        self.product_consumer_thread = Thread(target=self.product_consumer)
        self.product_consumer_thread.start()
        for thread in self.parser_threads:
            thread.start()

        self.write_cache_timer.start(self.write_cache_timer_delay)

    def stop_parse(self):
        self.is_abort_clicked = True
        self._join_parser_threads()
        self.logger.info('Stopping parsing process...')

    def on_clear_cache_button_clicked(self):
        for shop_name, parser in self.parsers.items():
            parser.clear_category_cache()

        self.product_writer = None
        self.save_session()

        self.progress_bar.hide()
        self.status_label.setText(_('Cache cleared!'))

    def on_parser_progress_changed(self, shop_name: str, progress: int):
        # print(f'[Progress] "{shop_name}" = {progress}')
        self.progress_bar.setValue(self.progress_bar.value() + 1)

    def on_parser_finished(self):
        self.parser_finished_count += 1

        self.logger.info(f'Parser finished: {self.parser_finished_count}/{self.parser_thread_count}')
        if self.parser_finished_count == self.parser_thread_count:
            self.on_parsing_process_finished()

    def on_parsing_process_finished(self):
        self._join_parser_threads()
        self._join_product_consumer_thread()

        if self.is_abort_clicked:
            self.logger.info(f'Parsing is done, but abort has been clicked...')
            self.status_label.setText(_('Parsing has been stopped...'))
            self.action_panel.set_action(ActionPanel.Action.RESUME)
        else:
            self.logger.info(f'Parsing process is done!')
            self.status_label.setText(_('Parsing is done!'))
            self.action_panel.set_action(ActionPanel.Action.START)

        self.write_cache_timer.stop()
        self.product_writer.merge()

        self.action_panel.set_clear_cache_enabled(True)

    def on_product_ready(self, shop_name: str, city_name: str, product: OrderedDict):
        message = {
            'product': product,
            'shop_name': shop_name,
            'city_name': city_name
        }

        self.product_queue.put(message)

    def product_consumer(self):
        while True:
            message = self.product_queue.get()
            if message is self.product_queue_sentinel:  # break the loop. 'sentinel' is the stop message
                self.product_queue.put(self.product_queue_sentinel)
                break
            elif message is self.write_cache_sentinel:
                self.save_session()
                continue

            product = message['product']
            shop_name = message['shop_name']
            city_name = message['city_name']

            if shop_parser.core.product_keys.NAME in product:
                product_name = product[shop_parser.core.product_keys.NAME]
                # self.logger.debug(f'[{shop_name}: {city_name}] Got the product: {product_name}')

            self.product_writer.write(shop_name, city_name, product)

        # print(f'[Product queue]: Done')
        self.save_session()

    def create_city_list_widget(self):
        widget = QListView()
        return widget

    def create_category_tree_widget(self):
        widget = QTreeView()
        widget.setHeaderHidden(True)
        return widget

    def create_city_and_category_models(self, shop: shop_parser.core.data.Shop):
        city_names, category_node_list = shop_parser.ui.core.merge_into_unique_lists(shop)
        out_cities_model = self.convert_to_cities_model(city_names)
        out_categories_model = self.convert_to_categories_model(category_node_list)

        return out_cities_model, out_categories_model

    def convert_to_cities_model(self, city_names: list[str]) -> QStandardItemModel:
        city_names.sort()

        out = QStandardItemModel()
        for city_name in city_names:
            item = QStandardItem(city_name)
            item.setCheckable(True)
            item.setCheckState(Qt.Checked)
            item.setEditable(False)
            out.appendRow(item)

        return out

    def convert_to_categories_model(self, category_node_list: list[Node]):
        category_node_list = sort_nodes(category_node_list)

        out = QStandardItemModel()
        for node in category_node_list:
            item = self.convert_to_tristate_standard_item(node)
            out.appendRow(item)

        return out

    def convert_to_tristate_standard_item(self, node: Node) -> TristateStandardItem:
        item = TristateStandardItem(node.name)
        item.setCheckable(True)
        item.setCheckState(Qt.Unchecked)
        item.setEditable(False)
        item.setFlags(item.flags() | Qt.ItemIsAutoTristate)

        child_nodes = []
        for child_node in node.children:
            child_nodes.append(child_node)

        child_nodes = sort_nodes(child_nodes)
        for child_node in child_nodes:
            sub_item = self.convert_to_tristate_standard_item(child_node)
            item.appendRow(sub_item)

        return item

    def create_shop_parsers(self):
        urls = []
        parsers = []

        for parser_class in PARSER_CLASSES:
            parser_instance = parser_class()
            url = parser_instance.url

            parsers.append(parser_instance)
            urls.append(url)

        # parsers.append(FakeShopParser('fake shop 1', 0.1))
        # parsers.append(FakeShopParser('fake shop 2', 0.1))
        # parsers.append(FakeShopParser('fake shop 3', 0.15))
        # parsers.append(FakeShopParser('fake shop 4', 0.15))
        # parsers.append(FakeShopParser('fake shop 5', 0.18))
        # parsers.append(FakeShopParser('fake shop 6', 0.19))
        # parsers.append(FakeShopParser('fake shop 7', 0.2))
        # parsers.append(FakeShopParser('fake shop 8', 0.2))
        # parsers.append(FakeShopParser('fake shop 9', 0.24))
        # parsers.append(FakeShopParser('fake shop 10', 0.27))
        # parsers.append(FakeShopParser('fake shop 11', 0.25))

        fake_url = 'https://z0r.de'
        for i in range(len(urls), len(parsers)):
            urls.append(fake_url)

        self.status_label.setText(_('Parsing category and city lists from shops...'))
        self.progress_bar.setRange(0, len(parsers))
        self.progress_bar.show()
        self.action_panel.setEnabled(False)

        self.shop_parser_master_thread = Thread(target=self.parse_shops, args=(parsers, urls,))
        self.parse_shops_finished.connect(self.on_parse_shops_finished)
        self.parse_shop_finished.connect(self.on_parse_shop_finished)
        self.shop_parser_master_thread.start()

    def parse_shops(self, parsers, urls):
        threads = []
        for i in range(len(parsers)):
            parser = parsers[i]
            url = urls[i]
            thread = Thread(target=self.parse_shop, args=(parser, url,))
            threads.append(thread)

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

        self.parse_shops_finished.emit()

    def parse_shop(self, parser, url):
        shop = parser.parse_shop(url)
        with self.shop_parser_master_lock:
            self.parsers[shop.name] = parser
            self.add_shop(shop)
            self.read_checkbox_state(shop.name)

        self.parse_shop_finished.emit()

    def on_parse_shop_finished(self):
        progress_value = self.progress_bar.value() + 1
        self.progress_bar.setValue(progress_value)
        # print(f'[Parse shop] Finished {progress_value} / {self.progress_bar.maximum()}')

    def on_parse_shops_finished(self):
        self.logger.info('Shop parsing is done! (Cities and categories)')
        for shop in self.shops:
            parser = self.parsers[shop.name]
            self.total_category_count += parser.get_iteration_count(shop)

            # parser.progress_changed.connect(self.on_parser_progress_changed)
            callback_helper = OnParserProgressChangedCallbackHelper(shop.name, self.on_parser_progress_changed)
            parser.progress_changed.connect(callback_helper.on_progress_changed)
            self.parser_progress_callback_helpers.append(callback_helper)

            parser.finished.connect(self.on_parser_finished)

        self.status_label.setText(_('Choose shops, cities, categories that you need'))
        self.progress_bar.hide()
        self.action_panel.setEnabled(True)

        self.total_category_count_label.setText(_('Total categories: {}').format(self.total_category_count))
        self.total_category_count_label.show()

        self.load_session()

    def add_shop(self, shop: shop_parser.core.data.Shop):
        self.shops.append(shop)

        item = QStandardItem(shop.name)
        item.setCheckable(True)
        item.setCheckState(Qt.Unchecked)
        item.setEditable(False)
        self.shop_model.appendRow(item)

        cities_model, categories_model = self.create_city_and_category_models(shop)
        self.cities_map[shop.name] = cities_model
        self.categories_map[shop.name] = categories_model

    def is_shop_selected(self, shop_name: str) -> bool:
        return shop_parser.ui.filter.is_item_selected(self.shop_model, shop_name)

    def set_shop_selected(self, shop_name: str, selected: bool = True):
        shop_parser.ui.filter.set_item_selected(self.shop_model, shop_name, selected)

    def is_city_selected(self, shop_name: str, city_name: str) -> bool:
        return shop_parser.ui.filter.is_item_selected(self.cities_map[shop_name], city_name)

    def set_city_selected(self, shop_name: str, city_name: str, selected: bool = True):
        shop_parser.ui.filter.set_item_selected(self.cities_map[shop_name], city_name, selected)

    def print_shop(self, shop: shop_parser.core.data.Shop):
        print(f'Shop name: {shop.name}')
        for city in shop.cities:
            print(f'City name: {city.name}')
            self.print_categories(city.categories)

    def print_categories(self, categories, indent_level=1):
        indent_string = ''
        for i in range(indent_level):
            indent_string += '  '

        for category in categories:
            print(f'{indent_string}Category name: {category.name}')
            if category.children:
                self.print_categories(category.children, indent_level + 1)

    def _remove_old_files(self):
        dir_path_list = [
            shop_parser.core.LOGS_DIRECTORY,
            shop_parser.core.CACHE_DIRECTORY + '/error_pages'
        ]

        remove_old_dir_paths(dir_path_list)

    def _join_parser_threads(self):
        for parser in self.parsers.values():
            parser.interrupted = True

        for thread in self.parser_threads:
            thread.join()

        self.parser_threads.clear()
        self.parser_callback_helpers.clear()

    def _join_product_consumer_thread(self):
        if self.product_consumer_thread:
            self.product_queue.put(self.product_queue_sentinel)
            self.product_consumer_thread.join()

        self.product_queue.queue.clear()

    def get_cache_file_path(self, shop_name: str) -> str:
        out = self.get_cache_dir_path()
        out += '/shops'
        out += '/' + shop_name + '.txt'

        return out

    def get_cache_dir_path(self) -> str:
        out = self.general_settings_widget.products_root_dir()
        out += '/parsed-data'
        out += '/cache'

        return out

    def write_cache(self):
        for shop in self.shops:
            parser = self.parsers[shop.name]
            file_path = self.get_cache_file_path(shop.name)
            shop_parser.io.cache.write_cache(file_path, parser)

    def read_cache(self):
        for shop in self.shops:
            parser = self.parsers[shop.name]
            cache_file_path = self.get_cache_file_path(shop.name)
            shop_parser.io.cache.read_cache(cache_file_path, parser)

    def get_session_path(self) -> str:
        out = self.get_cache_dir_path()
        out += '/session.ini'

        return out

    def save_session(self):
        self.write_cache()
        self.write_session_config()

    def load_session(self):
        self.read_cache()
        self.read_session_config()

    def write_session_config(self):
        session_path = self.get_session_path()
        config = ConfigParser()
        config.add_section('general')
        if self.product_writer:
            config.set('general', 'date-prefix', self.product_writer.get_date_prefix())
            config.set('general', 'time-prefix', self.product_writer.get_time_prefix())

        with open(session_path, 'w', encoding='utf-8') as file:
            config.write(file)

    def read_session_config(self):
        session_path = self.get_session_path()
        config = ConfigParser()
        config.read(session_path)

        if config.has_option('general', 'date-prefix') and config.has_option('general', 'time-prefix'):
            date_prefix = config.get('general', 'date-prefix')
            time_prefix = config.get('general', 'time-prefix')
            products_root_dir = self.general_settings_widget.products_root_dir()
            self.product_writer = ProductWriter(products_root_dir, date_prefix, time_prefix)

    def write_checkbox_state(self):
        shop_items = shop_parser.ui.filter.get_top_level_items(self.shop_model)
        for shop_item in shop_items:
            shop_name = shop_item.text()
            cities_model = self.cities_map[shop_name]
            categories_model = self.categories_map[shop_name]

            file_path = get_checkbox_state_file_path(shop_name)
            shop_parser.ui.io.checkbox_state.write(file_path, shop_item, cities_model, categories_model)

    def read_checkbox_state(self, shop_name: str):
        shop_item = shop_parser.ui.filter.find_item_in_model_by_name(self.shop_model, shop_name)
        cities_model = self.cities_map[shop_name]
        categories_model = self.categories_map[shop_name]

        file_path = get_checkbox_state_file_path(shop_name)
        shop_parser.ui.io.checkbox_state.read(file_path, shop_item, cities_model, categories_model)


def remove_old_dir_paths(dir_path_list: list[str]):
    for dir_path in dir_path_list:
        shop_parser.core.misc.path.remove_old_files_in_dir(dir_path)


def get_checkbox_state_file_path(shop_name: str) -> str:
    out = get_ui_settings_dir_path()
    out += '/shops'
    out += '/' + shop_name + '.json'

    return out


def get_ui_settings_dir_path() -> str:
    out = shop_parser.core.CACHE_DIRECTORY
    out += '/settings'
    out += '/ui'

    return out


def sort_nodes(nodes: list[Node]) -> list[Node]:
    def sort_node_func(node):
        return node.name

    nodes.sort(key=sort_node_func)
    return nodes
