from PySide6.QtCore import Qt
from PySide6.QtGui import QStandardItem


# https://stackoverflow.com/questions/49144042/itemisautotristate-flag-not-working-as-expected
class TristateStandardItem(QStandardItem):
    def data(self, role=Qt.UserRole + 1):
        if role == Qt.CheckStateRole and self.hasChildren():
            return self._childrenCheckState()
        return super().data(role)

    def setData(self, value, role=Qt.UserRole + 1):
        if role == Qt.CheckStateRole:
            if value != Qt.PartiallyChecked:
                for row in range(self.rowCount()):
                    for column in range(self.columnCount()):
                        child = self.child(row, column)
                        if child.data(role) is not None:
                            child.setData(value, role)

        super().setData(value, role)
        if self.model() is not None:
            # update all model data.
            self.model().dataChanged.emit(
                self.model().index(0, 0), self.model().index(self.model().rowCount(), 0),
                [Qt.CheckStateRole])

    def _childrenCheckState(self):
        checked = unchecked = False
        for row in range(self.rowCount()):
            for column in range(self.columnCount()):
                child = self.child(row, column)
                value = child.data(Qt.CheckStateRole)
                if value is None:
                    return
                elif value == Qt.Unchecked:
                    unchecked = True
                elif value == Qt.Checked:
                    checked = True
                else:
                    return Qt.PartiallyChecked
                if unchecked and checked:
                    return Qt.PartiallyChecked
        if unchecked:
            return Qt.Unchecked
        elif checked:
            return Qt.Checked
