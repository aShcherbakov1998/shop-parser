# Shop Parser

A simple python web-scrapper for predefined list of shops.


# Roadmap

[ROADMAP.md](ROADMAP.md)


# Usage

Write in the terminal:

```bash
python -m shop_parser
```

---


# Requirements

## pip packages
```bash
pip install -r requirements.txt
```

## Windows: OpenSSL
1. Go to [OpenSSL binaries website][1]
2. Choose OpenSSL 1.1.1 light version (x64)
3. Download the binaries ([exe][2] or [msi][3])
4. Install with default parameters
5. Don't forget to turn off any donation checkboxes at the end of the installation


## Translations

Translation helper scripts are located in the python root directory. 

You need to change `{python_root}` in the following commands to your python root directory.

For example, python root directory `{python_root}` might look like this:

`C:/Users/username/AppData/Local/Programs/Python/Python39`


### Generate

In order to generate translation (`mo`) files you need to run the following command:

```bash
python "{python_root}/Tools/i18n/msgfmt.py" -o "locales/ru/LC_MESSAGES/base.mo" "locales/ru/LC_MESSAGES/base"
```

where `{python_root}` is the python root directory.

For example, if `{python_root}` is `C:/Users/username/AppData/Local/Programs/Python/Python39`, then the command will be:
```bash
python "C:/Users/username/AppData/Local/Programs/Python/Python39/Tools/i18n/msgfmt.py" -o "locales/ru/LC_MESSAGES/base.mo" "locales/ru/LC_MESSAGES/base"
```


### Update

You need to update translations only when you're adding new strings that need to be translated.

Run the following command to update the translation template (`pot`):

```bash
python "{python_root}/Tools/i18n/pygettext.py" -d base -o "locales/base.pot" "shop_parser"
```

Then copy new strings from `locales/base.pot` to `locales/ru/LC_MESSAGES/base.po`.

And translate the new added strings.

---


# Testing

To test the package write in the terminal:
```bash
python -m pytest
```

Note that you might need to install the **pytest** package:
```bash
pip install pytest
```

---


# Generate executable

In order to generate the executable you need the `pyinstaller` module:
```bash
pip install pyinstaller
```

To create the executable run the following command:
```bash
pyinstaller --name "shop-parser" --add-data "shop_parser/logging.conf;." --add-data "locales/ru/LC_MESSAGES/*.mo;locales/ru/LC_MESSAGES" "shop_parser/__main__.py" --icon=shop_parser/re
s/icons8_spider-man_new.ico
```

After all that, the `dist` folder will contain the `shop-parser` executable.

Note: `pyinstaller` might ask you several questions (`y/N`), so pay attention to the output of `pyinstaller`.

Note: if you want to get rid of the console window, pass `-w` when running `pyinstaller`.


---

# Misc

## Network manager test application

If you want to quickly get a reply from a specified URL, you can use 'network manager' application:
```bash
python "tests/network/test_networkmanager.py"
```

Note: you must run this script from the project root directory (where `README.md` file is)


[1]: https://slproweb.com/products/Win32OpenSSL.html
[2]: https://slproweb.com/download/Win64OpenSSL_Light-1_1_1L.msi
[3]: https://slproweb.com/download/Win64OpenSSL_Light-1_1_1L.exe
