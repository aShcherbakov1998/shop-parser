import shop_parser.network.url


def test_url_update_params():
    url = 'https://www.castorama.ru/gardening-and-outdoor/seeds?limit=24&p=2'
    expected_url = 'https://www.castorama.ru/gardening-and-outdoor/seeds?limit=96&p=3'
    expected_limit = '96'
    expected_page = '3'

    params = {
        'limit': expected_limit,
        'p': expected_page,
    }

    assert shop_parser.network.url.update_params(url, params) == expected_url


def test_url_with_old_params():
    url = 'https://www.castorama.ru/gardening-and-outdoor/seeds?limit=96'
    expected_url = 'https://www.castorama.ru/gardening-and-outdoor/seeds?limit=96&p=11'
    expected_page = '11'

    params = {
        'p': expected_page,
    }

    assert shop_parser.network.url.update_params(url, params) == expected_url


def test_url_with_new_params():
    url = 'https://www.castorama.ru/gardening-and-outdoor/seeds'
    expected_url = 'https://www.castorama.ru/gardening-and-outdoor/seeds?p=11'
    expected_page = '11'

    params = {
        'p': expected_page,
    }

    assert shop_parser.network.url.update_params(url, params) == expected_url


def test_url_without_old_params():
    url = 'https://www.castorama.ru/gardening-and-outdoor/seeds?limit=96'
    expected_url = 'https://www.castorama.ru/gardening-and-outdoor/seeds?p=24'
    expected_page = '24'

    params = {
        'p': expected_page,
    }

    assert shop_parser.network.url.update_params(url, params, clear_old_params=True) == expected_url
