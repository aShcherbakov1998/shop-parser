import os
import sys

if __name__ == '__main__':  # For some reason Python can't find other custom modules
    sys.path.append(os.getcwd())  # So we add them directly

from pytestqt.qtbot import QtBot
import logging.config
from PySide6.QtCore import QCoreApplication, QTimer

from shop_parser.network import NetworkManager, Reply, Request
import tests.common


def test_networkmanager(qtbot: QtBot):
    """Simply asserts that the callback comes..."""
    manager = NetworkManager()
    with qtbot.waitCallback() as cb:
        request = Request(
            url='https://pytest-qt.readthedocs.io/en/master/wait_callback.html',
            callback=cb,
        )

        manager.get(request)

    return manager


def not_pytest_test_networkmanager():
    app = QCoreApplication()
    manager = NetworkManager()

    urls = [
        'https://leroymerlin.ru/catalogue/ugol-drova-i-sredstva-dlya-rozzhiga/zazhigalki/',
        'https://barnaul.leroymerlin.ru/catalogue/kotly-otopitelnye-gazovye/bosch/',
        'https://barnaul.leroymerlin.ru/catalogue/metly-veniki-i-tovary-dlya-uborki/veniki/',
        'https://barnaul.leroymerlin.ru/catalogue/dekorativnye-derevya-i-kustarniki/beresklet/',
        'https://belgorod.leroymerlin.ru/catalogue/kartridzhi-dlya-vody/novaya-voda/',
        'https://belgorod.leroymerlin.ru/catalogue/filtry-kuvshiny/zheltye/',
        'https://barnaul.leroymerlin.ru/catalogue/reduktory-davleniya/1-2-dyuyma/',
        'https://barnaul.leroymerlin.ru/catalogue/rabica-i-setki-dlya-zaborov/',
        'https://barnaul.leroymerlin.ru/catalogue/polotencesushiteli-vodyanye-i-elektricheskie/elektricheskie/',
        'https://belgorod.leroymerlin.ru/catalogue/gazonokosilki/51-sm/',
        'https://spb.leroymerlin.ru/catalogue/pistolety-dlya-poliva/karcher/',
        'https://belgorod.leroymerlin.ru/catalogue/schetchiki-dlya-vody/',
        'https://arkhangelsk.leroymerlin.ru/catalogue/cepnye-pily/elektricheskie/'
    ]

    for url in urls:
        manager.get(url, lambda reply: None)

    timer = QTimer()
    timer.timeout.connect(lambda: app.quit() if manager.requests_in_queue() == 0 else None)
    timer.start(10000)

    app.exec()


def start_network_manager_dialog():
    from PySide6.QtGui import QDesktopServices
    from PySide6.QtCore import QUrl, QStandardPaths
    from PySide6.QtWidgets import (
        QApplication,
        QWidget,
        QLineEdit,
        QPushButton,
        QFormLayout,
        QPlainTextEdit,
        QBoxLayout,
        QCheckBox
    )

    a = QApplication()
    w = QWidget()
    url = QLineEdit()
    url.setPlaceholderText('https://www.google.ru')
    cookies = QPlainTextEdit()
    cookies.setPlaceholderText('page=3; city=SPb; ID=12345')
    cookies.setToolTip(
        f"Pairs of strings 'key=value', separated by semicolon. \n"
        f"Example: {cookies.placeholderText()}")
    session = QCheckBox('Hold session')
    session.setToolTip("If 'checked', cookies are managed automatically."
                       "If cookies are passed explicitly, the session "
                       "cookies are updated with provided dict. ")
    ua = QLineEdit()
    ua.setPlaceholderText('auto')
    ua.setToolTip('If empty, the "user-agent" header is generated automatically.')

    send = QPushButton("GET")
    box_layout = QBoxLayout(QBoxLayout.TopToBottom)
    form_layout = QFormLayout()
    form_layout.addRow('url:', url)
    form_layout.addRow("Cookies:", cookies)
    form_layout.addRow('User-agent', ua)
    box_layout.addLayout(form_layout)
    box_layout.addWidget(session)
    box_layout.addWidget(send)
    w.setLayout(box_layout)
    nm = NetworkManager()
    nm.set_keep_session(False)

    def callback(reply: Reply, **kwargs):
        file_path = QStandardPaths.writableLocation(QStandardPaths.TempLocation) + '/reply.txt'
        with open(file_path, 'w', encoding='utf-8') as file:
            file.write(reply.text)
        QDesktopServices().openUrl(QUrl.fromLocalFile(file_path))

    def form_request() -> Request:
        cookie_pairs = cookies.toPlainText().split(';')
        cookies_dict = {}
        for key_value in cookie_pairs:
            try:
                key, value = key_value.split('=')
                cookies_dict[key.strip()] = value.strip()
            except Exception:
                pass

        return Request(url.text(), callback=callback, cookies=cookies_dict,
                       keep_session=session.isChecked(),
                       user_agent=ua.text().strip())

    send.clicked.connect(lambda: nm.get(form_request()))
    url.returnPressed.connect(lambda: nm.get(form_request()))

    w.show()
    a.exec()


def init_logging():
    logging.config.fileConfig(tests.common.get_logging_file_path())


def main():
    init_logging()
    start_network_manager_dialog()


if __name__ == '__main__':
    main()
