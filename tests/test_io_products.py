import shop_parser.core.items
import shop_parser.io.products

import tests.common


def test_read_products():
    vendor_code_label = 'Vendor code'
    category_label = 'Category'
    name_label = 'Name'
    price_label = 'Price'
    url_label = 'Url'

    relative_file_path = 'fixtures/io/products.csv'
    expected_header_labels = [vendor_code_label, category_label, name_label, price_label, url_label]
    expected_category = 'main'
    expected_products = [
        {
            vendor_code_label: '123',
            category_label: expected_category,
            name_label: 'something',
            price_label: 500.0,
            url_label: 'https://awesome-product.com/good1.html',
        },
        {
            vendor_code_label: '456',
            category_label: expected_category,
            name_label: 'another thing',
            price_label: 1500.0,
            url_label: 'https://awesome-product.com/good2.html',
        },
        {
            vendor_code_label: '789',
            category_label: expected_category,
            name_label: 'what is this',
            price_label: 2500.5,
            url_label: 'https://awesome-product.com/good3.html',
        },
        {
            vendor_code_label: '001',
            category_label: expected_category,
            name_label: 'good',
            price_label: 50.2,
            url_label: 'https://awesome-product.com/good4.html',
        },
    ]

    file_path = tests.common.get_fixture_absolute_file_path(relative_file_path)
    header_labels = shop_parser.io.products.get_header_labels_from_csv(file_path)
    assert(header_labels == expected_header_labels)

    data = shop_parser.io.products.read_csv_data(file_path, header_labels)
    for i in range(len(data)):
        product = data[i]
        expected_product = expected_products[i]
        assert(len(product) == len(expected_product))

        for key, value in product.items():
            assert(key in expected_product)

            value = product[key]
            expected_value = expected_product[key]
            if isinstance(expected_value, float):
                value = convert_to_float(value)

            assert(value == expected_value)


def convert_to_float(s: str):
    return float(s.replace(',', '.'))
