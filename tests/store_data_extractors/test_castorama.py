from shop_parser.parsers.castorama import functions as castorama
import shop_parser.core.product_keys as product_keys

import tests.common


def test_parse_cities():
    html_file_path = 'fixtures/stores/castorama/html/main-page.html'
    reply_url = 'https://castorama.ru/'
    expected_city_count = 15

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    cities = castorama.parse_cities(reply)
    assert len(cities) == expected_city_count

    city = cities[3]
    assert city.name == 'Санкт-Петербург'
    assert city.domain_name == '57'

    city = cities[4]
    assert city.name == 'Казань'
    assert city.domain_name == '51'

    city = cities[5]
    assert city.name == 'Самара'
    assert city.domain_name == '57'


def test_parse_catalog():
    html_file_path = 'fixtures/stores/castorama/html/main-page.html'
    reply_url = 'https://castorama.ru/'
    expected_tree_depth = 2
    expected_tree_item_count = 137
    expected_toplevel_categories = [
        'Дача и сад',
        'Новогодние товары',
        'Напольные покрытия',
        'Стройматериалы',
        'Кухня',
        'Ванная',
        'Инструменты',
        'Освещение',
        'Керамическая плитка',
        'Обои, шторы и декор',

        'Краски',
        'Окна и двери',
        'Отопление, охлаждение, водоснабжение и вентиляция',
        'Скобяные изделия',
        'Электротовары',
        'Хранение и хозтовары',
    ]

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    category_tree = castorama.parse_catalog(reply)
    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]


def test_catalog_with_many_pages():
    html_file_path = 'fixtures/stores/castorama/html/products-with-many-pages.html'
    reply_url = 'https://www.castorama.ru/gardening-and-outdoor/outdoor-furniture'
    expected_page_count = 5

    page_count = parse_page_count(html_file_path, reply_url)
    assert page_count == expected_page_count


def test_catalog_with_many_pages2():
    html_file_path = 'fixtures/stores/castorama/html/products-with-many-pages2.html'
    reply_url = 'https://www.castorama.ru/gardening-and-outdoor/seeds/vegetable-seeds'
    expected_page_count = 20

    page_count = parse_page_count(html_file_path, reply_url)
    assert page_count == expected_page_count


def test_catalog_with_many_pages3():
    html_file_path = 'fixtures/stores/castorama/html/products-with-many-pages3.html'
    reply_url = 'https://www.castorama.ru/gardening-and-outdoor/seeds/vegetable-seeds?p=8'
    expected_page_count = 20

    page_count = parse_page_count(html_file_path, reply_url)
    assert page_count == expected_page_count


def test_catalog_with_only_one_page():
    html_file_path = 'fixtures/stores/castorama/html/products-with-only-one-page.html'
    reply_url = 'https://www.castorama.ru/gardening-and-outdoor/sadovye-napol-nye-pokrytija/artificial-grass'
    expected_page_count = 1

    page_count = parse_page_count(html_file_path, reply_url)
    assert page_count == expected_page_count


def test_parse_products():
    html_file_path = 'fixtures/stores/castorama/html/products-with-many-pages.html'
    reply_url = 'https://www.castorama.ru/gardening-and-outdoor/outdoor-furniture'
    expected_product_count = 24

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = castorama.parse_products(reply)
    assert len(products) == expected_product_count

    product = products[0]
    assert product[product_keys.CATEGORY] == 'Дача и сад > Садовая мебель'
    assert product[product_keys.VENDOR_CODE] == '541288'
    assert product[product_keys.NAME] == 'Матрас надувной Intex Deluxe 64709, ПВХ, 25 х 152 х 203 см'
    assert product[product_keys.PRICE] == 2290.0
    assert product[product_keys.URL] == 'https://www.castorama.ru/matras-naduvnoj-intex-deluxe-64709-pvh-25-h-152-h-203-sm'

    product = products[2]
    assert product[product_keys.CATEGORY] == 'Дача и сад > Садовая мебель'
    assert product[product_keys.VENDOR_CODE] == '583306'
    assert product[product_keys.NAME] == 'Набор мебели для балкона Keter Jazz, 3 предмета'
    assert product[product_keys.PRICE] == 5150.0
    assert product[product_keys.URL] == 'https://www.castorama.ru/nabor-mebeli-dlja-balkona-keter-jazz-3-predmeta'


def test_parse_products2():
    html_file_path = 'fixtures/stores/castorama/html/products-with-only-one-page.html'
    reply_url = 'https://www.castorama.ru/gardening-and-outdoor/sadovye-napol-nye-pokrytija/artificial-grass'
    expected_product_count = 4
    expected_category_name = 'Дача и сад > Садовые напольные покрытия > Искусственная трава'

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = castorama.parse_products(reply)
    assert len(products) == expected_product_count

    product = products[0]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '560790'
    assert product[product_keys.NAME] == 'Комплект крепления для искусственной травы'
    assert product[product_keys.PRICE] == 628.0
    assert product[product_keys.URL] == 'https://www.castorama.ru/komplekt-kreplenija-dlja-iskusstvennoj-travy'

    product = products[1]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '571867'
    assert product[product_keys.NAME] == 'Искусственная трава 7 мм, в рулоне 1 х 4 м'
    assert product[product_keys.PRICE] == 1478.0
    assert product[product_keys.URL] == 'https://www.castorama.ru/iskusstvennaja-trava-7-mm-v-rulone-1-h-4-m'

    product = products[2]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '560783'
    assert product[product_keys.NAME] == 'Искусственная трава Dennis 20 мм, в рулоне 1 х 4 м'
    assert product[product_keys.PRICE] == 2045.0
    assert product[product_keys.URL] == 'https://www.castorama.ru/iskusstvennaja-trava-dennis-1-h-4-m'

    product = products[3]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '560789'
    assert product[product_keys.NAME] == 'Искусственная трава Boronia 1 х 4 м'
    assert product[product_keys.PRICE] == 920.0
    assert product[product_keys.URL] == 'https://www.castorama.ru/iskusstvennaja-trava-boronia-1-h-4-m'


def test_parse_products3():
    html_file_path = 'fixtures/stores/castorama/html/products-with-price-per-meter.html'
    reply_url = 'https://www.castorama.ru/tile/wall-tile'
    expected_product_count = 96

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = castorama.parse_products(reply)
    assert len(products) == expected_product_count

    category = 'Керамическая плитка > Настенная плитка'
    product = products[0]
    assert product[product_keys.CATEGORY] == category
    assert product[product_keys.VENDOR_CODE] == '545299'
    assert product[product_keys.NAME] == 'Плитка Калейдоскоп Kerama Marazzi, 20 х 20 см, желтая'
    assert product[product_keys.PRICE] == 870.0
    # assert product[product_keys.PRICE] == 836.54
    assert product[product_keys.URL] == 'https://www.castorama.ru/plitka-kalejdoskop-kerama-marazzi-20-h-20-sm-zheltaja'

    product = products[1]
    assert product[product_keys.CATEGORY] == category
    assert product[product_keys.VENDOR_CODE] == '545300'
    assert product[product_keys.NAME] == 'Плитка Калейдоскоп Kerama Marazzi, 20 х 20 см, серая'
    assert product[product_keys.PRICE] == 479.0
    # assert product[product_keys.PRICE] == 460.58
    assert product[product_keys.URL] == 'https://www.castorama.ru/plitka-kalejdoskop-kerama-marazzi-20-h-20-sm-seraja'

    product = products[5]
    assert product[product_keys.CATEGORY] == category
    assert product[product_keys.VENDOR_CODE] == '569355'
    assert product[product_keys.NAME] == 'Настенная плитка Global Tile Viola, 60 х 25 см, сиреневая'
    assert product[product_keys.PRICE] == 790.0
    # assert product[product_keys.PRICE] == 658.33
    assert product[product_keys.URL] == 'https://www.castorama.ru/nastennaja-plitka-global-tile-viola-60-h-25-sm-sirenevaja'


def parse_page_count(html_file_path: str, reply_url: str) -> int:
    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    return castorama.parse_page_count(reply)
