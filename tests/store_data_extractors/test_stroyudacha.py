import pytest

from shop_parser.parsers.stroyudacha import functions as stroyudacha
import shop_parser.core.product_keys as product_keys
from shop_parser.parsers.stroyudacha import product_keys as stroyudacha_product_keys

import tests.common


@pytest.mark.skip(reason='Cities are hardcoded into "stroyudacha/data_extractor.py"')
def test_parse_cities():
    pass


def test_parse_catalog():
    html_file_path = 'fixtures/stores/stroyudacha/html/main-page.html'
    reply_url = 'https://stroyudacha.ru/'
    expected_tree_depth = 3
    expected_tree_item_count = 673    # TODO: put the actual count
    expected_toplevel_categories = [
        'Общестроительные материалы',
        'Пиломатериалы, изделия из древесины, древесно-плитные материалы',
        'Кровля, отделка фасада, водосточные системы',
        'Теплоизоляция',
        'Сухие строительные смеси и гидроизоляция',
        'Материалы для внутренней отделки',
        'Крепеж',
        'Монтажные пены, клеи, герметики',
        'Лакокрасочные материалы',
        'Инструмент',

        'Спецодежда и средства защиты',
        'Сантехника и отопление',
        'Электротехника',
        'Вентиляция',
        'Газовое оборудование',
        'Двери, окна, скобяные изделия',
        'Бани, бытовки, печное оборудование',
        'Дача, сад и огород',
        'Обустройство дома',
        'Автотовары',
    ]

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    category_tree = stroyudacha.parse_catalog(reply)
    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]

def test_product_urls():
    html_file_path = 'fixtures/stores/stroyudacha/html/page-with-products.html'
    reply_url = 'https://aleksandrovskaya.stroyudacha.ru/groups/161-vodnye-kraski/?page=6'
    expected_product_count = 88

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    product_urls = stroyudacha.parse_product_urls(reply)
    assert len(product_urls) == expected_product_count


def test_parse_product_with_price_on_sale():
    html_file_path = 'fixtures/stores/stroyudacha/html/paint-product-with-price-on-sale.html'
    reply_url = 'https://aleksandrovskaya.stroyudacha.ru/products/98090-kraska-vlagostoykaya-dlya-sten-i-potolkov-aura-interior-fjord-belaya-4-5-l.html'
    expected_category_name = 'Лакокрасочные материалы > Краски, лаки, колеры > Водные краски > Краски влагостойкие'

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    product = stroyudacha.parse_product(reply)
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '12986'
    assert product[product_keys.NAME] == 'Краска влагостойкая для стен и потолков AURA Interior Fjord белая 4.5 л'
    assert product[product_keys.PRICE] == 951.0
    assert product[product_keys.PRICE_ON_SALE] == 760.8
    assert product[product_keys.URL] == reply_url
    assert product[stroyudacha_product_keys.BAR_CODE] == '4607003912499'


def test_parse_product():
    html_file_path = 'fixtures/stores/stroyudacha/html/paint-product.html'
    reply_url = 'https://aleksandrovskaya.stroyudacha.ru/products/197782-kraska-vlagostoykaya-dlya-sten-i-potolkov-tex-universal-belaya-0-9-l-1-5-kg.html'
    expected_category_name = 'Лакокрасочные материалы > Краски, лаки, колеры > Водные краски > Краски влагостойкие'

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    product = stroyudacha.parse_product(reply)
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '12793'
    assert product[product_keys.NAME] == 'Краска влагостойкая для стен и потолков ТЕКС Универсал белая 0.9 л/1.5 кг'
    assert product[product_keys.PRICE] == 180.0
    assert product[product_keys.URL] == reply_url
    assert product[stroyudacha_product_keys.BAR_CODE] == '4601541189023'
