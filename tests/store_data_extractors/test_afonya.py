from shop_parser.parsers.afonya import functions as afonya
import shop_parser.core.product_keys as product_keys

import tests.common


def test_parse_cities():
    html_file_path = 'fixtures/stores/afonya/html/cities.html'
    expected_city_count = 45

    reply = tests.common.get_reply_from_file(html_file_path)
    cities = afonya.parse_cities(reply)
    assert len(cities) == expected_city_count

    city = cities[0]
    assert city.name == 'Санкт-Петербург'
    assert city.domain_name == '0000103664'


def test_parse_catalog():
    html_file_path = 'fixtures/stores/afonya/html/categories.html'
    reply_url = 'https://www.afonya-spb.ru/'
    expected_tree_depth = 1
    expected_tree_item_count = 900

    # 31 categories
    # expected_toplevel_categories = [
    #     '',
    # ]

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    category_tree = afonya.parse_catalog(reply)
    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    # assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    # for i in range(len(expected_toplevel_categories)):
    #     item = category_tree.root_item.children[i]
    #     assert item.name == expected_toplevel_categories[i]


def test_parse_page_count():
    html_file_path = 'fixtures/stores/afonya/html/page_with_products.html'
    reply_url = 'https://www.afonya-spb.ru/catalog/vanny-akrilovye/'
    expected_page_count = 242

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    page_count = afonya.parse_page_count(reply)
    assert page_count == expected_page_count


def test_parse_products():
    html_file_path = 'fixtures/stores/afonya/html/page_with_products.html'
    reply_url = 'https://www.afonya-spb.ru/catalog/vanny-akrilovye/'
    expected_product_count = 10

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = afonya.parse_products(reply)
    assert len(products) == expected_product_count

    product = products[0]
    assert product[product_keys.CATEGORY] == 'Ванны > Акриловые ванны'
    assert product[product_keys.VENDOR_CODE] == '16045'
    assert product[product_keys.NAME] == 'Ванна акриловая Акватек АЛЬФА NEW 170х70 фронтальная панель, каркас, слив'
    assert product[product_keys.PRICE] == 28406.0
    assert product[product_keys.URL] == 'https://www.afonya-spb.ru/Vanna-Akrilovaya-Akvatek-ALFA-NEW-170x70-Frontalnaya-Panel-Karkas-Sliv/'
