from shop_parser.parsers.baucenter import functions as baucenter
import shop_parser.core.product_keys as product_keys

import tests.common


def test_parse_cities():
    html_file_path = 'fixtures/stores/baucenter/html/main_page.html'
    reply_url = 'https://baucenter.ru/'
    expected_city_count = 5

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    cities = baucenter.parse_cities(reply)
    assert len(cities) == expected_city_count

    city = cities[1]
    assert city.name == 'Краснодар'
    assert city.domain_name == '318'


def test_parse_catalog():
    html_file_path = 'fixtures/stores/baucenter/html/citemap.html'
    reply_url = 'https://baucenter.ru/search/map/'
    expected_tree_depth = 2
    expected_tree_item_count = 134
    expected_toplevel_categories = [
        'Стройматериалы',
        'Сад и огород',
        'Кухни, товары для дома',
        'Сантехника',
        'Обои, декор, краски',
        'Плитка',
        'Инструменты, крепеж',
        'Столярные изделия, двери',
        'Напольные покрытия',
        'Водоснабжение и отопление',

        'Электротовары',
        'Освещение',
    ]

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    category_tree = baucenter.parse_catalog_tree(reply)
    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]


def test_parse_downlevel_categories():
    html_file_path = 'fixtures/stores/baucenter/html/category_page.html'
    reply_url = 'https://baucenter.ru/truby_i_fitingi/'
    expected_url_count = 11

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    urls = baucenter.parse_downlevel_categories_urls(reply)
    assert len(urls) == expected_url_count

    assert urls[-1] == 'https://baucenter.ru/fitingi_stalnye_pod_svarku/'


def test_parse_page_count():
    html_file_path = 'fixtures/stores/baucenter/html/page_with_products.html'
    reply_url = 'https://baucenter.ru/polipropilenovye_truby_i_fitingi/?count=36'
    expected_page_count = 8

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    page_count = baucenter.parse_page_count(reply)
    assert page_count == expected_page_count


def test_parse_products():
    html_file_path = 'fixtures/stores/baucenter/html/page_with_products.html'
    reply_url = 'https://baucenter.ru/polipropilenovye_truby_i_fitingi/?count=36'
    expected_product_count = 36

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = baucenter.parse_products(reply)
    assert len(products) == expected_product_count

    product = products[0]
    assert product[product_keys.CATEGORY] == 'Водоснабжение и отопление > Трубы и фитинги > Полипропиленовые трубы и фитинги'
    assert product[product_keys.VENDOR_CODE] == '403005748'
    assert product[product_keys.NAME] == 'Труба полипропиленовая PN 20 d20х2 м стекловолокно белая'
    assert product[product_keys.PRICE] == '103'     # TODO: float?
    assert product[product_keys.URL] == 'https://baucenter.ru/polipropilenovye_truby_i_fitingi/747548/'
