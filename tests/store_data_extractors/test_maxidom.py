from shop_parser.parsers.maxidom import functions as maxidom
import shop_parser.core.product_keys as product_keys

import tests.common


def test_parse_cities():
    html_file_path = 'fixtures/stores/maxidom/html/main_page.html'
    expected_city_count = 8

    reply = tests.common.get_reply_from_file(html_file_path)
    cities = maxidom.parse_cities(reply)
    assert len(cities) == expected_city_count

    city = cities[0]
    assert city.name == 'Санкт-Петербург'
    assert city.domain_name == '2'


def test_parse_catalog():
    html_file_path = 'fixtures/stores/maxidom/html/main_page2.html'
    reply_url = 'https://www.maxidom.ru/'
    expected_tree_depth = 2
    expected_tree_item_count = 347
    expected_toplevel_categories = __get_top_level_categories()

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    category_tree = maxidom.parse_catalog(reply)
    for item in category_tree.root_item.children:
        print(item.name, item.url)

    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]


def test_parse_catalog2():
    html_file_path = 'fixtures/stores/maxidom/html/main_page3.html'
    reply_url = 'https://www.maxidom.ru/'
    expected_tree_depth = 2
    expected_tree_item_count = 346
    expected_toplevel_categories = __get_top_level_categories()

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    category_tree = maxidom.parse_catalog(reply)
    for item in category_tree.root_item.children:
        print(item.name, item.url)

    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]


def test_parse_page_count():
    html_file_path = 'fixtures/stores/maxidom/html/page_with_products.html'
    reply_url = 'https://www.maxidom.ru/catalog/zerkala-dlya-mebeli/'
    expected_page_count = 2

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    page_count = maxidom.parse_page_count(reply)
    assert page_count == expected_page_count


def test_parse_page_count2():
    html_file_path = 'fixtures/stores/maxidom/html/page_with_products2.html'
    reply_url = 'https://www.maxidom.ru/catalog/sifony/'
    expected_page_count = 1

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    page_count = maxidom.parse_page_count(reply)
    assert page_count == expected_page_count


def test_parse_page_count3():
    html_file_path = 'fixtures/stores/maxidom/html/page_with_products3.html'
    reply_url = 'https://www.maxidom.ru/catalog/sifony/'
    expected_page_count = 6

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    page_count = maxidom.parse_page_count(reply)
    assert page_count == expected_page_count


def test_parse_products():
    html_file_path = 'fixtures/stores/maxidom/html/page_with_products.html'
    reply_url = 'https://www.maxidom.ru/catalog/zerkala-dlya-mebeli/'
    expected_product_count = 11

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = maxidom.parse_products(reply)
    assert len(products) == expected_product_count

    expected_category = 'Сантехника > Мебель для ванных комнат > Зеркала для мебели'
    product = products[0]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001270566'
    assert product[product_keys.NAME] == 'зеркало RAVAL Frame 75 см с подсветкой дуб трюфель'
    assert product[product_keys.PRICE] == 13165.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/zerkala-dlya-mebeli/1001270566/'

    product = products[1]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001414859'
    assert product[product_keys.NAME] == 'зеркало для ванной IDDIS Edifice 100х70см внутренняя подсветка белый'
    assert product[product_keys.PRICE] == 15890.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/zerkala-dlya-mebeli/1001414859/'

    product = products[4]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001406646'
    assert product[product_keys.NAME] == 'зеркало для ванной IDDIS Edifice 80х70 см внутренняя подсветка белый'
    assert product[product_keys.PRICE] == 14890.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/zerkala-dlya-mebeli/1001406646/'


def test_parse_products2():
    html_file_path = 'fixtures/stores/maxidom/html/page_with_products2.html'
    reply_url = 'https://www.maxidom.ru/catalog/kultivatory/'
    expected_product_count = 9

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = maxidom.parse_products(reply)
    assert len(products) == expected_product_count

    expected_category = 'Товары для сада и отдыха > Садовая техника > Культиваторы'
    product = products[0]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001174421'
    assert product[product_keys.NAME] == 'мотокультиватор бензиновый Champion ВC 4311'
    assert product[product_keys.PRICE] == 20300.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/kultivatory/1001174421/'

    product = products[1]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001174422'
    assert product[product_keys.NAME] == 'мотокультиватор бензиновый Champion ВC 5712'
    assert product[product_keys.PRICE] == 28900.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/kultivatory/1001174422/'

    product = products[2]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001219715'
    assert product[product_keys.NAME] == 'культиватор электрический Champion EC750'
    assert product[product_keys.PRICE] == 7950.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/kultivatory/1001219715/'

    product = products[6]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001313586'
    assert product[product_keys.NAME] == 'культиватор электрический DAEWOO DAT 2000E'
    assert product[product_keys.PRICE] == 13890.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/kultivatory/1001313586/'

    product = products[7]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1000880821'
    assert product[product_keys.NAME] == 'мотокультиватор бензиновый CHAMPION GC243'
    assert product[product_keys.PRICE] == 16490.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/kultivatory/1000880821/'

    product = products[8]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001387612'
    assert product[product_keys.NAME] == 'мотокультиватор бензиновый HUTER GMC-7.0'
    assert product[product_keys.PRICE] == 29890.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/kultivatory/1001387612/'


def test_parse_products3():
    html_file_path = 'fixtures/stores/maxidom/html/page_with_products3.html'
    reply_url = 'https://www.maxidom.ru/catalog/sifony/'
    expected_product_count = 11

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = maxidom.parse_products(reply)
    assert len(products) == expected_product_count

    expected_category = 'Сантехника > Инженерная сантехника > Сифоны'
    product = products[0]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001334199'
    assert product[product_keys.NAME] == 'сифон универсальный VIRPLAST 1 1/2"х40 белый'
    assert product[product_keys.PRICE] == 190.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/sifony/1001334199/'

    product = products[1]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001334745'
    assert product[product_keys.NAME] == 'сифон для душевого поддона RAVAK Basic 90мм слив 40мм'
    assert product[product_keys.PRICE] == 1380.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/sifony/1001334745/'

    product = products[2]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001334691'
    assert product[product_keys.NAME] == 'сифон для мойки AQUANT 3 1/2"х40 сдвоенный белый'
    assert product[product_keys.PRICE] == 1849.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/sifony/1001334691/'

    product = products[4]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001334689'
    assert product[product_keys.NAME] == 'сифон для мойки AQUANT 115 мм с квадр переливом'
    assert product[product_keys.PRICE] == 985
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/sifony/1001334689/'

    product = products[9]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001328376'
    assert product[product_keys.NAME] == 'сифон для раковины WIRQUIN 1 1/4x32 с выпуском черный'
    assert product[product_keys.PRICE] == 4592.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/sifony/1001328376/'

    product = products[10]
    assert product[product_keys.CATEGORY] == expected_category
    assert product[product_keys.VENDOR_CODE] == '1001328375'
    assert product[product_keys.NAME] == 'сифон для душевого поддона WIRQUIN Slim 90 мм слив 40 мм'
    assert product[product_keys.PRICE] == 2200.0
    assert product[product_keys.URL] == 'https://www.maxidom.ru/catalog/sifony/1001328375/'


def test_parse_subcategory_urls():
    html_file_path = 'fixtures/stores/maxidom/html/page_with_subcategories.html'
    reply_url = 'https://www.maxidom.ru/catalog/mebel-dlya-vannyh-komnat/'
    expected_url_count = 10

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    urls = maxidom.parse_subcategory_urls(reply)
    for url in urls:
        print(url)

    assert len(urls) == expected_url_count


def test_parse_subcategory_urls2():
    html_file_path = 'fixtures/stores/maxidom/html/page_with_subcategories2.html'
    reply_url = 'https://www.maxidom.ru/catalog/inzhenernaya-santehnika/'
    expected_url_count = 28

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    urls = maxidom.parse_subcategory_urls(reply)
    for url in urls:
        print(url)

    assert len(urls) == expected_url_count


def __get_top_level_categories() -> list[str]:
    out = [
        'Товары для сада и отдыха',
        'Строительные материалы',
        'Сантехника',
        'Электроинструмент',
        'Ручной инструмент',
        'Строительное оборудование',
        'Краска и малярный инструмент',
        'Плитка',
        'Напольные покрытия',
        'Интерьер',
        'Электротовары',
        'Скобяные изделия',
        'Освещение',
        'Товары для дома и декора',
        'Красота и здоровье',
        'Мебель',
        'Бытовая техника',
        'Посуда и утварь кухонная',
        'Автотовары',
        'Аудио-Видео',
        'Канцтовары',
        'Игрушки',
        'Прочие товары',
        'Новогодние товары',
        'Кухни',
        'Коллекции кухонь',
    ]

    return out
