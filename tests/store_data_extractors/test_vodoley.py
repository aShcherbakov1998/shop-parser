import pytest

from shop_parser.parsers.vodoley import functions as vodoley
import shop_parser.core.product_keys as product_keys

import tests.common


@pytest.mark.skip(reason='Cities are hardcoded into "vodoley/data_extractor.py"')
def test_parse_cities():
    pass


def test_parse_catalog():
    html_file_path = 'fixtures/stores/vodoley/html/main_page.html'
    reply_url = 'https://водолей.рф/?cityId=0000896058'
    expected_tree_depth = 3
    expected_tree_item_count = 1091
    expected_toplevel_categories = [
        'Сантехника',
        'Плитка',
        'Водоснабжение и водоотведение',
        'Отопление',
        'Строительные и отделочные материалы',
        'Инструмент и инвентарь',
        'Услуги'
    ]

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    category_tree = vodoley.parse_catalog_tree(reply)
    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]


def test_parse_page_count():
    html_file_path = 'fixtures/stores/vodoley/html/page_with_products.html'
    reply_url = 'https://водолей.рф/catalog/santekhnika/smesiteli/smesiteli_dlya_vanny/?cityId=0000896058&setPage=48'
    expected_page_count = 14

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    page_count = vodoley.parse_page_count(reply)
    assert page_count == expected_page_count


def test_parse_products():
    html_file_path = 'fixtures/stores/vodoley/html/page_with_products.html'
    reply_url = 'https://водолей.рф/catalog/santekhnika/smesiteli/smesiteli_dlya_vanny/?cityId=0000896058&setPage=48'
    expected_product_count = 48

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = vodoley.parse_products(reply)
    assert len(products) == expected_product_count

    product = products[-1]
    assert product[product_keys.CATEGORY] == 'Сантехника > Смесители > Смесители для ванны'
    assert product[product_keys.VENDOR_CODE] == '205312'
    assert product[product_keys.NAME] == 'Смеситель для ванны Teka SALINA  271216200  карт35 низкий излив без душ.набора'
    assert product[product_keys.PRICE] == 5500.0
    assert product[product_keys.URL] == 'https://водолей.рф/catalog/205312/'
