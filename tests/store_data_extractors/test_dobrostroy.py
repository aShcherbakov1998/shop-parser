from shop_parser.parsers.dobrostroy import functions as dobrostroy
import shop_parser.core.product_keys as product_keys

import tests.common


def test_parse_cities():
    html_file_path = 'fixtures/stores/dobrostroy/html/catalog-page.html'
    reply_url = 'https://добрострой.рф/sitemap/'
    expected_city_count = 5

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    cities = dobrostroy.parse_cities(reply)
    assert len(cities) == expected_city_count

    city = cities[0]
    assert city.name == 'Астрахань'
    assert city.domain_name == '77385'

    city = cities[1]
    assert city.name == 'Волжский'
    assert city.domain_name == '154188'

    city = cities[2]
    assert city.name == 'Липецк'
    assert city.domain_name == '77386'


def test_parse_catalog():
    html_file_path = 'fixtures/stores/dobrostroy/html/catalog-page.html'
    reply_url = 'https://добрострой.рф/sitemap/'
    expected_tree_depth = 2
    expected_tree_item_count = 145
    expected_toplevel_categories = [
        'Стройматериалы',
        'Инструмент и крепеж',
        'Лакокрасочные материалы',
        'Обои и панели',
        'Напольные покрытия',
        'Керамическая плитка, керамогранит',
        'Водогазоснабжение и канализация',
        'Отопление, охлаждение и вентиляция',
        'Электрика',
        'Свет',

        'Двери, окна и замочно-скобяные изделия',
        'Ванная комната',
        'Кухня',
        'Дача и баня',
        'Декор и солнцезащита',
        'Новый год',
        'Хранение и хозтовары',
        'Подарочные карты',
    ]

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    category_tree = dobrostroy.parse_catalog(reply)
    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]


def test_catalog_with_only_one_page():
    html_file_path = 'fixtures/stores/dobrostroy/html/products-with-only-one-page.html'
    reply_url = 'https://орел.добрострой.рф/catalog/stroymaterialy/zabory/setka-rabitsa/'
    expected_page_count = 1

    page_count = parse_page_count(html_file_path, reply_url)
    assert page_count == expected_page_count


def test_catalog_with_many_pages():
    html_file_path = 'fixtures/stores/dobrostroy/html/products-with-many-pages.html'
    reply_url = 'https://орел.добрострой.рф/catalog/oboi-i-paneli/oboi/'
    expected_page_count = 49

    page_count = parse_page_count(html_file_path, reply_url)
    assert page_count == expected_page_count


def test_catalog_with_many_pages2():
    html_file_path = 'fixtures/stores/dobrostroy/html/products-with-many-pages2.html'
    reply_url = 'https://орел.добрострой.рф/catalog/kukhnya/moyki-kukhonnye/'
    expected_page_count = 3

    page_count = parse_page_count(html_file_path, reply_url)
    assert page_count == expected_page_count


def test_page_without_products():
    html_file_path = 'fixtures/stores/dobrostroy/html/page-without-products.html'
    reply_url = 'https://орел.добрострой.рф/catalog/stroymaterialy/izolyatsionnye-materialy/porolon/'
    expected_page_count = 1

    page_count = parse_page_count(html_file_path, reply_url)
    assert page_count == expected_page_count


def test_page_without_products2():
    html_file_path = 'fixtures/stores/dobrostroy/html/page-without-products2.html'
    reply_url = 'https://орел.добрострой.рф/catalog/kukhnya/tekhnika-dlya-kukhni/'
    expected_page_count = 1

    page_count = parse_page_count(html_file_path, reply_url)
    assert page_count == expected_page_count


def test_parse_products():
    html_file_path = 'fixtures/stores/dobrostroy/html/products-with-many-pages.html'
    reply_url = 'https://орел.добрострой.рф/catalog/oboi-i-paneli/oboi/'
    expected_product_count = 20
    expected_category_name = 'Обои и панели > Обои'

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = dobrostroy.parse_products(reply)
    assert len(products) == expected_product_count

    product = products[0]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '3120254'
    assert product[product_keys.NAME] == 'OSTIMA Sultan Обои горячего тиснения на флиз. основе 1,06*10м'
    assert product[product_keys.PRICE] == 2050.0
    assert product[product_keys.URL] == 'https://орел.добрострой.рф/products/88451-ostima-sultan-oboi-goryachego-tisneniya-na-fliz-osnove-1-06-10m/'

    product = products[2]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '3121681'
    assert product[product_keys.NAME] == 'OSTIMA Sultan Обои горячего тиснения на флиз. основе 1,06*10м'
    assert product[product_keys.PRICE] == 2150.0
    assert product[product_keys.URL] == 'https://орел.добрострой.рф/products/88458-ostima-sultan-oboi-goryachego-tisneniya-na-fliz-osnove-1-06-10m/'


def test_parse_products2():
    html_file_path = 'fixtures/stores/dobrostroy/html/products-with-only-one-page.html'
    reply_url = 'https://орел.добрострой.рф/catalog/stroymaterialy/zabory/setka-rabitsa/'
    expected_product_count = 6
    expected_category_name = 'Стройматериалы > Заборы > Сетка рабица'

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = dobrostroy.parse_products(reply)
    assert len(products) == expected_product_count

    product = products[0]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '0938547'
    assert product[product_keys.NAME] == 'Сетка рабица 1,5х10 м, ячейка 35х35х1,6 мм'
    assert product[product_keys.PRICE] == 1615.0
    assert product[product_keys.URL] == 'https://орел.добрострой.рф/products/setka-rabitsa-1-5kh10-m-yacheyka-35kh35kh1-6-mm/'

    product = products[3]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '0078493'
    assert product[product_keys.NAME] == 'Сетка рабица в ПВХ 1,5х10м ячейка 55х55х2,5мм'
    assert product[product_keys.PRICE] == 1745.0
    assert product[product_keys.URL] == 'https://орел.добрострой.рф/products/setka-rabitsa-v-pvkh-1-5kh10m-yacheyka-55kh55kh2-5mm/'

    product = products[5]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '0070427'
    assert product[product_keys.NAME] == 'Сетка рабица оцинкованная 1.6х60*60 мм, 1.5*10 м'
    assert product[product_keys.PRICE] == 1110.0
    assert product[product_keys.URL] == 'https://орел.добрострой.рф/products/setka-rabitsa-otsinkovannaya-1-6kh60-60-mm-1-5-10-m/'


def test_parse_products3():
    html_file_path = 'fixtures/stores/dobrostroy/html/products-with-many-pages3.html'
    reply_url = 'https://волжский.добрострой.рф/catalog/oboi-i-paneli/oboi/?PAGEN_1=48'
    expected_product_count = 20
    expected_category_name = 'Обои и панели > Обои'

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = dobrostroy.parse_products(reply)
    assert len(products) == expected_product_count

    for product in products:
        assert product[product_keys.CATEGORY] == expected_category_name

    product = products[-1]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '3112138'
    assert product[product_keys.NAME] == 'Обои Палитра Edem Винил 1,06*10м темно-серый мотив арт. PL71654-42 (Россия)'
    assert product[product_keys.PRICE] == 2160.0
    assert product[product_keys.URL] == 'https://волжский.добрострой.рф/products/oboi-palitra-edem-vinil-1-06-10m-temno-seryy-motiv-art-pl71654-42-rossiya/'

    product = products[-2]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '3112141'
    assert product[product_keys.NAME] == 'Обои Палитра Edem Винил 1,06*10м серый фон арт. PL71655-42 (Россия)'
    assert product[product_keys.PRICE] == 2035.0
    assert product[product_keys.URL] == 'https://волжский.добрострой.рф/products/oboi-palitra-edem-vinil-1-06-10m-seryy-fon-art-pl71655-42-rossiya/'

    product = products[-3]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '3112140'
    assert product[product_keys.NAME] == 'Обои Палитра Edem Винил 1,06*10м бежевый фон арт. PL71655-22 (Россия)'
    assert product[product_keys.PRICE] == 2035.0
    assert product[product_keys.URL] == 'https://волжский.добрострой.рф/products/oboi-palitra-edem-vinil-1-06-10m-bezhevyy-fon-art-pl71655-22-rossiya/'

    product = products[-4]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '3112137'
    assert product[product_keys.NAME] == 'Обои Палитра Edem Винил 1,06*10м бежевый мотив арт. PL71654-22 (Россия)'
    assert product[product_keys.PRICE] == 2160.0
    assert product[product_keys.URL] == 'https://волжский.добрострой.рф/products/oboi-palitra-edem-vinil-1-06-10m-bezhevyy-motiv-art-pl71654-22-rossiya/'

    product = products[-5]
    assert product[product_keys.CATEGORY] == expected_category_name
    assert product[product_keys.VENDOR_CODE] == '3126288'
    assert product[product_keys.NAME] == 'Обои Палитра Diamond Винил 1,06*10м серый фон арт. HC71782-14 (Россия)'
    assert product[product_keys.PRICE] == 1950.0
    assert product[product_keys.URL] == 'https://волжский.добрострой.рф/products/oboi-palitra-diamond-vinil-1-06-10m-seryy-fon-art-hc71782-14-rossiya/'


def parse_page_count(html_file_path: str, reply_url: str) -> int:
    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    return dobrostroy.parse_page_count(reply)
