import pytest

from shop_parser.parsers.obi import functions as obi
import shop_parser.core.product_keys as product_keys

import tests.common


@pytest.mark.skip(reason='Cities are hardcoded into "obi/data_extractor.py"')
def test_parse_cities():
    pass


def test_parse_catalog():
    html_file_path = 'fixtures/stores/obi/html/page_with_catalog.html'
    reply_url = 'https://www.obi.ru/sitemap/'
    expected_tree_depth = 2
    expected_tree_item_count = 91
    expected_toplevel_categories = [
        'Стройка',
        'Сад и досуг',
        'Техника',
        'Всё для дома',
        'Кухня',
        'Ванная комната',
    ]

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    category_tree = obi.parse_catalog_tree(reply)
    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]


def test_parse_downlevel_categories():
    html_file_path_key = 'html_file_path'
    expected_url_count_key = 'expected_url_count'
    reply_url = 'https://www.obi.ru/sad-i-dosug/sadovaya-tekhnika/c/862'

    data_list = [
        {
            html_file_path_key: 'fixtures/stores/obi/html/page_to_parse_downlevel_categories_1.html',
            expected_url_count_key: 6,
        },
        {
            html_file_path_key: 'fixtures/stores/obi/html/page_to_parse_downlevel_categories_2.html',
            expected_url_count_key: 14,
        },
    ]

    for data in data_list:
        html_file_path = data[html_file_path_key]
        expected_url_count = data[expected_url_count_key]

        reply = tests.common.get_reply_from_file(html_file_path, reply_url)
        category_urls = obi.parse_downlevel_categories(reply)
        assert len(category_urls) == expected_url_count


def test_parse_page_count():
    html_file_path = 'fixtures/stores/obi/html/page_with_many_products.html'
    reply_url = 'https://www.obi.ru/stroika/plitka/c/308'
    expected_page_count = 20

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    page_count = obi.parse_page_count(reply)
    assert page_count == expected_page_count


def test_parse_products():
    html_file_path = 'fixtures/stores/obi/html/page_with_many_products.html'
    reply_url = 'https://www.obi.ru/stroika/plitka/c/308'
    expected_product_count = 72

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = obi.parse_products(reply)
    assert len(products) == expected_product_count

    product = products[0]
    assert product[product_keys.CATEGORY] == 'Стройка > Плитка'
    assert product[product_keys.VENDOR_CODE] == '3101813'
    assert product[product_keys.NAME] == 'Керамогранит Cersanit Gres A100 бежевый 30х30 см'
    assert product[product_keys.PRICE] == 349.0
    assert product[product_keys.URL] == 'https://www.obi.ru/keramogranit/keramogranit-cersanit-gres-a100-bezhevyi-30kh30-sm/p/3101813'
