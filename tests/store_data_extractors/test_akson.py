import pytest

import shop_parser.core.items
from shop_parser.parsers.akson import functions as akson
import shop_parser.core.product_keys as product_keys

import tests.common


@pytest.mark.skip(reason='Cities are hardcoded into "akson/data_extractor.py"')
def test_parse_cities():
    pass


@pytest.mark.skip(reason='Page count is parsed differently')
def test_parse_page_count():
    pass


def test_parse_catalog():
    html_file_path = 'fixtures/stores/akson/html/categories.html'
    reply_url = 'https://akson.ru//'
    expected_tree_depth = 3
    expected_tree_item_count = 48
    expected_toplevel_categories = [
        'Строительные материалы',
    ]

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    category_tree_item = akson.parse_category_item(reply.text, reply.url)

    category_tree = shop_parser.core.items.CategoryTree()
    category_tree.root_item.children = [category_tree_item]
    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]


def test_parse_products_json():
    json_file_path = 'fixtures/stores/akson/json/products.json'
    reply_url = 'https://api1.akson.ru:8443/v8/catalog/section_products/fitingi_trubnye_rezbovye/33/0/?limit=100&skip=200'
    expected_product_count = 100

    reply = tests.common.get_reply_from_file(json_file_path, reply_url)
    products = akson.parse_products_json(reply)
    assert len(products) == expected_product_count

    product = products[0]
    assert product[product_keys.CATEGORY] == 'Водоснабжение, отопление > Санфурнитура > Фитинги трубные резьбовые'
    assert product[product_keys.VENDOR_CODE] == '529626'
    assert product[product_keys.NAME] == 'Заглушка на трубу 1/2" в серия LIGHT, MP-У'
    assert product[product_keys.PRICE] == 70
    assert product[product_keys.URL] == 'https://akson.ru/p/zaglushka_na_trubu_1_2_fortum_shk/'


def test_parse_products_html():
    json_file_path = 'fixtures/stores/akson/html/products_page.html'
    reply_url = 'https://akson.ru/c/sverla_bury/'
    expected_product_count = 20

    reply = tests.common.get_reply_from_file(json_file_path, reply_url)
    products = akson.parse_products_html(reply)
    assert len(products) == expected_product_count

    product = products[0]
    assert product[product_keys.CATEGORY] == 'Инструменты > Расходные материалы > Сверла и буры'
    assert product[product_keys.VENDOR_CODE] == '186927'
    assert product[product_keys.NAME] == 'Бур по бетону SDS+ 6,0*160мм Hagwert 562006'
    assert product[product_keys.PRICE] == '81'
    assert product[product_keys.URL] == 'https://akson.ru/p/bur_po_betonu_sds_6_0_160mm_hagwert_562006/'


def test_parse_product_count():
    html_file_path = 'fixtures/stores/akson/json/products.json'
    reply_url = 'https://api1.akson.ru:8443/v8/catalog/section_products/fitingi_trubnye_rezbovye/33/0/?limit=100&skip=200'
    expected_product_count = 208

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products_count = akson.parse_product_count_json(reply)
    assert products_count == expected_product_count


def test_parse_section_id():
    html_file_path = 'fixtures/stores/akson/html/products_page.html'
    reply_url = 'https://akson.ru/c/sverla_bury/'
    reply = tests.common.get_reply_from_file(html_file_path, reply_url)

    section = akson.parse_section_id(reply)

    assert section == '582'


def test_parse_subcategories():
    html_file_path = 'fixtures/stores/akson/html/page_with_subcategories.html'
    reply_url = 'https://akson.ru/c/dreli/'
    reply = tests.common.get_reply_from_file(html_file_path, reply_url)

    subcategories = akson.parse_subcategories(reply)
    sub = ['https://akson.ru/c/dreli_akkumulyatornye/',
           'https://akson.ru/c/shurupoverty_setevye/',
           'https://akson.ru/c/dreli_udarnye/',
           'https://akson.ru/c/dreli_miksery/',
           'https://akson.ru/c/dreli_bezudarnye/']
    assert subcategories == sub
