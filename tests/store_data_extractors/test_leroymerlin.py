from shop_parser.parsers.leroymerlin import functions as leroymerlin
import shop_parser.core.product_keys as product_keys

import tests.common


def test_parse_cities():
    html_file_path = 'fixtures/stores/leroymerlin/html/main_page.html'
    expected_city_count = 56

    reply = tests.common.get_reply_from_file(html_file_path)
    cities = leroymerlin.parse_cities(reply)
    assert len(cities) == expected_city_count

    city = cities[1]
    assert city.name == 'Санкт-Петербург'
    assert city.domain_name == 'https://spb.leroymerlin.ru/'


def test_create_json_link_from_city_url():
    url_key = 'url'
    json_link_key = 'json_link'
    city_link_list = [
        {
            url_key: 'https://spb.leroymerlin.ru/',
            json_link_key: 'https://spb.leroymerlin.ru/content/elbrus/spb/ru/displayedCatalogue.json',
        },
        {
            url_key: 'https://barnaul.leroymerlin.ru/',
            json_link_key: 'https://barnaul.leroymerlin.ru/content/elbrus/barnaul/ru/displayedCatalogue.json',
        },
        {
            url_key: 'https://leroymerlin.ru/',
            json_link_key: 'https://leroymerlin.ru/content/elbrus/moscow/ru/displayedCatalogue.json',
        },
    ]

    for city_link in city_link_list:
        url = city_link[url_key]
        expected_json_link = city_link[json_link_key]
        json_link = leroymerlin.create_json_link_from_city_url(url)

        assert json_link == expected_json_link


def test_parse_json_link():
    html_file_path = 'fixtures/stores/leroymerlin/html/main_page.html'
    expected_json_link = 'http://www.example.com/content/elbrus/moscow/ru/displayedCatalogue.json'

    reply = tests.common.get_reply_from_file(html_file_path)
    json_link = leroymerlin.parse_json_link(reply)
    assert json_link == expected_json_link


def test_parse_catalog():
    html_file_path = 'fixtures/stores/leroymerlin/json/displayedCatalogue.json'
    expected_tree_depth = 2
    expected_tree_item_count = 7866
    expected_toplevel_categories = [
        'Водоснабжение',
        'Электротовары',
        'Стройматериалы',
        'Краски',
        'Скобяные изделия',
        'Столярные изделия',
        'Окна и двери',
        'Инструменты',
        'Хранение',
        'Плитка',

        'Сантехника',
        'Напольные покрытия',
        'Декор',
        'Освещение',
        'Кухни',
        'Сад',
    ]

    reply = tests.common.get_reply_from_file(html_file_path)
    category_tree = leroymerlin.parse_catalog(reply)
    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]


def test_parse_page_count():
    html_file_path = 'fixtures/stores/leroymerlin/html/page_with_products.html'
    expected_page_count = 8

    reply = tests.common.get_reply_from_file(html_file_path)
    page_count = leroymerlin.parse_page_count(reply)
    assert page_count == expected_page_count


def test_parse_products():
    html_file_path = 'fixtures/stores/leroymerlin/html/page_with_products.html'
    expected_product_count = 30

    reply = tests.common.get_reply_from_file(html_file_path)
    products = leroymerlin.parse_products(reply)
    assert len(products) == expected_product_count

    product = products[0]
    assert product[product_keys.CATEGORY] == 'Водоснабжение > Радиаторы отопления'
    assert product[product_keys.VENDOR_CODE] == '89106830'
    assert product[product_keys.NAME] == 'Радиатор Equation 500/90, 10 секций, двухсторонний, биметалл'
    assert product[product_keys.PRICE] == 9200
    assert product[product_keys.URL] == 'https://leroymerlin.ru/product/radiator-equation-500-90-89106830/'
