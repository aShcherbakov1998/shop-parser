import pytest

from shop_parser.parsers.akvalink import functions as akvalink
import shop_parser.core.product_keys as product_keys

import tests.common


def test_parse_catalog():
    html_file_path = 'fixtures/stores/akvalink/html/main_page.html'
    reply_url = 'https://akvalink.ru/'
    expected_tree_depth = 3
    expected_tree_item_count = 395
    expected_toplevel_categories = [
        'Инженерная сантехника',
        'Насосы',
        'Отопление',
        'Сантехника',
        'Смесители',
        'ЭКОдом'
    ]

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    category_tree = akvalink.parse_catalog_tree(reply)

    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]


def test_parse_page_count():
    html_file_path = 'fixtures/stores/akvalink/html/page_with_products.html'
    reply_url = 'https://akvalink.ru/catalog/otoplenie/radiatory/komplektuyushchie-i-prinadlezhnosti-radiatorov/'
    expected_page_count = 2

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    page_count = akvalink.parse_page_count(reply)
    assert page_count == expected_page_count


def test_parse_products():
    html_file_path = 'fixtures/stores/akvalink/html/page_with_products.html'
    reply_url = 'https://akvalink.ru/catalog/otoplenie/radiatory/komplektuyushchie-i-prinadlezhnosti-radiatorov/'
    expected_product_count = 44
    expected_url_count = 15

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products, urls = akvalink.parse_products(reply)
    assert len(products) == expected_product_count
    assert len(urls) == expected_url_count
    product = products[-1]
    print(product)
    assert product[product_keys.CATEGORY] == 'Отопление>Радиаторы и конвекторы>Комплектующие и принадлежности для ' \
                                             'радиаторов '
    assert product[product_keys.VENDOR_CODE] == 'SVS-0013-000015'
    assert product[product_keys.NAME] == 'Воздухоотводчик STOUT автоматический угловой'
    assert product[product_keys.PRICE] == 458.0
    assert product[product_keys.URL] == 'https://akvalink.ru/catalog/otoplenie/radiatory/komplektuyushchie-i' \
                                        '-prinadlezhnosti-radiatorov/vozdukhootvodchiki' \
                                        '/vozdukhootvodchik_stout_avtomaticheskiy_uglovoy/ '


def test_parse_product():
    html_file_path = 'fixtures/stores/akvalink/html/product_page.html'
    reply_url = 'https://akvalink.ru/catalog/otoplenie/radiatory/komplektuyushchie-i-prinadlezhnosti-radiatorov/'

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    product_offers = akvalink.parse_product(reply)
    assert len(product_offers) == 4
    assert product_offers[0]['Артикул'] == '00014648_508339'

test_parse_product()