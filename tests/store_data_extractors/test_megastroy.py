import pytest

from shop_parser.parsers.megastroy import functions as megastroy
import shop_parser.core.product_keys as product_keys

import tests.common


@pytest.mark.skip(reason='Cities are hardcoded into "megastroy/data_extractor.py"')
def test_parse_cities():
    pass


def test_catalog_with_many_pages():
    html_file_path = 'fixtures/stores/megastroy/html/product-list-with-many-pages.html'
    expected_page_count = 44

    page_count = parse_page_count(html_file_path)
    assert page_count == expected_page_count


def test_catalog_with_only_one_page():
    html_file_path = 'fixtures/stores/megastroy/html/product-list-with-only-one-page.html'
    expected_page_count = 1

    page_count = parse_page_count(html_file_path)
    assert page_count == expected_page_count


def test_product_urls():
    html_file_path = 'fixtures/stores/megastroy/html/product-list-with-many-pages2.html'
    reply_url = "https://www.pytest.com"
    expected_urls = [
        '/products/238172',
        '/products/295356',
        '/products/255383',
        '/products/238198',

        '/products/238083',
        '/products/238178',
        '/products/238234',
        '/products/238144',

        '/products/280671',
        '/products/295347',
        '/products/238226',
        '/products/238233',

        '/products/238051',
        '/products/238105',
        '/products/255410',
        '/products/282367',

        '/products/282518',
        '/products/282528',
        '/products/282531',
        '/products/282536',

        '/products/282538',
        '/products/282540',
        '/products/282559',
        '/products/282572'
    ]

    for index, expected_url in enumerate(expected_urls):
        expected_urls[index] = reply_url + expected_url

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    urls = megastroy.parse_categories_from_category_page(reply)

    assert len(urls) == len(expected_urls)
    for url in urls:
        assert tests.common.has_string(expected_urls, url)

    for expected_url in expected_urls:
        assert (tests.common.has_string(urls, expected_url))


def test_parse_catalog():
    html_file_path = 'fixtures/stores/megastroy/html/top-level.html'
    expected_tree_depth = 3
    expected_tree_item_count = 1035
    expected_toplevel_categories = [
        'Стройматериалы',
        'Отопление, водоснабжение и климат',
        'Инструменты и крепеж',
        'Электротовары',
        'Автотовары',
        'Системы безопасности',
        'Спецодежда и средства индивидуальной защиты',
        'Сантехника',
        'Плитка',
        'Двери, окна, скобяные изделия',

        'Кухни и мебель',
        'Освещение',
        'Напольные покрытия',
        'Обои и декор потолка',
        'Текстиль и декор для дома',
        'Лакокрасочные материалы',
        'Сад и дача',
        'Баня и сауна',
        'Спорт и отдых',
        'Товары для дома',
    ]

    reply = tests.common.get_reply_from_file(html_file_path)
    category_tree = megastroy.parse_category_tree(reply)
    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]


def test_parse_products():
    html_file_path = 'fixtures/stores/megastroy/html/product-list-with-many-pages.html'
    reply_url = 'https://kazan.megastroy.com'
    expected_product_count = 24

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = megastroy.parse_products_from_catalog(reply)
    assert len(products) == expected_product_count

    product = products[0]
    assert product[product_keys.CATEGORY] == 'Сад и дача > Садовые растения и семена'
    assert product[product_keys.VENDOR_CODE] == '238051'
    assert product[product_keys.NAME] == 'Семена Аквилегия Желтый кристалл гибридная'
    assert product[product_keys.PRICE] == 23.0
    assert product[product_keys.URL] == f'{reply_url}/products/238051'


def test_parse_predownlevel_category_page():
    html_file_path = 'fixtures/stores/megastroy/html/pre-downlevel-category-page.html'
    reply_url = 'https://kazan.megastroy.com/catalog/komplektuyuschie-dlya-radiatorov'
    expected_url_count = 8

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    urls = megastroy.parse_categories_from_category_page(reply)
    assert len(urls) == expected_url_count


def parse_page_count(html_file_path: str) -> int:
    reply = tests.common.get_reply_from_file(html_file_path)
    return megastroy.parse_page_count(reply)
