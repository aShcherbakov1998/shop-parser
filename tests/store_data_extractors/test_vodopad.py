import pytest

from shop_parser.parsers.vodopad import functions as vodopad
import shop_parser.core.product_keys as product_keys

import tests.common


@pytest.mark.skip(reason='Cities are hardcoded into "vodopad/data_extractor.py"')
def test_parse_cities():
    pass


def test_parse_catalog():
    html_file_path = 'fixtures/stores/vodopad/html/sitemap.html'
    reply_url = 'https://vodopad.ru/sitemap/'
    expected_tree_depth = 3
    expected_tree_item_count = 334
    expected_toplevel_categories = [
        'Сантехника',
        'Отопление',
        'Водонагреватели',
        'Насосы',
        'Инженерная сантехника',
        'Метизы, электрика',
    ]

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    category_tree = vodopad.parse_catalog(reply)
    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]


def test_parse_page_count():
    html_file_path = 'fixtures/stores/vodopad/html/page_with_products.html'
    expected_page_count = 60

    reply = tests.common.get_reply_from_file(html_file_path)
    page_count = vodopad.parse_page_count(reply)
    assert page_count == expected_page_count


def test_parse_products_list():
    html_file_path = 'fixtures/stores/vodopad/html/page_with_products.html'
    reply_url = 'https://vodopad.ru/catalog/126030/?VIEW=LINE'
    expected_product_count = 32

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = vodopad.parse_products(reply)
    assert len(products) == expected_product_count

    product = products[0]
    assert product[product_keys.CATEGORY] == 'Инженерная сантехника > Трубы и фитинги > Трубы и фитинги к ним > ' \
                                             'Полипропиленовые трубы и фитинги к ним > Трубы и фитинги белые > Трубы ' \
                                             'и фитинги SAV белые'
    assert product[product_keys.VENDOR_CODE] == '47 216 441'
    assert product[product_keys.NAME] == 'Тройник SAV полипропилен 20мм 25бар, t-95*C, сварка'
    assert product[product_keys.PRICE] == 14.0
    assert product[product_keys.URL] == 'https://vodopad.ru/catalog/126030/188878/'


def test_parse_products_tables():
    html_file_path = 'fixtures/stores/vodopad/html/page_with_products_tables.html'
    reply_url = 'https://vodopad.ru/catalog/125938/'
    expected_product_count = 32

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    products = vodopad.parse_products(reply)
    assert len(products) == expected_product_count
    product = products[0]
    assert product[product_keys.CATEGORY] == 'Инженерная сантехника > Трубы и фитинги'
    assert product[product_keys.VENDOR_CODE] == '40 123 587'
    assert product[
               product_keys.NAME] == 'Водорозетка на планке EKOPLASTIK полипропилен 20х1/2" ВР с металлической резьбой'
    assert product[product_keys.PRICE] == 623.0
    assert product[product_keys.URL] == 'https://vodopad.ru/catalog/125938/188886/'
