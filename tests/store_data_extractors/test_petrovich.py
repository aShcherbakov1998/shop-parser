from shop_parser.parsers.petrovich import functions as petrovich
import shop_parser.core.product_keys as product_keys

import tests.common


def test_parse_cities():
    html_file_path = 'fixtures/stores/petrovich/html/main_page.html'
    reply_url = 'https://petrovich.ru/'
    expected_city_count = 13

    reply = tests.common.get_reply_from_file(html_file_path, reply_url)
    cities = petrovich.parse_cities(reply)
    assert len(cities) == expected_city_count

    city = cities[1]
    assert city.name == 'Москва'
    assert city.domain_name == 'https://moscow.petrovich.ru/'


def test_parse_catalog():
    html = 'fixtures/stores/petrovich/html/main_page.html'
    reply_url = 'https://petrovich.ru/'
    expected_tree_depth = 3
    expected_tree_item_count = 499
    expected_toplevel_categories = [
        'Стройматериалы',
        'Инструмент',
        'Электрика',
        'Инженерные системы',
        'Интерьер и отделка',
        'Сантехника',
        'Крепеж',
        'Благоустройство'
    ]

    reply = tests.common.get_reply_from_file(html, reply_url)
    category_tree = petrovich.parse_catalog(reply)
    assert len(category_tree.get_downlevel_items()) == expected_tree_item_count
    assert tests.common.get_tree_depth(category_tree) == expected_tree_depth

    assert len(category_tree.root_item.children) == len(expected_toplevel_categories)
    for i in range(len(expected_toplevel_categories)):
        item = category_tree.root_item.children[i]
        assert item.name == expected_toplevel_categories[i]


def test_parse_page_count():
    html = 'fixtures/stores/petrovich/html/page_with_many_products.html'
    reply_url = 'https://petrovich.ru/catalog/245805260/'
    expected_page_count = 7

    reply = tests.common.get_reply_from_file(html, reply_url)
    page_count = petrovich.parse_page_count(reply)
    assert page_count == expected_page_count


def test_parse_products():
    html = 'fixtures/stores/petrovich/html/page_with_many_products.html'
    reply_url = 'https://petrovich.ru/catalog/245805260/'
    expected_product_count = 20

    reply = tests.common.get_reply_from_file(html, reply_url)
    products = petrovich.parse_products(reply)
    assert len(products) == expected_product_count

    product = products[0]
    assert product[product_keys.CATEGORY] == 'Инструмент > Электроинструмент'
    assert product[product_keys.VENDOR_CODE] == '504595'
    assert product[product_keys.NAME] == 'Шуруповерт аккумуляторный ударный Einhell TE-CI 18 18В Li-Ion без АКБ и ЗУ'
    assert product[product_keys.PRICE] == 5390.0
    assert product[product_keys.URL] == 'https://petrovich.ru/catalog/12809/504595/'
