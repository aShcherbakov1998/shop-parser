import shop_parser.core.data
import shop_parser.ui.core


def make_category_from_string(s: str) -> shop_parser.core.data.Category:
    out = shop_parser.core.data.Category()
    out.name = s

    return out


def make_categories_from_string(s: str) -> list[shop_parser.core.data.Category]:
    return [make_category_from_string(s)]


def make_categories_from_strings(strings: list[str]) -> list[shop_parser.core.data.Category]:
    out = []
    for s in strings:
        out.append(make_category_from_string(s))

    return out


def test_merge_into_unique_lists():
    city1_category_names = ['category1', 'category2', 'category3']
    city1_subcategory_names = ['subcategory1_1', 'subcategory1_2', 'subcategory1_3']

    city2_category_names = ['category4', 'category5', 'category6']
    city2_subcategory_names = ['subcategory5_1', 'subcategory5_2', 'subcategory5_3']

    # City 1
    city1_categories = make_categories_from_strings(city1_category_names)
    city1_categories[0].children = make_categories_from_strings(city1_subcategory_names)
    city1_categories[1].children = make_categories_from_string('subcategory2_1')
    city1_categories[2].children = make_categories_from_string('subcategory3_1')

    city1 = shop_parser.core.data.City()
    city1.name = 'city1'
    city1.categories = city1_categories

    # City 2
    city2_categories = make_categories_from_strings(city2_category_names)
    city2_categories[0].children = make_categories_from_string('subcategory4_1')
    city2_categories[1].children = make_categories_from_strings(city2_subcategory_names)
    city2_categories[2].children = make_categories_from_string('subcategory6_1')

    city2 = shop_parser.core.data.City()
    city2.name = 'city2'
    city2.categories = city2_categories

    # City 3
    city3_categories = make_categories_from_strings(['category1', 'category5', 'category7'])
    city3_categories[0].children = make_categories_from_strings(city1_subcategory_names)
    city3_categories[1].children = make_categories_from_strings(city2_subcategory_names)
    city3_categories[1].children[0].children = make_categories_from_string('subcategory5_1_1')
    city3_categories[2].children = make_categories_from_string('subcategory7_1')

    city3 = shop_parser.core.data.City()
    city3.name = 'city3'
    city3.categories = city3_categories

    # Shop
    shop = shop_parser.core.data.Shop()
    shop.name = 'shop1'
    shop.cities = [city1, city2, city3]

    # Call the function and call some asserts
    cities, category_tree = shop_parser.ui.core.merge_into_unique_lists(shop)
    # for category_node in category_tree:
    #     for pre, fill, node in RenderTree(category_node):
    #         print('%s%s' % (pre, node.name))

    assert(len(cities) == 3)
    assert(len(category_tree) == 7)

    big_subcategory_indexes = [0, 4]
    for i in range(len(category_tree)):
        node = category_tree[i]
        category_index = i + 1
        assert(node.name == f'category{category_index}')
        if i in big_subcategory_indexes:
            continue

        assert(len(node.children) == 1)
        assert(node.children[0].name == f'subcategory{category_index}_1')

    big_subcategory_count = 3
    for i in big_subcategory_indexes:
        node = category_tree[i]
        children = node.children
        assert(len(children) == big_subcategory_count)
        for j in range(len(children)):
            child_node = children[j]
            assert(child_node.name == f'subcategory{i + 1}_{j + 1}')

    assert(category_tree[4].children[0].children[0].name == 'subcategory5_1_1')
