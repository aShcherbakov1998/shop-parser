import pytest
import logging.config

from PySide6.QtCore import QCoreApplication

from shop_parser.core import Processor
from shop_parser import parsers

import tests.common


@pytest.mark.skip(reason='It takes too long to execute (more than a minute)')
def test_store_data_extractor():
    init_logging()

    app = QCoreApplication()
    data_extractor = parsers.akson.AksonShopInfoDataExtractor()
    processor = Processor()
    processor.add_data_extractor(data_extractor)
    processor.start()
    processor.finished.connect(app.quit)

    app.exec()


def init_logging():
    logging.config.fileConfig(tests.common.get_logging_file_path())
