import pytest
import logging.config

from PySide6.QtCore import QCoreApplication

from shop_parser.core import DataExtractor, Processor
from shop_parser.network import Request, Reply
from shop_parser.parsers.megastroy.data_extractor import MegastroyShopInfoDataExtractor

import tests.common


class LeroyMerlinDataExtractor(DataExtractor):
    name = 'TestDataExtractor'
    start_urls = ['https://leroymerlin.ru/']

    def process_reply(self, reply: Reply, **kwargs):
        urls = ['https://leroymerlin.ru/catalogue/radiatory-otopleniya/']
        for url in urls:
            yield Request(url, callback=self.process_second_page)

    def process_second_page(self, reply, **kwargs):
        urls = [
            'https://leroymerlin.ru/product/radiator-royal-thermo-vittoria-b500-82755705/',
            'https://leroymerlin.ru/product/radiator-equation-500-90-18360629/'
        ]

        for url in urls:
            yield Request(url, callback=self.finish)

    def finish(self, reply: Reply, **kwargs):
        for i in range(5):
            yield f'some string {i}'


@pytest.mark.skip(reason='It takes too long to execute (more than a minute)')
def test_processor():
    init_logging()

    app = QCoreApplication()
    processor = Processor()
    processor.add_data_extractor(LeroyMerlinDataExtractor())
    processor.start()
    app.exec()


@pytest.mark.skip(reason='It takes too long to execute (about 20 seconds)')
def test_megatsroy_processor():
    init_logging()

    app = QCoreApplication()
    processor = Processor()
    processor.add_data_extractor(MegastroyShopInfoDataExtractor())
    processor.start()
    processor.finished.connect(app.quit)
    app.exec()


def init_logging():
    logging.config.fileConfig(tests.common.get_logging_file_path())
