from pathlib import Path
import os.path

import shop_parser.core.items as sp
from shop_parser.network import Reply, Request


CURRENT_MODULE_DIR = os.path.dirname(os.path.abspath(__file__))


def get_tests_root_dir() -> str:
    return CURRENT_MODULE_DIR


def get_project_root_dir() -> str:
    out = Path(get_tests_root_dir()).parent
    return str(out)


def get_project_src_root_dir() -> str:
    out = get_project_root_dir()
    out += '/shop_parser'

    return out


def get_logging_file_path() -> str:
    out = get_project_src_root_dir()
    out += '/logging.conf'

    return out


def get_fixture_absolute_file_path(fixture_relative_file_path: str) -> str:
    return os.path.join(get_tests_root_dir(), fixture_relative_file_path)


def read_relative_file(relative_file_path: str) -> str:
    file_path = get_fixture_absolute_file_path(relative_file_path)
    with open(file_path, 'r', encoding='utf-8') as file:
        out = file.read()

    return out


def get_reply_from_file(relative_file_path, url='http://www.example.com'):
    content = read_relative_file(relative_file_path)
    request = Request(url, callback=None)
    return Reply(status_code=200, url=url, text=content, request=request)


def get_tree_depth(category_tree: sp.CategoryTree) -> int:
    depth = 0
    tree_item = category_tree.root_item
    while tree_item.children:
        depth = depth + 1
        tree_item = tree_item.children[0]

    return depth


def has_city(
        city_list: list[sp.City],
        city_to_find: sp.City) -> bool:
    out = False
    for city in city_list:
        if city == city_to_find:
            out = True
            break

    return out


def has_string(string_list: list[str], string_to_find: str) -> bool:
    out = False
    for string in string_list:
        if string == string_to_find:
            out = True
            break

    return out
