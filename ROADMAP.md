### Shops to parse:
- [x] [Леруа Мерлен]
- [x] [Мегастрой]
- [x] [Максидом]
- [x] [Оби]
- [x] [Петрович]
- [x] [Афоня]
- [x] [Водопад]
- [x] [Аксон]
- [x] [Бауцентр]
- [x] [Касторама]
- [x] [Добрострой]
- [x] [Аквалинк]
- [x] [Водолей]
- [x] [Стройдача]


# Version 0.1

Target shop: [Мегастрой]

- [x] Gather cities list from the main page
- [x] Collect category tree from the main page
- [x] Gather pagination 
- [x] Gather product information:

    + [x] name
    + [x] price
    + [x] vendor code
    + [x] url
  

# Version 0.2

Target shop: [Максидом]

- [x] Gather cities list from the main page
- [x] Collect category tree from the main page
- [x] Gather pagination 
- [x] Gather product information:

    + [x] name
    + [x] price
    + [x] vendor code
    + [x] url


# Version 0.3

Target shop: [Леруа Мерлен]

- [x] Gather cities list from the main page
- [x] Collect category tree from json api
- [x] Gather pagination 
- [x] Gather product information:

    + [x] name
    + [x] price
    + [x] vendor code
    + [ ] url
  

# Version 0.4

Target shop: [Оби]

- [ ] Gather cities list (??)
- [x] Collect category tree [API](https://www.obi.ru/sitemap/)
- [x] Gather pagination 
- [x] Gather product information:

    + [x] name
    + [x] price
    + [x] vendor code (from url)
    + [x] url
  

# Version 0.5

Target shop: [Петрович]

- [x] Gather cities list
- [x] Collect category tree 
- [x] Gather pagination 
- [x] Gather product information:

    + [x] name
    + [x] price
    + [x] vendor code 
    + [x] url


# Version 0.6

GUI:

- [x] Choose a framework
- [x] Basic views: shops, cities, catalogs
- [x] Fill the views by data
- [x] Filter data by checkboxes
- [x] 'Start parsing' button
- [x] Test with fake 'process' classes


# Version 0.7

- [x] Tie the GUI together with real parsing classes

# Bugfix
- [x] Fix category parsing in Petrovich
- [x] Remove duplicates in .csv
- [ ] Missing items in parsing. 
- [ ] Petrovich parse price with sale
- [ ] Stop parsing process immediately


[Леруа Мерлен]: https://leroymerlin.ru/
[Мегастрой]: https://megastroy.com
[Максидом]: https://www.maxidom.ru/
[Оби]: https://www.obi.ru/
[Петрович]: https://petrovich.ru/
[Афоня]: https://www.afonya-spb.ru/
[Водопад]: https://vodopad.ru/
[Аксон]: https://akson.ru/
[Бауцентр]: https://baucenter.ru
[Касторама]: https://www.castorama.ru/
[Добрострой]: https://добрострой.рф/
[Аквалинк]: https://akvalink.ru/
[Водолей]: https://водолей.рф/
[Стройдача]: https://stroyudacha.ru/
